//
//  PaymentViewController.h
//  Stripe
//
//  Created by Alex MacCaw on 3/4/13.
//
//

/*
 - (void)addCardViewController:
 (nonnull STPAddCardViewController *)addCardViewController
 didCreateToken:(nonnull STPToken *)token
 completion:(nonnull STPErrorBlock)completion;
 */

#import <Stripe/Stripe.h>
#import <UIKit/UIKit.h>

@class PaymentViewController;

@protocol PaymentViewControllerDelegate<NSObject>

- (void)addCardViewController:
(STPAddCardViewController *)addCardViewController
               didCreateToken:(STPToken *)token
                   completion:(STPErrorBlock)completion;

- (void)paymentViewController:(PaymentViewController *)controller didFinish:(NSError *)error;

@end

@interface PaymentViewController : UIViewController {
    // AppDelegate *appdelegate;
    NSMutableDictionary *dicOfferList;
    NSString * TransactionNo;
    NSString * str;
}

@property (nonatomic) NSDecimalNumber *amount;
@property (nonatomic) NSDecimalNumber *Eventid;
@property (nonatomic) NSDecimalNumber *Playerid;
@property (nonatomic) STPToken  *Token;
@property (nonatomic) STPCard *card;

@property (nonatomic, weak) id<PaymentViewControllerDelegate> delegate;

@end
