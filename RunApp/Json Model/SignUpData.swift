//
//  SignUpData.swift
//  RunApp
//
//  Created by My Mac on 30/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import ObjectMapper

class GetUserRoleData : Mappable {
    var RoleID: String?
    var RoleName: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        RoleID <- map["RoleID"]
        RoleName <- map["RoleName"]
    }
}
