//
//  extensions.swift
//  ChatApp
//
//  Created by My Mac on 2/12/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import MobileCoreServices
import SHSnackBarView
import Alamofire
import SVProgressHUD
import CoreLocation
import NightNight

//MARK:- Tab + Navigation

extension UIViewController
{
    
    func callApi(_ urlString: String,
                 method: HTTPMethod,
                 param: [String:Any] = [String: Any](),
                 extraHeader: [String:Any] = [String: Any](),
                 withLoader: Bool = true,
                 withHeader: Bool = true,
                 data: [Data] = [Data](),
                 data1: [URL] = [URL](),
                 dataKey: [String] = [String](),
                 completionHandler: @escaping ([String: Any]) -> ())
    {
        guard let url = URL(string: urlString) else { return }
        
        print("url: \(url)")
        print("param : \(param)")
        print("method : \(method)")
        if isInternetAvailable(){

            if withLoader
            {
                SVProgressHUD.show(withStatus: nil)
            }
            
            var header: HTTPHeaders? = nil
//            if withHeader
//            {
//                let username = ServiceList.USERNAME
//                let password = ServiceList.PASSWORD
//
//                let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
//                let base64LoginData = loginData.base64EncodedString()
//
            var dic1 = [String: Any]()
            dic1 = ["Authorization": "Basic \(ServiceList.AUTHORIZATION_AUTH)",
                "X-SIMPLE-API-KEY" : ServiceList.X_SIMPLE_API_KEY]
            dic1.merge(dict: extraHeader)
            header = dic1 as? HTTPHeaders ?? nil
                
//            }
            print("header : \(String(describing: header))")
            
            if dataKey.count > 0
            {
                Alamofire.upload(
                    multipartFormData: { multipartFormData in
                        
                        for (key, value) in param
                        {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                        }
                        
                        for i in 0..<data.count
                        {
                            multipartFormData.append(data[i], withName: dataKey[i], fileName: dataKey[i] + ".png", mimeType: "image/png")
                        }
                        
                        for index in 0..<data1.count {
                            
                            let data = NSData(contentsOf: data1[index] as URL)!
                            multipartFormData.append(data as Data, withName: dataKey[index], fileName: "video.mp4", mimeType: "video/mp4")
                            
                        }
                        
                },
                    to: urlString,
                    headers: header,
                    encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { result in
                                
                                if withLoader
                                {
                                    SVProgressHUD.dismiss()
                                }
                                
                                print(result)
                                print(result.result)
                                if let httpError = result.result.error
                                {
                                    print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue) ?? "Exception")
                                    print(httpError._code)
                                }
                                
                                if  result.result.isSuccess
                                {
                                    let response1 = result.result.value as! [String:Any]
                                    completionHandler(response1)
                                }
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                })
            }
            else
            {
                Alamofire.request(
                    URL(string: urlString)!,
                    method: method,
                    parameters: param,
                    headers: header)
                    .validate()
                    .responseJSON { (result) in
                        if withLoader
                        {
                            SVProgressHUD.dismiss()
                        }
                        
                        if let httpError = result.result.error
                        {
                          
                            print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                            
                            let getErrData = String(data: result.data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
                            print(getErrData)
                            print(httpError._code)
                        
                           let json = self.convertToDictionary(text: getErrData)
                            print("json: \(String(describing: json))")
                            completionHandler(json ?? [:])
                        }
                            
                        else if result.result.isSuccess
                        {
                            let response = result.result.value as! [String:Any]
                            completionHandler(response)
                        }
                }
            }
        }
            
        else
        {
            noInternetConnectionAlert(uiview: self)
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // Navigation bar
    func setGlobalNavigationbarWithMenuButton(strTitle : String , strRightTitle : String , color : UIColor , strBadgeCount : String , strRightTitle2 : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().isTranslucent = true
        navigationController?.navigationBar.barTintColor = color

        self.navigationItem.hidesBackButton = true
        
//        let sizeLength = UIScreen.main.bounds.size.height * 2
//        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 44)
//
//        let bgGredientImage = UIImageView()
//        bgGredientImage.frame = defaultNavigationBarFrame
//        bgGredientImage.setGredient()
//
//        self.navigationController?.navigationBar.addSubview(bgGredientImage)

        let gradient = CAGradientLayer()
        var bounds =  self.navigationController?.navigationBar.bounds
        bounds!.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds!
//        gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
        gradient.colors = [UIColor.init(rgb: ConstantVariables.Constants.navigationColor).cgColor,
         UIColor.init(rgb: ConstantVariables.Constants.nav_red).withAlphaComponent(1).cgColor]
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 0)
        
        if let image = getImageFrom(gradientLayer: gradient) {
             self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        }
        
        let btnName = UIButton()
        let image = UIImage(named: "menu_icon")?.withRenderingMode(.alwaysTemplate)
        btnName.setImage(image, for: .normal)
        btnName.tintColor = UIColor.white
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.addTarget(self, action: #selector(btnMenuAction), for: .touchUpInside)
        
        let menuBarButton1 = UIBarButtonItem()
        menuBarButton1.customView = btnName
        
//        let menuBarButton2 = UIBarButtonItem()
//        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
//        lblTitle.text = strTitle
//        lblTitle.textColor = UIColor.white
//        lblTitle.textAlignment = .left
//        menuBarButton2.customView = lblTitle
        
        self.title = strTitle
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        let btnInfo = UIButton()
        btnInfo.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let image1 = UIImage(named: strRightTitle)?.withRenderingMode(.alwaysTemplate)
        btnInfo.setImage(image1, for: .normal)
        btnInfo.tintColor = UIColor.white
        btnInfo.setTitleColor(UIColor.white, for: .normal)
        btnInfo.titleLabel?.textAlignment = .right
        btnInfo.addTarget(self, action: #selector(btnRightAction), for: .touchUpInside)

        let btnFilter = UIButton()
        btnFilter.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let image2 = UIImage(named: strRightTitle2)?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(image2, for: .normal)
        btnFilter.tintColor = UIColor.white
        btnFilter.imageView?.contentMode = .scaleAspectFit
        btnFilter.addTarget(self, action: #selector(btnRightAction2), for: .touchUpInside)
        
        let menuBarRightButton1 = UIBarButtonItem()
        menuBarRightButton1.customView = btnInfo
       
        let menuBarRightButton2 = UIBarButtonItem()
        menuBarRightButton2.customView = btnFilter
        
        self.navigationItem.setLeftBarButtonItems([menuBarButton1], animated: true)
        self.navigationItem.setRightBarButtonItems([menuBarRightButton1,menuBarRightButton2 ], animated: true)
        
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    @objc func btnMenuAction()
    {
//        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @objc func btnRightAction()
    {
        
    }
    
    @objc func btnRightAction2()
    {
        
    }
    
    //Back Button
    
    func setGlobalNavigationbarWithBackButton(strTitle : String , strRightTitle : String , strBadgeCount : String , color : UIColor, isBlackArrow : Bool)
    {
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = color
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().isTranslucent = true
        
        let btnName = UIButton()
 
        btnName.setImage(UIImage(named: "backicon"), for: .normal)
        btnName.imageView?.changeImageViewImageColor(color: UIColor.white)
        
        btnName.setImage(btnName.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnName.tintColor = color
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.addTarget(self, action: #selector(btnGlobalBackAction), for: .touchUpInside)
        
        let menuBarButton1 = UIBarButtonItem()
        menuBarButton1.customView = btnName
        
        self.title = strTitle
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        // Info button
        
        let label = UILabel(frame: CGRect(x: 15, y: -10, width: 20, height: 20))
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.init(rgb: ConstantVariables.Constants.navigationColor)
        
        if let a = UserDefaults.standard.object(forKey: "cart_count")
        {
            label.text = "\(a)"
            
            if "\(a)" == "0"
            {
                label.isHidden = true
            }
            else
            {
                label.isHidden = false
            }
        }
        else
        {
            label.text = "0"
            label.isHidden = true
        }
        
        label.font = UIFont.systemFont(ofSize: 10)
        label.layer.cornerRadius = label.frame.height/2
        label.clipsToBounds = true
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        label.isUserInteractionEnabled = false
        
        let btnInfo = UIButton()
        btnInfo.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnInfo.setImage(UIImage(named : strRightTitle), for: .normal)
        if strRightTitle != ""
        {
            let image = UIImage(named: strRightTitle)?.withRenderingMode(.alwaysTemplate)
            btnInfo.setImage(image, for: .normal)
            btnInfo.tintColor = UIColor.white
        }
        
        if strRightTitle != ""
        {
            btnInfo.addSubview(label)
        }
        
        btnInfo.addTarget(self, action: #selector(btnGlobal_Back_RightAction), for: .touchUpInside)
        
        let menuBarRightButton1 = UIBarButtonItem()
        menuBarRightButton1.customView = btnInfo
        
        if isBlackArrow
        {
            self.navigationItem.setLeftBarButtonItems([menuBarButton1], animated: true)
        }
        self.navigationItem.setRightBarButton(menuBarRightButton1, animated: true)
        
    }
    
    @objc func btnGlobalBackAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnGlobal_Back_RightAction()
    {
        
    }
    
    func openGalleryCameraPicker(picker : UIImagePickerController)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera(picker: picker)
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary(picker: picker)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //picker.mediaTypes = [kUTTypePNG as NSString as String]
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Image Selection
    func openCamera(picker : UIImagePickerController) {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
            
        else
        {
            openGallary(picker: picker)
        }
    }
    
    func openGallary(picker : UIImagePickerController)
    {
//        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        //above recent add
//        picker.allowsEditing = true
        
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    /*
     imagePicker.allowsEditing = false
     imagePicker.sourceType = .photoLibrary
     
     present(imagePicker, animated: true, completion: nil)
 */
    
    //Image_Video Selection
    
    func openImageVideoSelectionPicker(picker : UIImagePickerController)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Choose Image", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGalleryCameraPicker(picker: picker)
        }
        let gallaryAction = UIAlertAction(title: "Choose Video", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallaryVideos(picker: picker)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //picker.mediaTypes = [kUTTypePNG as NSString as String]
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallaryVideos(picker : UIImagePickerController)
    {
        picker.sourceType = .savedPhotosAlbum
        picker.allowsEditing = true
        picker.videoMaximumDuration = TimeInterval(30.0)
        picker.mediaTypes = [kUTTypeMovie as NSString as String]
        self.present(picker, animated: true, completion: nil)
    }
    
    func showViewWithAnimation(vw : UIView , img : UIImageView) {
        vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.3) {
            vw.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            vw.isHidden = false
            img.isHidden = false
            
        }
    }
    
    func hideViewWithAnimation(vw : UIView , img : UIImageView) {
        UIView.animate(withDuration: 0.3, animations: {
            vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }, completion: {
            (value: Bool) in
            vw.isHidden = true
            img.isHidden = true
        })
    }
    
    func sortArrayDictDescending(dict: [[String : Any]], dateFormat: String) -> [[String : Any]] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dict.sorted{[dateFormatter] one, two in
            return dateFormatter.date(from: one["CreatedOn"] as? String ?? "" )! > dateFormatter.date(from: two["CreatedOn"] as? String ?? "" )! }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
         tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
}

//MARK:- PageControll
extension UIPageControl {
    func customPageControl(dotWidth: CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            dotView.frame.size = CGSize.init(width: dotWidth, height: dotWidth)
        }
    }
}

//MARK:- TextField
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat , strImage : String){
        
        let paddingView = UIImageView(frame: CGRect(x: 15, y: 20, width: amount, height: 20))
        paddingView.image = UIImage(named : strImage)
        if strImage != "" {
            if NightNight.theme == .night {
                paddingView.changeImageViewImageColor(color: UIColor.white)
                
            } else {
                paddingView.changeImageViewImageColor(color: UIColor.black)
            }
           
        }
        paddingView.contentMode = .scaleAspectFit
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat , strImage : String){
        let paddingView = UIImageView(frame: CGRect(x: 15, y: 20, width: amount, height: 20))
        paddingView.image = UIImage(named : strImage)
        paddingView.contentMode = .scaleAspectFit
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
}

//MARK:- Image
extension UIImageView {
  func enableZoom() {
    let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(startZooming(_:)))
    isUserInteractionEnabled = true
    addGestureRecognizer(pinchGesture)
  }

  @objc
  private func startZooming(_ sender: UIPinchGestureRecognizer) {
    let scaleResult = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)
    guard let scale = scaleResult, scale.a > 1, scale.d > 1 else { return }
    sender.view?.transform = scale
    sender.scale = 1
  }
}

extension UIImage
{
    /// Given a required height, returns a (rasterised) copy
    /// of the image, aspect-fitted to that height.

    func aspectFittedToHeight(_ newHeight: CGFloat) -> UIImage
    {
        let scale = newHeight / self.size.height
        let newWidth = self.size.width * scale
        let newSize = CGSize(width: newWidth, height: newHeight)
        let renderer = UIGraphicsImageRenderer(size: newSize)

        return renderer.image { _ in
            self.draw(in: CGRect(origin: .zero, size: newSize))
        }
    }
}

extension UIImage
{
    func imageWithInsets(insetDimen: CGFloat) -> UIImage {
        return imageWithInsets(insets: UIEdgeInsets(top: insetDimen, left: insetDimen, bottom: insetDimen, right: insetDimen))!
    }
    
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
    
}

//MARK:- Image

extension UIImageView
{
    func changeImageViewImageColor(color : UIColor)
    {
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
}

//MARK:- UIView

extension UIView
{
    func setRadius(radius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setRadiusBorder(color: UIColor , weight : CGFloat = 2) {
        self.layoutIfNeeded()
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = weight
        self.layer.masksToBounds = true
    }
    
    func setDottedRadiusBorder(color: UIColor) {
        self.layoutIfNeeded()
        let border = CAShapeLayer()
        border.strokeColor = color.cgColor
        border.fillColor = nil
        border.lineDashPattern = [4, 4]
        border.path = UIBezierPath(rect: self.bounds).cgPath
        border.frame = self.bounds
        self.layer.addSublayer(border)
        self.layoutIfNeeded()
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        self.layoutIfNeeded()
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        self.layoutIfNeeded()
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.black.withAlphaComponent(1).cgColor
        let colorBottom = UIColor.white.withAlphaComponent(0.1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop , colorTop, colorBottom]
        gradientLayer.locations = [0.0 , 0.05  , 1.0]
        gradientLayer.frame = self.bounds
        self.layer.mask = gradientLayer
    }
    
    func setGredientBorder()
    {
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [UIColor.black.withAlphaComponent(0.7).cgColor,
                           UIColor.lightGray.withAlphaComponent(0.5).cgColor]
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(rect: self.bounds).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
    
    func setGredientDark()
    {
        self.layoutIfNeeded()
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.lightGray.cgColor,
                                 UIColor.white.withAlphaComponent(1).cgColor].map{$0}
        self.layer.addSublayer(gradientLayer)
        self.layoutIfNeeded()
    }
    
    func setShadow()
    {
        self.layer.shadowColor = UIColor.init(rgb: 0xB5B5B5).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
    }
    
    func setTopShadow()
    {
        self.layer.shadowColor = UIColor.init(rgb: 0xB5B5B5).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: -4.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
    }
    
    func setGredient()
    {
        self.layoutIfNeeded()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.init(rgb: ConstantVariables.Constants.nav_red).cgColor,
                                 UIColor.init(rgb: ConstantVariables.Constants.navigationColor).withAlphaComponent(1).cgColor].map{$0}
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.addSublayer(gradientLayer)
    }
    
    //Table Animation
    func loadAnimation( ) {
        self.transform = CGAffineTransform(translationX: 0, y: 50)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.6, delay: 0.05, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)
            self.alpha = 1
        }, completion: nil)
    }
    
    func loadAnimationForCollection( ) {
        self.transform = CGAffineTransform(translationX:50, y: 0)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.6, delay: 0.05, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)
            self.alpha = 1
        }, completion: nil)
    }
    
    func dampAnimation()
    {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.4,
                       initialSpringVelocity: 8.0,
                       options: .allowUserInteraction,
                       animations: {
                        self.transform = .identity
                        self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        
        },
                       completion: nil)
    }
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
}

//MARK:- Color

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }

}

//MARK:- UIAlertController

func showMessage(msg: String , uiview: UIViewController) {
    let alert = UIAlertController(title: ConstantVariables.Constants.Project_Name, message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
    uiview.present(alert, animated: true, completion: nil)
}

//MARK:- Custom Alert
func showAlert(uiview: UIViewController , msg : String , isTwoButton : Bool) {

        let window : UIWindow = UIApplication.shared.keyWindow!
        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: msg)

        let action1 = EMAlertAction(title: "Ok", style: .cancel)
        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            // Perform Action
        }

        alert.addAction(action: action1)
        if isTwoButton {
            alert.addAction(action: action2)
        }
        uiview.present(alert, animated: true, completion: nil)
}


//MARK:- UPLOAD IMAGE
extension UIImageView
{
    //options: [.transition(ImageTransition.fade(1))]
    func setImage(_ url: String)
    {
        let url = URL(string: url)
        
//        self.kf.setImage(with: url,
//                         placeholder: nil,
//                         options: [.transition(ImageTransition.fade(1))],
//                         progressBlock: { receivedSize, totalSize in
//        },
//                         completionHandler: { image, error, cacheType, imageURL in
//        })
    }
}

//MARK:- String

extension String {
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func trimmingString() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)        
    }
    
    func URLEncodedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return escapedString
    }
    
    func URLQueryAllowedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        return escapedString
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func toDate() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: self)!
    }
    
    func toDate2() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: self)!
    }
    
    func toDate3() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)!
        dateFormatter.dateFormat = "MM/yyyy"
        let newDate = dateFormatter.string(from: date)
        return newDate
    }
    
    func toDate4() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "MM-dd-yy"
        let newDate = dateFormatter.string(from: date ?? Date())
        return newDate
    }
    
    func toDate5() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        //"MM-dd-yy hh:mm a"
        dateFormatter.dateFormat = "MM-dd-yy hh:mm a"
        let newDate = dateFormatter.string(from: date ?? Date())
        return newDate
    }
    
    func decimal(with string: String) -> NSDecimalNumber {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        return formatter.number(from: string) as? NSDecimalNumber ?? 0
    }
}

//MARK:- Custom No Internet Alert

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

func noInternetConnectionAlert(uiview: UIViewController) {

    let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Please check your internet connection or try again later")
    let action1 = EMAlertAction(title: "Ok", style: .cancel)
    alert.addAction(action: action1)
    uiview.present(alert, animated: true, completion: nil)
}

//MARK:- CLLocation
typealias AddressDictionaryHandler = ([String: Any]?) -> Void

extension CLLocation {
    
    func addressDictionary(completion: @escaping AddressDictionaryHandler) {
        
        CLGeocoder().reverseGeocodeLocation(self) { placemarks, _ in
            completion(placemarks?.first?.addressDictionary as? [String: AnyObject])
        }
    }
}

//MARK:- Toast

func showToast(uiview: UIViewController , msg : String )
{
    let window : UIWindow = UIApplication.shared.keyWindow!
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

//    let snackbarView = snackBar()
    
//            if NightNight.theme == .night {
//               snackbarView.showSnackBar(view: appDelegate.window!, bgColor: UIColor.white, text: msg, textColor: UIColor.black, interval: 2)
//
//            } else {
//
//                snackbarView.showSnackBar(view: appDelegate.window!, bgColor: UIColor.white, text: msg, textColor: UIColor.black, interval: 2)
//
//           }
    
    
    let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert);
    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
    uiview.present(alert, animated: true, completion: nil)
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


extension Dictionary
{
    func getInt(key: String) -> Int
    {
        for (keyFromDict, val) in self
        {
            if keyFromDict as! String == key
            {
                switch val {
                case is NSNull:
                    return 0
                case is Int:
                    return val as! Int
                case is Double:
                    return Int(val as! Double)
                case is Bool:
                    return val as! Bool ? 1 : 0
                case is String:
                    return (val as! String).contains(".") ?
                        Int(Double(val as! String) ?? 0.0) :
                        Int(val as! String) ?? 0
                default:
                    return 0
                }
            }
        }
        return 0
    }
    
    func getDouble(key: String) -> Double
    {
        for (keyFromDict, val) in self
        {
            if keyFromDict as! String == key
            {
                switch val {
                case is NSNull:
                    return 0.0
                case is Double:
                    return val as! Double
                case is Int:
                    return Double(val as! Int)
                case is Bool:
                    return val as! Bool ? 1 : 0
                case is String:
                    return Double(val as! String) ?? 0.0
                default:
                    return 0.0
                }
            }
        }
        return 0.0
    }
    
    func getBool(key: String) -> Bool
    {
        for (keyFromDict, val) in self
        {
            if keyFromDict as! String == key
            {
                switch val {
                case is NSNull:
                    return false
                case is Int:
                    return (val as! Int) != 0
                case is Double:
                    return (val as! Double) != 0.0
                case is Bool:
                    return val as! Bool
                case is String:
                    return (val as! String) == "true" || (val as! String) != "0"
                default:
                    return false
                }
            }
        }
        return false
    }
    
  
    
    func getArray(key: String) -> [Any]
    {
        for (keyFromDict, val) in self
        {
            if keyFromDict as! String == key
            {
                switch val {
                case is NSNull:
                    return [Any]()
                case is Array<Any>:
                    return val as! [Any]
                default:
                    return [Any]()
                }
            }
        }
        return [Any]()
    }
    
    func getDictionary(key: String) -> [String: Any]
    {
        for (keyFromDict, val) in self
        {
            if keyFromDict as! String == key
            {
                switch val {
                case is NSNull:
                    return [String: Any]()
                case is [String: Any]:
                    return val as! [String: Any]
                default:
                    return [String: Any]()
                }
            }
        }
        return [String: Any]()
    }
    
    func getArrayofInt(key: String) -> [Int]
    {
        let array = self.getArray(key: key)
        if array is [Int]
        {
            return array.map({ ($0 as! Int) })
        }
        return [Int]()
    }
    
    func getArrayofDouble(key: String) -> [Double]
    {
        let array = self.getArray(key: key)
        if array is [Double]
        {
            return array.map({ ($0 as! Double) })
        }
        return [Double]()
    }
    
    func getArrayofBool(key: String) -> [Bool]
    {
        let array = self.getArray(key: key)
        if array is [Bool]
        {
            return array.map({ ($0 as! Bool) })
        }
        return [Bool]()
    }
    
    func getArrayofDictionary(key: String) -> [[String: Any]]
    {
        let array = self.getArray(key: key)
        if array is [[String: Any]]
        {
            return array.map({ ($0 as! [String: Any]) })
        }
        return [[String: Any]]()
    }
    
    func getArrayofString(key: String) -> [String]
    {
        return self.getArray(key: key).map({ "\($0)" })
    }
    
    func getString(key: String) -> String
    {
        for (keyFromDict, val) in self
        {
            if keyFromDict as! String == key
            {
                switch val {
                case is NSNull:
                    return ""
                default:
                    return "\(val)"
                }
            }
        }
        return ""
    }
}


extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

extension Date
{
    func timeAgoSinceDate() -> String {
        
        let calendar = NSCalendar.current
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: Date())
        
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        
        let components = calendar.dateComponents(unitFlags, from: date1, to: date2)// components(flags,
        if (components.year! >= 2)
        {
            return "\(components.year!) years ago"
        }
        else if (components.year! >= 1)
        {
            return "1 year ago"
        }
        else if (components.month! >= 2)
        {
            return "\(components.month!) months ago"
        }
        else if (components.month! >= 1)
        {
            return "1 month ago"
        }
        else if (components.weekOfYear! >= 2)
        {
            return "\(components.weekOfYear!) weeks ago"
        }
        else if (components.weekOfYear! >= 1)
        {
            return "1 week ago"
        }
        else if (components.day! > 1 && components.day! < 7)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! >= 2)
        {
            return "\(components.day!) days ago"
        }
        else if (components.day! == 1)
        {
            return "Yesterday"
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            return "\(dateFormatter.string(from: self))"
        }
    }
    
    func timeAgoSinceDate1() -> String
    {
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: Date())
        
        let unitFlags: Set<Calendar.Component> = [.day]
        //        let flags = NSCalendar.Unit.day
        let components = calendar.dateComponents(unitFlags, from: date1, to: date2)// components(flags, fromDate: date1, toDate: date2, options: [])
        
        if (components.day! >= 7)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM MM, yyyy"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! > 1)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! == 1)
        {
            return "Yesterday"
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            return "\(dateFormatter.string(from: self))"
        }
    }
    
    func timeAgoSinceDate2() -> String
    {
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: Date())
        
        let unitFlags: Set<Calendar.Component> = [.day]
        //        let flags = NSCalendar.Unit.day
        let components = calendar.dateComponents(unitFlags, from: date1, to: date2)// components(flags, fromDate: date1, toDate: date2, options: [])
        
        if (components.day! >= 7)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM MM, yyyy"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! > 1)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! == 1)
        {
            return "Yesterday"
        }
        else
        {
            return "Today"
        }
    }
    
    func time() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return "\(dateFormatter.string(from: self))"
    }
    
    
    
}


@IBDesignable
class FormTextField: UITextView {

    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
}
extension String
{
    
        var decodeEmoji: String {
            if let encodeStr = NSString(cString: self.cString(using: .isoLatin1)!, encoding: String.Encoding.utf8.rawValue)
            {
                      return encodeStr as String
                
            }
                  return self
            
//                 NSString *ConvertedString = [NSString stringWithCString:[str cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
            
//          let ConvertedString = String(
//            cString: self.cString(using: String.Encoding(rawValue: String.Encoding.isoLatin1.rawValue))! ,
//            encoding: .utf8) ?? ""
            
           
        }
    
    
    var encodeEmoji: String{
      
        let data = self.data(using: String.Encoding.utf8);
                   let decodedStr = NSString(data: data!, encoding: String.Encoding.isoLatin1.rawValue)
                   if let str = decodedStr{
                       return str as String
                   }
                   return self
        
//        let dataenc = self.data(using: .utf8)
//        var encodevalue: String? = nil
//        if let dataenc = dataenc {
//            encodevalue = String(
//                data: dataenc,
//                encoding: .isoLatin1)
//        }
//        return encodevalue ?? ""
    }
}
