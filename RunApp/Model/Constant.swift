//
//  Constant.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit

struct MyClassConstants
{
    static var dictGlobalData: [String : AnyObject] = [:]
}

struct CustomerConstants
{
    static var user_id = String()
    
}

//MARK:- Screen Size

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
}

//MARK:- Custom fonts
struct FontNames {
    
//    static let RajdhaniName = "rajdhani"
//    struct Rajdhani {
        static let RajdhaniBold = "rajdhani-bold"
        static let RajdhaniMedium = "rajdhani-medium"
//    }
}
struct FontColor {
    
    static let dayfont = 0x000000
    static let nightfont = 0xffffff
}

//MARK:- Webservices

 struct ServiceList
 {
   // static let SERVICE_URL = "https://test.runapp.blenzabi.com/api/v2/"
    
    
    static let SERVICE_URL = "http://54.83.90.21/api/v2/"
    
    static let SERVICE_AUTH = "auth/"
   // static let IMAGE_URL = "https://test.runapp.blenzabi.com/uploads/"
    
    static let IMAGE_URL = "http://54.83.90.21/uploads/"
    
   // static let IMAGE_URL = "https://test.runapp.blenzabi.com/admin/"
    
   //static let ADMIN_IMAGE_URL = "https://test.runapp.blenzabi.com/admin/"
     
    static let ADMIN_IMAGE_URL = "http://54.83.90.21/admin/"
    
    static let USERNAME = "admin"
    static let PASSWORD = "admin"
    static let X_SIMPLE_API_KEY = "ow400c888s4c8wck8w0c0w8co0kc00o0wgoosw80"
    static let AUTHORIZATION_AUTH = "YWRtaW46YWRtaW4="

    static let ADD_REGISTER_API = SERVICE_AUTH + "register_user"
    static let GET_POSITION_LIST = "AppPosition"
    static let GET_LOC_LIST = "AppCompitionLevel"
    static let report_reason_list_get_API = "AppUserReport/report_reason_list"
    static let reportUser = "AppUserReport/ReportUser"
    
    
    static let GET_USER_TYPE_LIST = "AppRole"
    static let GET_ORGANIZATION_ROLE = "AppRoleOrganization"
    static let LOGIN = SERVICE_AUTH + "login"
    static let FORGOT_PASSWORD = SERVICE_AUTH + "forgot_password"
    static let SOCIAL_LOGIN = SERVICE_AUTH + "register_social"
 
    static let STATE_LIST = "State"
    static let CITY_LIST = "City"
    static let ADD_LOCATION = "AppLocation"
    static let GET_LOCATIONS = "AppGetLocation"  
    static let ADD_EVENT = "AppEvent"
    static let Cancel_UserEvent = "CancelEvent"
    static let GET_EVENT = "AppGetEvent"
    static let APP_PLAYER_LIST = "AppPlayerList"
     static let APP_Org_LIST = "AppPlayerList/organizer"
    static let APP_EVENT_STATUS = "AppEventStatus"
    static let GET_PRODUCT_LIST = "AppProduct"
    static let BUY_PRODUCT = "AppCart"
    static let GET_NEWS_FEED = "AppNews"
    static let GET_APP_TICKER = "AppTicker2"
    static let GET_MAIN_COMMENT_LIST = "AppGetComment"
    static let GET_SUB_COMMENT_LIST = "AppCommentInner"
    static let ADD_COMMENT = "AppCommentAdd"
    static let APP_USER_PROFILE = "AppUserProfile" 
    static let ORGANIZATION_SOCIAL = SERVICE_AUTH + "organization_social"
    static let ORGANIZATION = SERVICE_AUTH + "organization"
    static let ADD_FRIENDS = "AppFriendsAdd"
    static let CHANGE_PASSWORD_LOGIN = SERVICE_AUTH + "change_password"
    static let EDIT_REGISTER_USER = SERVICE_AUTH + "edit_register_user"
    static let EDIT_ORGANIZATION = SERVICE_AUTH + "edit_organization"
    static let APP_FRIEND_GET = "AppFriendsGet"
    static let APP_BADGEST_SKILL = "AppBadgeListSkill"
    static let APP_BADGE_LIST_CHARECTER = "AppBadgeListCharacter"
    static let APP_BADGE_LIST_TEAM = "AppBadgeListTeam"
    static let APP_BADGE_DESCRIPTION = "AppBadgeDescription"
    static let APP_LIVE_LOCATION = "AppLiveLocation"
    static let APP_NOMINATE_ADD = "AppNominateAdd"
    static let APP_PROPS = "AppProps"
    static let APP_PLAYER_WISE_EVENT = "AppPlayerWiseEvent"
    static let APP_PLAYER_CHECK_IN = "AppPlayerCheckIn"
    static let APP_GET_RSVP_LOCATION = "AppGetRSVPLocation"
    static let APP_SPECTATOR_JOIN = "AppSpectorJoin"
    static let APP_SPECTATOR_CANCEL = "AppCancelSpectorJoin"
    static let APP_PLAYER_WISE_EVENT_DETAILS = "AppPlayerWiseEventDetails"
    static let APP_PHOTO_ADD = "AppPhotoAdd"
    static let APP_GET_PHOTO = "AppGetPhoto"
    static let APP_HISTORY = "AppHistory"
    static let APP_MY_FRIEND = "AppMyFriend"
    static let APP_FRIENDS_REQUEST_GET = "AppFriendsRequestGet"
    static let APP_UN_FRIENDS = "AppUnFriends"
    static let APP_FRIENDS_ACTION = "AppFriendsAction"
    static let APP_TREADING_HEADLINE = "AppTreadingheadline"
    static let APP_PAYMENT = "AppPayment" 
    static let APP_FAVOURITE = "AppFavouriteAdd"
    static let APP_CANCLE_FRIEND = "AppCancleFriends"
    static let APP_FAVOURITE_DELETE = "AppFavouriteDelete"
    static let APP_CHECK_IN_ADD = "AppCheckInAdd"
    static let APP_TICKER = "AppTicker"
    static let APP_CHECK_IN_LIST = "AppCheckInList"
    static let APP_CHECK_IN_OUT_LIST = "AppCheckInOutList"
    static let APP_AUTO_CHECK_OUT = "AppAutoCheckOut"
    static let APP_BACKGROUND = "AppBackground"
    
    static let APP_GET_ALL_FORUMS  = "getallforam"
    static let APP_GET_MY_FORUMS  = "Getmyforam"
    static let APP_ADD_FORUM  = "Addforam"
    static let APP_ADD_REPLY  = "Addreply"
    static let APP_GET_REPLY  = "getreply"
    static let APP_SHARE = "AppShareContent"
    static let APP_PLAYER_LIKE = "AppPlayerLike"
    
    static let APP_Get_Notification = "AppNotificationLogs"
    static  let Sponser_click_count = "SponsorClickCounts"
    
    static let APP_PHOTO_DELETE = "AppDelete/photo_delete"
    static let APP_get_checkinList = "AppCheckInAllList"
    
    
    static let AppSpectororPlayerJoin = "AppSpectororPlayerJoin"
    static let AppSpectororPlayerEventCheckIn = "AppSpectororPlayerEventCheckIn"
    
    static let APP_CREATE_CROUPCHAT = "Groupchat/create_group"
    static let APP_ADD_Players = "Groupchat/add_group_member"
    
    static let APP_GET_CHATLIST  = "AppChatList"
    static let APP_GET_GROUPCHATLIST = "Groupchat"
    static let APP_get_ADDPLAYERList = "Groupchat/player_list"
    static let GetGroupMemebers = "Groupchat/group_member_list"
    
    //static let Getchathistory = "http://195.201.147.244:7000/" + "chat/PendingMessages"
    
    static let Getchathistory = "http://54.83.90.21:7000/" + "chat/PendingMessages"
    
    static let GetGroupChatHistory =   "http://54.83.90.21:7005/" + "group_chat/PendingMessages"
    
    
    
   // static let SocketUrl = "http://195.201.147.244:7000"
   // static let groupsocketURL = "http://195.201.147.244:7005"
    
    
    
    
     static let SocketUrl = "http://54.83.90.21:7000"
     static let groupsocketURL = "http://54.83.90.21:7005"
    
    
   // static let PUBLISHKEY_LIVE = "pk_live_n9dZgqsWUMjSJjx6SB5GEl4Q"
   // static let PUBLISHKEY_TEST = "pk_test_d1VHmq3QUwqKIKyrTVkOGozA"
    
    //static let PUBLISH_KEY = "pk_test_kIknWwDghYESAT4wXtiIKKMH00W2pSShhA"
    static let PUBLISH_KEY = "pk_test_sX5bdT8pAlIEZwgAALx3Z60e"
    
    
}


