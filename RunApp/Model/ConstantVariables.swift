//
//  ConstantVariables.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import UIKit
import Foundation

/*
 class Constants {
 static let DefultCurrency = "usd"
 static let KEY_LIVE = "Bearer sk_live_hwRAqWmkEMEdk2F73HfUJK3c"
 static let KEY_TEST = "Bearer sk_test_Mg6QdXbI1j7qH5KIjvE0Upph003iGOoFNW"
 static let StripeChargeURL = "https://api.stripe.com/v1/charges"
 }
 */

class ConstantVariables: NSObject {
    struct Constants {
        static let Project_Name = "Sports Run"
        static let currency_INR = "₹"
        
        static let navigationColor = 0x101010 //0x0090CF
        static let nav_red = 0xCD0007 //0x54D57B
        
        static let radius : CGFloat = 15

        static let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        static let DefultCurrency = "usd"
        //static let KEY_LIVE = "Bearer pk_test_kIknWwDghYESAT4wXtiIKKMH00W2pSShhA"
        static let KEY_LIVE =  "sk_live_gAdvd5q0fTpoqo4E918kLhcC00V3lHXGxz"
       // static let KEY_TEST = "Bearer sk_test_Mg6QdXbI1j7qH5KIjvE0Upph003iGOoFNW"
        static let KEY_TEST = "Bearer sk_test_iD4ZaC51RQaRyPcJNLQRdBfZ00Do1JpdHX"
        static let StripeChargeURL = "https://api.stripe.com/v1/charges"
    }
}

class StoredData: NSObject {
    
    static let shared = StoredData()
    var playerId: String!
    var eventId: String!
    var EventStatus: String!
    var totalPoint: String!
}

extension UserDefaults{
    
    func setIsLogin(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func getIsLogin()-> Bool {
        return bool(forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func setUserDict(value: [String : Any]){
        set(value, forKey: UserDefaultsKeys.userData.rawValue)
    }
    
    func getUserDict() -> [String : Any]{
        return dictionary(forKey: UserDefaultsKeys.userData.rawValue) ?? [:]
    }
    
}

enum UserDefaultsKeys : String {
    case userData
    case isUserLogin
}
