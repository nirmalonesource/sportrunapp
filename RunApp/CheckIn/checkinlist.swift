//
//  checkinlist.swift
//  RunApp
//
//  Created by My Mac on 07/10/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight
import CoreLocation

class checkincell: UITableViewCell {
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblsubtitle: UILabel!
    @IBOutlet var btncheckstatus: UIButton!
    
    override func awakeFromNib() {
           lbltitle.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
         lblsubtitle.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
      }
    
}

class checkinlist: UIViewController , UITableViewDataSource, UITableViewDelegate,CLLocationManagerDelegate{

    @IBOutlet var vwpopup: UIView!
    @IBOutlet var tblchecklist: UITableView!
    @IBAction func back_click(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    var checklistarr:NSArray = []
    var LocationID: String?
    var lagitude = Double()
        var logitude = Double()
        var locationManager = CLLocationManager()
       var flag = Bool()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
         {
            if flag {
                flag = false
                if let location = locations.last
                {
                    //print("Found user's location: \(location)")
                    print("latitude: \(location.coordinate.latitude)")
                    print("longitude: \(location.coordinate.longitude)")
                    lagitude = location.coordinate.latitude
                    logitude = location.coordinate.longitude
                    locationManager.stopUpdatingLocation()
                   // getlocationinfo()
                }
            }
                
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
               print("Unable to access your current location")
           }
       
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          vwpopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        tblchecklist.tableFooterView = UIView()
        if CLLocationManager.locationServicesEnabled() == true {
                        
                        if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                            locationManager.requestWhenInUseAuthorization()
                        }
                        
                        locationManager.desiredAccuracy = kCLLocationAccuracyBest
                        locationManager.delegate = self
                        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        locationManager.requestWhenInUseAuthorization()
                        locationManager.startUpdatingLocation()
                        locationManager.requestLocation()
                        flag = true
                        // mapVW.mapType = .hybrid
                    } else {
                        print("Please turn on location services or GPS")
                    }
        AppCheckList()
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
             return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return checklistarr.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "checkincell", for: indexPath) as! checkincell
            
            let dic:NSDictionary = checklistarr.object(at: indexPath.row) as! NSDictionary
            
            cell.lbltitle.text = dic.value(forKey: "LocationName") as? String
            cell.lblsubtitle.text = dic.value(forKey: "LocationAddress") as? String
            
            let CheckIn_Status = dic.value(forKey: "CheckIn_Status") as! String
            
            if CheckIn_Status == "1"
            {
                cell.btncheckstatus.setTitle("CheckOut", for: .normal)
            }
            else
            {
                cell.btncheckstatus.setTitle("CheckIn", for: .normal)
            }
            
            cell.btncheckstatus.tag = indexPath.row
            cell.btncheckstatus.addTarget(self, action: #selector(self.btnCellbtncheckstatusAction(_:)), for: .touchUpInside)
            
           // cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
            cell.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        }
    
     @objc func btnCellbtncheckstatusAction(_ sender : UIButton)
     {
        let dic:NSDictionary = checklistarr.object(at: sender.tag) as! NSDictionary
        print("SelectedData:",dic)
        LocationID = dic.value(forKey: "LocationID") as? String
        AppCheckInAdd(status: dic.value(forKey: "CheckIn_Status") as! String)
        
    }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatAllListView") as! ChatAllListView
//            nextViewController.LocationID = LocationID
//            nextViewController.commentData = commentList[indexPath.row]
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
   //     }
    }
    
    func AppCheckList()
      {
          let parameters = ["Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any]
          
          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                        "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                        ] as [String : Any]
          
          callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_OUT_LIST,
                  method: .post,
                  param: parameters ,
                  extraHeader: header ,
                  completionHandler: { (result) in
                      print(result)
                      if result.getBool(key: "status")
                      {
                        // showToast(uiview: self, msg: result.getString(key: "message"))
                        let arr = result["data"] as! NSArray
                        self.checklistarr = arr
                        self.tblchecklist.reloadData()
                      }
                          
                      else {
                          showToast(uiview: self, msg: result.getString(key: "message"))
                       
                          
                      }
          })
      }
    
    func AppCheckInAdd(status:String)
       {
        let parameters = ["CheckIn_Status" : status ,
                             "Location_ID" : LocationID! ,
                             "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                             "Lat" : lagitude,
                             "Long" : logitude
                             ] as [String : Any]
           
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
           
           callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_ADD,
                   method: .post,
                   param: parameters ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                          showToast(uiview: self, msg: result.getString(key: "message"))
                        
                        self.AppCheckList()
                       }
                           
                       else {
                            showToast(uiview: self, msg: result.getString(key: "message"))
                       }
           })
       }
   
}
