//
//  launchscreen.swift
//  RunApp
//
//  Created by My Mac on 29/08/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight
import SDWebImage

class launchscreen: UIViewController {
    @IBOutlet var imgbackground: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
         fetchbackground()
    }
    

   func fetchbackground()
       {
           if !isInternetAvailable(){
               noInternetConnectionAlert(uiview: self)
           }
           else
           {
               let parameters:[String:Any] = [:]
                let header:[String:Any] = [:]
                    callApi(ServiceList.SERVICE_URL+ServiceList.APP_BACKGROUND,
                            method: .post,
                            param: parameters ,
                            extraHeader: header ,
                            completionHandler: { (result) in
                                print(result)
                                if result.getBool(key: "status")
                                {
                                   let data = result["data"] as! [String:Any]
                                    print(data)
                                    
                                    if UserDefaults.standard.getIsLogin()
                                           {
                                            
                                             let imgURL = data["Image"] as! String
                                            self.imgbackground.sd_setImage(with: URL(string: imgURL)) { (image, error, cache, url) in
                                               // Your code inside completion block
                                               DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
                                                                                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                                                }
                                               
                                           }
                                               
                                           }
                                           else
                                           {
                                                 NightNight.theme = .night
                                                UserDefaults.standard.set(true, forKey: "on")
                                            
                                            let imgURL = data["Image"] as! String
                                                                                     self.imgbackground.sd_setImage(with: URL(string: imgURL)) { (image, error, cache, url) in
                                                                                        // Your code inside completion block
                                                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                                                                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                                                                                             self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                         }
                                                                                        
                                                                                    }
                                            
                                           
                                           }
                                }
                             //   showToast(uiview: self, msg: result["message"]! as! String)
                    })
                    
           }
       }

}
