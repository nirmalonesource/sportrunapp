//
//  ChatBoardViewController.swift
//  RunApp
//
//  Created by My Mac on 13/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
///

import UIKit
import NightNight
import CoreLocation

struct cellData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class chatTableCell: UITableViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var leadingConstrants: NSLayoutConstraint!
    @IBOutlet weak var lblReplies: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
}

class ChatBoardViewController: UIViewController , UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var lblChartboard: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    
    
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var lblAddressDetail: UILabel!
    
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblAvg: UILabel!
    @IBOutlet weak var lblRunSince: UILabel!
    
    @IBOutlet weak var tblChatView: UITableView!
    
    @IBOutlet weak var vwPopupTopComment: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtWriteComment: UITextField!
    @IBOutlet weak var TableHeightConstraints: NSLayoutConstraint!
    
    
    
    
    var rsvpList = [[String : Any]]()
    var rsvpListGet = [[String : Any]]()
     var EventID = ""
    var commentList = [[String:Any]]()
    var LocationDetail = [String : Any]()
    var tableViewData = [cellData]()
    var LocationID: String?
    var EventStatus:String?
    var dictLocation = [String : Any]()
    var firstViewCont:HomeView!
   
    
    var lagitude = Double()
      var logitude = Double()
      var locationManager = CLLocationManager()
     var flag = Bool()
    
    
      
    
   @IBOutlet weak var btnCheckInOutlet: UIButton!
    @IBOutlet var btncheckmark: UIButton!
    
    //MARK:- Web service
    
    func getPlaces()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOCATIONS,
                method: .post,
                param: ["LocationID" : LocationID!] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let placeList = result["data"] as? [[String:Any]] ?? []
                        //2019-05-01 07:07:16
                        
                        for i in 0..<placeList.count {
                            self.lblTitle.text = placeList[i]["LocationName"] as? String ?? ""
                            self.lblAddressDetail.text = placeList[i]["LocationAddress"] as? String ?? ""
//                            let imgPin = placeList[i]["product_image"] as? String ?? ""
//                            let Distance = placeList[i]["product_image"] as? String ?? ""
                             let cretedOn = placeList[i]["RegistrationDate"]as? String ?? ""
                             self.lblRunSince.text = "Run site Since: \(cretedOn)"
                           //  let Since = cretedOn.toDate3()
                             self.lblRunSince.text = "Run site Since: \(cretedOn)"
                             self.lblAvg.text = "\(placeList[i]["LocMode"] as? String ?? "")"
                        }
                        
                       // self.tblView2.reloadData()
                    }
        })
    }
    
    func getComment()
    {
        
        let parameters = ["Location_ID" : LocationID ?? ""] as [String : Any]

        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_MAIN_COMMENT_LIST,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    
                    if result.getBool(key: "status")
                    {
                        //let arrDict = result["data"] as? [[String:Any]] ?? []
                        let GetData:NSDictionary = result["data"] as? NSDictionary ?? [:]
                        let arrDict = GetData.value(forKey: "comment") as? [[String:Any]] ?? []
                        self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                        self.parseLocation(LocationDict: self.dictLocation)
                    
                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
                        self.commentList = sortedArray
                        self.tblChatView.reloadData()
                        self.view.layoutIfNeeded()
                        self.TableHeightConstraints.constant = self.tblChatView.contentSize.height
                        self.view.layoutIfNeeded()
                    }
        })
    }
    
    func parseLocation(LocationDict : [String:Any]) {
        
        self.lblTitle.text = LocationDict["LocationName"] as? String ?? ""
        self.lblAddressDetail.text = LocationDict["LocationAddress"] as? String ?? ""
        self.EventID = LocationDict["EventID"] as? String ?? ""
        let cretedOn = LocationDict["RegistrationDate"] as? String ?? ""
        let Since = cretedOn.toDate3()
        self.lblRunSince.text = "Run site Since: \(Since)"
        
        //                            let imgPin = placeList[i]["product_image"] as? String ?? ""
        //                            let Distance = placeList[i]["product_image"] as? String ?? ""
        
        self.lblAvg.text = "\(LocationDict["LocMode"] as? String ?? "")"
        print("LocationDict: \(LocationDict)")
    }
    
    func addComment()
    {
        let parameters = ["Location_ID" : LocationID! ,
                          "User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                          "Sub_ID" : 0 ,
                          "Comment" : txtWriteComment.text!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_COMMENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.getComment()
                        self.txtWriteComment.text = ""
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func AppCheckInAdd()
    {
        let parameters = ["Location_ID" : LocationID! ,
                          "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "Lat" : lagitude,
                          "Long" : logitude
                          ] as [String : Any]
      
        print(parameters)
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_ADD,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                     //  showToast(uiview: self, msg: result.getString(key: "message"))
                        self.btnCheckInOutlet.setTitle("Checked In", for: .normal)
                                               self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
                                               self.imgLocation.image = UIImage(named: "map_green")
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "checkinlist") as! checkinlist;
                                           nextViewController.LocationID = self.LocationID
                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                        
                    else {
                        showToast(uiview: self, msg: result.getString(key: "message"))
//                        self.btnCheckInOutlet.setTitle("Checked In  ", for: .normal)
//                        self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
//                        self.imgLocation.image = UIImage(named: "map_green")
                        
                    }
                    
        })
    }
    
    func AppCheckInList()
    {
        let parameters = ["Location_ID" : LocationID! ,
                          "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_LIST,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.btnCheckInOutlet.setTitle("Checked In", for: .normal)
                        self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
                      // self.imgLocation.image = UIImage(named: "map_green")
                    }
                        
                    else {
                        self.btnCheckInOutlet.setTitle("Check In", for: .normal)
                        self.btnCheckInOutlet.setTitleColor(UIColor.red, for: .normal)
                       // self.imgLocation.image = UIImage(named: "redLocation")
                    }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
      {
         if flag {
             flag = false
             if let location = locations.last
             {
                 //print("Found user's location: \(location)")
                 print("latitude: \(location.coordinate.latitude)")
                 print("longitude: \(location.coordinate.longitude)")
                 lagitude = location.coordinate.latitude
                 logitude = location.coordinate.longitude
                 locationManager.stopUpdatingLocation()
                // getlocationinfo()
             }
         }
             
     }
     
     func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Unable to access your current location")
        }
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
     
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblTitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
       // vwPopupTopComment.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtWriteComment.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblChartboard.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        
        txtWriteComment.setLeftPaddingPoints(30, strImage: "")
        txtWriteComment.layer.masksToBounds = true
        txtWriteComment.layer.cornerRadius = 20.0
        
        txtWriteComment.setRadiusBorder(color: UIColor.red)
        
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
       // tblView.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        
        if CLLocationManager.locationServicesEnabled() == true {
                   
                   if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                       locationManager.requestWhenInUseAuthorization()
                   }
                   
                   locationManager.desiredAccuracy = kCLLocationAccuracyBest
                   locationManager.delegate = self
                   locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                   locationManager.requestWhenInUseAuthorization()
                   locationManager.startUpdatingLocation()
                   locationManager.requestLocation()
                   flag = true
                   // mapVW.mapType = .hybrid
               } else {
                   print("Please turn on location services or GPS")
               }
        if EventStatus == "0"
        {
            imgLocation.image = UIImage(named: "redLocation")
        }
        else if EventStatus == "1"
        {
            imgLocation.image = UIImage(named: "yellowLocation")
        }
        else
        {
           imgLocation.image = UIImage(named: "map_green")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appGetRsvpLocation()
        if EventStatus == "0"
               {
                   imgLocation.image = UIImage(named: "redLocation")
               }
               else if EventStatus == "1"
               {
                   imgLocation.image = UIImage(named: "yellowLocation")
               }
               else
               {
                  imgLocation.image = UIImage(named: "map_green")
               }
         getComment()
         AppCheckInList()
        self.view.layoutIfNeeded()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.viewDidLayoutSubviews()
        
        TableHeightConstraints.constant = tblChatView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    // MARK: - TextField Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
     // MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtWriteComment.text == ""
        {
            showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
        }
        else
        {
            addComment()
        }
    }
    
    @IBAction func btnAddLocationAction(_ sender: UIButton) {
       
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
//    
//    @IBAction func btn_seemore(_ sender: Any) {
//     
//         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SeeMoreViewController") as! SeeMoreViewController
//               nextViewController.LocationID = LocationID
//               nextViewController.EventID = EventID
//               nextViewController.rsvpDictList = rsvpListGet
//               self.navigationController?.pushViewController(nextViewController, animated: true)
//    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
       
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btn_checkinlist(_ sender: Any) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventCheckInListViewController") as! EventCheckInListViewController
      
        nextViewController.locationid = LocationID 
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    @IBAction func btnEventAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewEventViewController") as! AddNewEventViewController
        nextViewController.LocationDetail = LocationDetail
        nextViewController.LocationID = LocationID
        nextViewController.FViewCont = self.firstViewCont
        nextViewController.EventID1 = EventID
        nextViewController.rsvpListGet1 = rsvpListGet
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
//    @IBAction func BtnGroupAction(_ sender: UIButton) {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatCommentsListView") as! ChatCommentsListView
//        nextViewController.LocationID = LocationID
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//    }
    
     @IBAction func BtnCheckedInAction(_ sender: UIButton) {
        print(sender.title)
       (btnCheckInOutlet.currentTitle)
        
        if btnCheckInOutlet.currentTitle != "Checked In" {
            AppCheckInAdd()
        }
        else{
            
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "checkinlist") as! checkinlist;
        nextViewController.LocationID = self.LocationID
        self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
    }
    
    //Already Checkedin
    
    // MARK: - TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableViewData[section].opened == true {
//            return tableViewData[section].sectionData.count + 1
//        }
//        else{
//            return 1
//        }
        return commentList.count > 6 ?  5 : commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatTableCell", for: indexPath) as! chatTableCell
        
        cell.lblName.text = commentList[indexPath.row]["FirstName"] as? String ?? "Unknown name"
        cell.lblDescription.text = commentList[indexPath.row]["Comment"] as? String
        cell.lblReplies.text = "\(commentList[indexPath.row]["total"] as? String ?? "")Replies"
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        
        let imgURL = ServiceList.IMAGE_URL + "\(commentList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                  completed: nil)
        cell.lblTime.text = "\(commentList[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()

        cell.lblName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
//        let dataIndex = indexPath.row - 1
//        if  indexPath.row == 0 {
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "chatTableCell") as? chatTableCell else { return UITableViewCell()}
//            cell.lblName.text = tableViewData[indexPath.section].title
//            cell.leadingConstrants.constant = 0
//            cell.selectionStyle = .none
//            return cell
//        }
//        else{
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "chatTableCell") as? chatTableCell else { return UITableViewCell()}
//           // cell.textLabel?.text = tableViewData[indexPath.section].sectionData[dataIndex]
//            cell.lblName.text = tableViewData[indexPath.section].sectionData[dataIndex]
//            cell.leadingConstrants.constant = 40
//            //cell.removeSeparatorLeftPadding()
//            cell.selectionStyle = .none
//        }
        
//        cell.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatAllListView") as! ChatAllListView
        nextViewController.LocationID = LocationID
        nextViewController.commentData = commentList[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatCommentsListView") as! ChatCommentsListView
//        nextViewController.LocationID = LocationID
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    
//        if indexPath.row == 0 {
//            if tableViewData[indexPath.section].opened == true {
//                tableViewData[indexPath.section].opened = false
//                let sections = IndexSet.init(integer:indexPath.section)
//                tableView.reloadSections(sections, with: .none) //play around with this
//            }
//
//            else {
//                //use diffent cell identifier if needed
//                tableViewData[indexPath.section].opened = true
//                let sections = IndexSet.init(integer:indexPath.section)
//                tableView.reloadSections(sections, with: .none) //play around with this
//            }
//        }
//
//         tblChatView.reloadData()
//         self.view.layoutIfNeeded()
//         TableHeightConstraints.constant = tblChatView.contentSize.height
//         self.view.layoutIfNeeded()
        
    }
    
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
    
    func appGetRsvpLocation()
       {
           let parameters = ["LocationID": LocationID ?? "","EventID":EventID,"PlayerID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""
               ] as [String : Any]
           
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]

           callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_RSVP_LOCATION,
                   method: .post,
                   param: parameters ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           //let data = result["data"] as? [[String:Any]] ?? []
                           
                           let GetData:NSDictionary = result["data"] as! NSDictionary
                        self.rsvpListGet = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                           print("rsvpListGet:\(self.rsvpListGet)")
                           let jsonarr:NSArray = GetData.value(forKey: "event") as! NSArray
                           if jsonarr.count > 0
                            
                            
                           {
                                let dict1:NSDictionary = jsonarr.lastObject as! NSDictionary
                               self.rsvpList = [[String : Any]]()
                               self.rsvpList.append(dict1 as! [String : Any])
                           }
                          
                        //   let dict2:NSDictionary = jsonarr.object(at: jsonarr.count-2) as! NSDictionary
                          
                          // self.rsvpList.append(dict2 as! [String : Any])
                          
                           
                           self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                            self.parseLocation(LocationDict: self.dictLocation)
                       }
                       else
                       {
                           showToast(uiview: self, msg: result.getString(key: "message"))
                       }
                       
//                       DispatchQueue.main.async {
//                           self.tblUpcomingView.reloadData()
//                         //  self.constTblUpcomingHeight.constant = self.tblUpcomingView.contentSize.height
//                       }
           })
       }
    
    
}

extension UITableViewCell
{
    func removeSeparatorLeftPadding() -> Void
    {
        if self.responds(to: #selector(setter: separatorInset)) // Safety check
        {
            self.separatorInset = UIEdgeInsets.zero
        }
        if self.responds(to: #selector(setter: layoutMargins)) // Safety check
        {
            self.layoutMargins = UIEdgeInsets.zero
        }
    }
}
