//
//  ChatCommentsListView.swift
//  RunApp
//
//  Created by My Mac on 09/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class ChatCommentsListView: UIViewController , UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate
{

    
    @IBOutlet weak var lblchatboard: UILabel!
    
    
    
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var tblChatView: UITableView!
    
    @IBOutlet weak var vwPopupTopComment: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtWriteComment: UITextField!
    var commentList = [[String:Any]]()

    var LocationID: String?
    
    //MARK:- Web service
    
    func getComment()
    {
        let parameters = ["Location_ID" : LocationID!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_MAIN_COMMENT_LIST,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    
        
                    
                    if result.getBool(key: "status")
                    {
//                        let arrDict = result["data"] as? [[String:Any]] ?? []
//                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
//                        self.commentList = sortedArray
                        let GetData:NSDictionary = result["data"] as! NSDictionary
                        let arrDict = GetData.value(forKey: "comment") as? [[String:Any]] ?? []
                        
                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
                        self.commentList = sortedArray
           
                        self.tblChatView.reloadData()
                        
                    }
        })
    }
    
    func addComment()
    {
        let parameters = ["Location_ID" : 1 ,
                          "User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                          "Sub_ID" : 0 ,
                          "Comment" : txtWriteComment.text!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_COMMENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.getComment()
                        self.txtWriteComment.text = ""
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
        
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //hideKeyboardWhenTappedAround()
   vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
    lblchatboard.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
    txtWriteComment.mixedTextColor  = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
        
        txtWriteComment.setLeftPaddingPoints(30, strImage: "")
        txtWriteComment.layer.masksToBounds = true
        txtWriteComment.layer.cornerRadius = 20.0
        
        txtWriteComment.setRadiusBorder(color: UIColor.red)
        
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        tblChatView.tableFooterView = UIView()
        
        getComment()
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- TextField Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtWriteComment.text == ""
        {
            showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
        }
        else
        {
            addComment()
        }
        
    }
    
    //MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatAllListCell", for: indexPath) as! ChatAllListCell
        
        cell.lblName.text = commentList[indexPath.row]["FirstName"] as? String ?? "Unknown name"
        cell.lblDescription.text = commentList[indexPath.row]["Comment"] as? String
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        
        let imgURL = ServiceList.IMAGE_URL + "\(commentList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                 completed: nil)
        cell.lblTime.text = "\(commentList[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()

        cell.lblName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
         cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatAllListView") as! ChatAllListView
        nextViewController.LocationID = LocationID
        nextViewController.commentData = commentList[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
