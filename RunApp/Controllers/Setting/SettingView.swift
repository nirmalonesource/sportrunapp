//
//  SettingView.swift
//  RunApp
//
//  Created by My Mac on 05/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight
//import SwiftTheme

//class BaseCell: UITableViewCell {
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        theme_backgroundColor = GlobalPicker.backgroundColor
//    }
//
//}

class SettingListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var themechangeswitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
        
        if (UserDefaults.standard.object(forKey: "on") != nil)
        {
            themechangeswitch.isOn = true;
        }
        else
        {
            themechangeswitch.isOn = false;
        }
       
//         lblTitle.theme_textColor = GlobalPicker.textColor
//         updateNightSwitch()
//        NotificationCenter.default.addObserver(self, selector: #selector(updateNightSwitch), name: NSNotification.Name(rawValue: ThemeUpdateNotification), object: nil)
    }
    
//    @objc private func updateNightSwitch() {
//        themechangeswitch.isOn = MyThemes.isNight()
//    }
    
   
}

class SettingView: UIViewController , UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    var arrMenu = [String]()
    var ProfileStoreDetail = [[String : Any]]()
    var usertype = ""

    //MARK:- View Life Cycle
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
//        imgBG.setGredient()
         APP_USER_PROFILE()
        vwPopupTop.setRadius(radius: 10)
        btnLogout.setRadius(radius: btnLogout.frame.height/2)
        
        arrMenu = ["My Events" , "Edit Profile" , "Change Password" , "Privacy Policy" , "Notification" , "Purchase History","Night Mode","Points Earned","Tutorial"]
//        tblMenu.theme_backgroundColor = GlobalPicker.backgroundColor
//        tblMenu.theme_separatorColor = ["#000", "#FFF"]
         vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
         tblMenu.mixedSeparatorColor =  MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
        self.view.layoutIfNeeded()
         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("nightmode"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification){
        
        // self.changeTheme()
        
    }
    override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.viewDidLayoutSubviews()
        constTblHeight.constant = tblMenu.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    //MARK:- WEB SERVICE
    
    func APP_USER_PROFILE()
    {
        //let parameters = ["UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
         //   ] as [String : Any]
        let parameters = ["UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "", "FromID": "0"]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_USER_PROFILE,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    
                    if result.getBool(key: "status")
                    {
                        self.ProfileStoreDetail = result["data"] as? [[String:Any]] ?? []
                        
                        for i in 0..<self.ProfileStoreDetail.count {
                            self.usertype = self.ProfileStoreDetail[i]["usertype"] as? String ?? ""
                        }
                        
                    }
                    
                 //   showToast(uiview: self, msg: result["message"]! as! String)
        })
        
    }
    
    //MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Are you sure you want to Logout?")
        
        let action1 = EMAlertAction(title: "Logout", style: .cancel)
        {
            //let domain = Bundle.main.bundleIdentifier!
            
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            
            dictionary.keys.forEach
                {
                    key in
                    if key != "user_data" &&  key != "on" && key != "NightNightThemeKey"
                    {
                        defaults.removeObject(forKey: key)
                    }
                    
            }
            
            UserDefaults.standard.synchronize()
            
            print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
            
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            // Perform Action
        }
        
        alert.addAction(action: action1)
        alert.addAction(action: action2)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- TABLEVIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingListCell", for: indexPath) as! SettingListCell
         cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        cell.lblTitle.text = arrMenu[indexPath.row]
        cell.selectionStyle = .none
        
         cell.themechangeswitch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        if indexPath.row == 6
        {
            cell.themechangeswitch.isHidden = false
        }
        else
        {
            cell.themechangeswitch.isHidden = true
        }
      if  cell.lblTitle.text ==  "Notification"
      {
        cell.lblTitle.textColor = UIColor.darkGray
        }
        if  cell.lblTitle.text ==  "Purchase History"
            {
              
                cell.lblTitle.textColor = UIColor.darkGray
             
        }
        
        return cell
    }
    
    @objc func switchChanged(_ sender : UISwitch!){
        
                // NotificationCenter.default.post(name: Notification.Name("nightmode"), object: nil)
                print("table row switch Changed \(sender.tag)")
                print("The switch is \(sender.isOn ? "ON" : "OFF")")

                if sender.isOn
                    
                {
                    NightNight.theme = .night
                    UserDefaults.standard.set(true, forKey: "on") //Bool
                   //  UIApplication.shared.statusBarStyle = .lightContent
                }
                    
                else
                    
                {
                  //  UserDefaults.standard.set(false, forKey: "on")
                     NightNight.theme = .normal
                     UserDefaults.standard.removeObject(forKey: "on")
                    // UIApplication.shared.statusBarStyle = .default
                }
        
        
       //  MyThemes.switchNight(isToNight: sender.isOn)
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventTableView") as! EventTableView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            else if indexPath.row == 7 {
            
                              let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                              self.navigationController?.pushViewController(nextViewController, animated: true)
            
                          }
        else if indexPath.row == 1 {
            print("usertype: \(usertype)")
            
            if usertype == "Organization" {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpOrganizationView") as! SignUpOrganizationView
                
                nextViewController.dictEdit = ProfileStoreDetail
                print(nextViewController.dictEdit)
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
                
            else if usertype == "User" {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! signUpViewController
                nextViewController.dictEdit = ProfileStoreDetail
                print(nextViewController.dictEdit)
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
            
        else if indexPath.row == 2 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordView") as! ChangePasswordView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            
        else if indexPath.row == 3 {
            
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyView") as! PrivacyPolicyView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        
        }
            else if indexPath.row == 4 {

      //  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
//                 self.navigationController?.pushViewController(nextViewController, animated: true)
            
                  }
            else if indexPath.row == 8 {

                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
                             self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                              }
        else {
            print("Cell is Pressed")
        }
        
    }

}
