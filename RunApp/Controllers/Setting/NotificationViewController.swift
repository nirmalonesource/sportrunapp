//
//  NotificationViewController.swift
//  RunApp
//
//  Created by Mac on 12/03/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import  NightNight


class tableNotificationcell: UITableViewCell {

    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var lblpointsearned: UILabel!
}

class NotificationViewController: UIViewController, UITableViewDelegate , UITableViewDataSource  {

    
    @IBOutlet weak var lbltotalpointsearned: UILabel!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var tblNotification : UITableView!
    
    var notificationList = [[String : Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        tblNotification.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        lbltotalpointsearned.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        Notification()
        // Do any additional setup after loading the view.
    }
    func Notification()
     {
         let parameters = ["Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
           
             ] as [String : Any]
         
         let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                       "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                       ] as [String : Any]
         
         self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_Get_Notification,
                      method: .post,
                      param: parameters,
                      extraHeader: header) { (result) in
                      
                          print(result)
                         
                         if result.getBool(key: "status")
                         {
                           
                            self.notificationList = result["date"] as? [[String : Any]] ?? []
                            let totalpoint =  result["TotalPoints"] as? String ?? ""
                            self.lbltotalpointsearned.text! = "Points Earned " + totalpoint
                         }
                         
                         self.tblNotification.reloadData()
                        // showToast(uiview: self, msg: result.getString(key: "message"))
         }
     }
     
      //MARK:- TABLEVIEW METHODS
     func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return notificationList.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableNotificationcell", for: indexPath) as! tableNotificationcell
        
         cell.lbltitle.text = notificationList[indexPath.row]["Activity"] as? String ?? ""
         let points = notificationList[indexPath.row]["Points"] as? String ?? ""
        
         cell.lblpointsearned.text = points  + " Points"
            
         cell.lbltitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         cell.lblpointsearned.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
     
        
         cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
         
         return cell
     }

    
    @IBAction func btn_back(_ sender: Any) {
         
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
