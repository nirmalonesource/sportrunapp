//
//  AddNewLocationController.swift
//  RunApp
//
//  Created by My Mac on 07/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0
import NightNight
import GooglePlacesSearchController

protocol DataProtocol {
    //func UpdateLocationData(latitude: Double , longitude : Double)
    func UpdateLocationData(AddLocationDetail: [String : Any])
}
extension AddNewLocationController: GooglePlacesAutocompleteViewControllerDelegate {
    func viewController(didAutocompleteWith place: PlaceDetails) {
        print(place.description)
        print("Place name: \(place.name ?? "")")
               print("Place address: \(place.formattedAddress ?? "")")
               print("Place address: \(place.coordinate)")
             //  print("Place attributions: \(String(describing: place.attributions))")
               self.txtZipcode.text = place.postalCode
        txtAddress.text = place.formattedAddress
        addressCoordinate = place.coordinate!
             //  getPostalCode()
               dismiss(animated: true, completion: nil)
       // placesSearchController.isActive = false
    }
}

class AddNewLocationController: UIViewController , UITextFieldDelegate , UIGestureRecognizerDelegate
{
    var delegate: DataProtocol?
    
    @IBOutlet weak var scrlVW: UIScrollView!
    
    @IBOutlet var lblnewlocation: UILabel!
    @IBOutlet weak var txtLocationName: UITextField!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var btnPublicOutlet: UIButton!
    @IBOutlet weak var btnAddLocation: UIButton!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    var stateData = [[String : Any]]()
    var cityData = [[String : Any]]()
    var strStateID , strCityID : String?
    var addressCoordinate = CLLocationCoordinate2D()
    var autocompletecontroller = GMSAutocompleteViewController()
    @IBOutlet weak var btnBack: UIButton!
    
    var DictAddLocationDetail = [String:Any]()
    
    @IBOutlet weak var btnHeightState: NSLayoutConstraint!
    
    var postalCode:String! = "" {
        didSet {
            print("Postal code is: \(self.postalCode!)")
            // and whatever other code you need here.
        }
    }
    
    //MARK:- Web Service
    
    func getStateList()
    {
        callApi(ServiceList.SERVICE_URL+ServiceList.STATE_LIST,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.stateData = result["data"] as? [[String:Any]] ?? []
                    }
        })
        
    }
    
//    func getCityList()
//    {
//        callApi(ServiceList.SERVICE_URL+ServiceList.CITY_LIST,
//                method: .post,
//                param: ["state_id" : strStateID ?? ""] ,
//                completionHandler: { (result) in
//                    print(result)
//                    if result.getBool(key: "status")
//                    {
//                        self.cityData = result["data"] as? [[String:Any]] ?? []
//                        if self.cityData.count != 0
//                        {
//                            self.btnCity.setTitle(self.cityData[0]["city_name"] as? String, for: .normal)
//                            self.strCityID = self.cityData[0]["city_id"] as? String ?? ""
//
//                            ActionSheetStringPicker.show(withTitle: "Select City", rows: self.cityData.map({ $0["city_name"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
//                                picker, indexes, values in
//                                self.btnCity.setTitle("\(values!)", for: .normal)
//                                self.strCityID = self.cityData[indexes]["city_id"] as? String ?? ""
//                                return
//                            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: self.btnCity)
//                        }
//                    }
//        })
//    }
    
    func addLocation()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        let parameters = ["LocationName":txtLocationName.text ?? "" ,
                          "LocationAddress":txtAddress.text ?? "" ,
                          "ZipCode":txtZipcode.text ?? "" ,
                          "City":strCityID ?? "" ,
                          "State":strStateID ?? "" ,
                          "LocMode":btnPublicOutlet.currentTitle?.lowercased() ?? "" ,
                          "longitude":addressCoordinate.longitude ,
                          "latitude":addressCoordinate.latitude ,
                           "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any]
       
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_LOCATION,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.DictAddLocationDetail = result["data"] as? [String:Any] ?? [:]
                        
                        print("LocationDetail: \(self.DictAddLocationDetail)")
                        
                        for vc in (self.navigationController?.viewControllers ?? []) {
                            if vc is HomeView {
                                self.delegate?.UpdateLocationData(AddLocationDetail: self.DictAddLocationDetail)
                                _ = self.navigationController?.popToViewController(vc, animated: true)
                                break
                            }
                        }
                    }
                    
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // hideKeyboardWhenTappedAround()
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
         lblnewlocation.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
         txtLocationName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
         txtAddress.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
         txtZipcode.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
         btnPublicOutlet.setMixedTitleColor(MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont), forState: UIControl.State())
        
        
       // ActionSheetStringPicker.mixedBackgroundColor = MixedColor(normal: 0xffffff, night: 0x000000)
        
        view.layoutIfNeeded()
//        imgBG.setGredient()
        
        for view in scrlVW.subviews as [UIView] {
            if let txt = view as? UITextField {
                txt.setRadius(radius: 10)
                txt.setRadiusBorder(color: UIColor.lightGray)
                txt.setLeftPaddingPoints(15, strImage: "")
            }
        }
        
//   btnBack.transform = CGAffineTransform(scaleX: -1, y: 1)

       // btnState.setRadius(radius: 10)
       // btnState.setRadiusBorder(color: UIColor.lightGray)
        
       // btnCity.setRadius(radius: 10)
        //btnCity.setRadiusBorder(color: UIColor.lightGray)
        
        btnPublicOutlet.setRadius(radius: 10)
        btnPublicOutlet.setRadiusBorder(color: UIColor.lightGray)
        btnAddLocation.setRadius(radius: 10)
        
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
//        let filter = GMSAutocompleteFilter()
//        autocompletecontroller.delegate = self
//       // filter.type = .establishment //  //suitable filter type
//        filter.type = .address
//
//        //filter.country = "uk|country:fr|country:us"
//       // filter.country = "USA"  //appropriate country code  USA,IN,AU
//        autocompletecontroller.autocompleteFilter = filter
        
        let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        gestureRecognizer.delegate = self
        gestureRecognizer.direction = .right
        self.view.addGestureRecognizer(gestureRecognizer)
        
       // getStateList()
        self.view.layoutIfNeeded()
        
    }
    
    /*
     let filter = GMSAutocompleteFilter()
     filter.type = GMSPlacesAutocompleteTypeFilter.City
    //Another code
     
     @IBAction func clickMe(sender: UIButton) {
     let autocompletecontroller = GMSAutocompleteViewController()
     autocompletecontroller.delegate = self
     let filter = GMSAutocompleteFilter()
     filter.type = .Establishment  //suitable filter type
     filter.country = "SG"  //appropriate country code
     autocompletecontroller.autocompleteFilter = filter
     self.presentViewController(autocompletecontroller, animated: true, completion: nil)
     }
 */
    
    //MARK:- METHODS ACTION
    
    @IBAction func btnPublicClicked(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select Mode", rows: ["Public Park" , "Private Gym", "School Building", "Arena", "Sports Complex","School Playground"] , initialSelection: 0, doneBlock: {
            picker, indexes, values in
            self.btnPublicOutlet.setTitle("\(values!)", for: .normal)
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    //MARK:- Button Action
    
    @IBAction func AddLocationClicked(_ sender: UIButton) {
        
        validation()
        
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewEventViewController") as! AddNewEventViewController
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnStateAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        if stateData.count != 0
//        {
//            ActionSheetStringPicker.show(withTitle: "Select State", rows: stateData.map({ $0["state_name"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
//                picker, indexes, values in
//                self.btnState.setTitle("\(values!)", for: .normal)
//                self.strStateID = self.stateData[indexes]["state_id"] as? String ?? ""
//
//                return
//            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
//        }
    }
    
    @IBAction func btnCityAction(_ sender: UIButton) {
        self.view.endEditing(true)
       // getCityList()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- Textfield Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //            autocompletecontroller.delegate = self
        //            self.present(autocompletecontroller, animated: true, completion: nil)
        if textField == txtAddress
        {
            let controller = GooglePlacesSearchController(delegate: self,
            apiKey: "AIzaSyDO1hi6nmBAo9Ai8wcsU16lm26DiOglmYo",
            placeType: .all)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    /*
     @objc func autocompleteClicked(_ sender: UIButton) {
     let autocompleteController = GMSAutocompleteViewController()
     autocompleteController.delegate = self
     
     // Specify the place data types to return.
     let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
     UInt(GMSPlaceField.placeID.rawValue))!
     autocompleteController.placeFields = fields
     
     // Specify a filter.
     let filter = GMSAutocompleteFilter()
     filter.type = .address
     autocompleteController.autocompleteFilter = filter
     
     // Display the autocomplete view controller.
     present(autocompleteController, animated: true, completion: nil)
     }
    */
   
    //MARK:- Other method
    func validation()
    {
        self.view.endEditing(true)
        if txtLocationName.text == "" && txtAddress.text == "" && txtZipcode.text == ""
        {
            //&& btnState.currentTitle == "State" && btnCity.currentTitle == "City"
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        else if txtLocationName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter location", isTwoButton: false)
        }
            
        else if txtAddress.text == ""
        {
            showAlert(uiview: self, msg: "Please enter address", isTwoButton: false)
        }
            
        else if txtZipcode.text == ""
        {
            showAlert(uiview: self, msg: "Please enter zip code", isTwoButton: false)
        }
//        else if btnState.currentTitle == "State"
//        {
//            showAlert(uiview: self, msg: "Please select state", isTwoButton: false)
//        }
//        else if btnCity.currentTitle == "City"
//        {
//            showAlert(uiview: self, msg: "Please select city", isTwoButton: false)
//        }
        else
        {
            addLocation()
        }
    }

}

/*
 let autocompleteController = GMSAutocompleteViewController()
 autocompleteController.delegate = self
 
 // Specify the place data types to return.
 let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
 UInt(GMSPlaceField.placeID.rawValue))!
 autocompleteController.placeFields = fields
 
 // Specify a filter.
 let filter = GMSAutocompleteFilter()
 filter.type = .address
 autocompleteController.autocompleteFilter = filter
 
 // Display the autocomplete view controller.
 present(autocompleteController, animated: true, completion: nil)
 
 extension ViewController: GMSAutocompleteViewControllerDelegate {
 
 // Handle the user's selection.
 func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
 print("Place name: \(place.name)")
 print("Place ID: \(place.placeID)")
 print("Place attributions: \(place.attributions)")
 dismiss(animated: true, completion: nil)
 }
 
 func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
 // TODO: handle the error.
 print("Error: ", error.localizedDescription)
 }
 
 // User canceled the operation.
 func wasCancelled(_ viewController: GMSAutocompleteViewController) {
 dismiss(animated: true, completion: nil)
 }
 
 // Turn the network activity indicator on and off again.
 func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
 UIApplication.shared.isNetworkActivityIndicatorVisible = true
 }
 
 func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
 UIApplication.shared.isNetworkActivityIndicatorVisible = false
 }
 
 }
 */

extension AddNewLocationController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place address: \(place.formattedAddress ?? "")")
        print("Place address: \(place.coordinate)")
        print("Place attributions: \(String(describing: place.attributions))")
        
        txtAddress.text = place.formattedAddress ?? ""
        addressCoordinate = place.coordinate
       
        getPostalCode()
      
        dismiss(animated: true, completion: nil)
    }
  
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    func getPostalCode()
    {
        let location: String = txtAddress.text ?? ""
        let geocoder: CLGeocoder = CLGeocoder()
        
//        let geocoder = CLGeocoder()
//        geocoder.geocodeAddressString(location) {
//            (placemarks, error) -> Void in
//            // Placemarks is an optional array of CLPlacemarks, first item in array is best guess of Address
//
//            if let placemark = placemarks?[0] {
//                print(placemark.addressDictionary!)
//                let postal_code : String = placemark.postalCode ?? ""
//                self.txtZipcode.text = postal_code
//                self.strStateID = placemark.administrativeArea ?? ""
//                self.strCityID = placemark.locality ?? ""
//            }
//        }
        
        geocoder.geocodeAddressString(location, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) -> Void in

            if placemarks != nil
            {
                if ((placemarks?.count)! > 0) {
                    let placemark: CLPlacemark = (placemarks?[0])!
                  //  let postal_code : String = placemark.postalCode ?? ""
                    self.postalCode = placemark.postalCode ?? ""
                    self.strStateID = placemark.administrativeArea ?? ""
                    self.strCityID = placemark.locality ?? ""

                    print(self.postalCode)
                    self.txtZipcode.text = self.postalCode
                   // self.txtZipcode.text = postal_code
                }
            }
        })
    }
    
//    func getPostalCode()
//    {
//        let location: String = txtAddress.text ?? ""
//        let geocoder: CLGeocoder = CLGeocoder()
//        geocoder.geocodeAddressString(location, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) -> Void in
//            if placemarks != nil
//            {
//                if ((placemarks?.count)! > 0) {
//                    let placemark: CLPlacemark = (placemarks?[0])!
//                    let postal_code : String = placemark.postalCode ?? ""
//
//                    print(postal_code)
//                    self.txtZipcode.text = postal_code
//                }
//            }
//        } )
//    }
    
    @objc func handleSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
        
        /*
            let location = CLLocation()
         
            location.addressDictionary { dictionary in
         
            let city = dictionary?["City"] as? String
            let street = dictionary?["Street"] as? String
          }
       */
        
    }
    
}



