//
//  AddNewEventViewController.swift
//  RunApp

//
//  Created by My Mac on 08/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
///

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0
import NightNight


class AddNewEventViewController: UIViewController , UIGestureRecognizerDelegate
{
    
    
    
    var rsvpListGet1 = [[String : Any]]()
     var EditParam = [String : Any]()
    var EventID1 = ""
    var LocationID1 = ""
    var IsEdit =  ""
    @IBOutlet weak var lbladdnewevent: UILabel!
    
    
    
    
    @IBOutlet weak var btnupcomingrun: UIButton!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var scrlVW: UIScrollView!
    @IBOutlet weak var btnMaxPlayer: UIButton!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtPlayer: UITextField!
    
    @IBOutlet weak var btnPaid: UIButton!
    @IBOutlet weak var txtFess: UITextField!
    @IBOutlet weak var btnDateTime: UIButton!
    @IBOutlet weak var btnLoc: UIButton!
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var textTopHeight: NSLayoutConstraint!
    @IBOutlet weak var textHeight: NSLayoutConstraint!
    @IBOutlet weak var playerTopHeight: NSLayoutConstraint!
    
    @IBOutlet weak var playerHeight: NSLayoutConstraint!
    
    var levelOfCompositionList = [[String : Any]]()
    var strSelectedDate , strEndDate , strLOCId : String?
    var LocationDetail = [String : Any]()
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    var LocationID: String?
    
    var FViewCont:HomeView!
  
    
    
    //MARK:- Web Service
    
    func addEvent()
    {
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
    
    let parameters = [
                      "EventFromDate" : strEndDate ?? "" ,
                      "CreationUserID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","clvlid":strLOCId!,
                       "EvenetName":txtName.text ?? "" ,"LocationID":LocationID ?? "","MaxPlayer":btnMaxPlayer.currentTitle ?? "" ,
                            "EventType":btnPaid.currentTitle ?? "" ,
                            "Amt":txtFess.text ?? "" ,
                            "PlayerAmt":txtPlayer.text! ,
                            "EventDate" : strSelectedDate ?? "",
                            "EventID" : EventID1
                             
                          //"event_to_date" : strEndDate ?? "" ,
                         
        ]
        
        
        
        print(parameters)
        
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_EVENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        for vc in (self.navigationController?.viewControllers ?? []) {
                            if vc is HomeView {
                               // self.FViewCont.a = "Yellow"
                               // self.FViewCont.startDate = self.strSelectedDate ?? ""
                                //self.FViewCont.EndDate = self.strEndDate ?? ""
                                StoredData.shared.EventStatus = "Event"
                                _ = self.navigationController?.popToViewController(vc, animated: true)
                                break
                                
                            }
                        }
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func getLOCList()
    {
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOC_LIST,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.levelOfCompositionList = result["data"] as? [[String:Any]] ?? []
                    }
        })
    }
    
    
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lbladdnewevent.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtFess.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtPlayer.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnLoc.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnPaid.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnEndDate.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
                btnAddEvent.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
                btnDateTime.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
                btnLoc.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnMaxPlayer.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
          btnupcomingrun.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
         btnBack.setMixedImage(MixedImage(normal:"left-arrow-B", night:"left-arrow"), forState: .normal)
       // btnupcomingrun.layer.borderWidth = 0
        
        
        //  lblName.text = LocationDetail["LocationName"]as? String ?? ""
        //  lblAddress.text = LocationDetail["LocationAddress"]as? String ?? ""
        
        
        
        self.view.layoutIfNeeded()
        let userororganization = UserDefaults.standard.getUserDict()["usertype"] as?
                   String ?? ""
                   
                  if userororganization == "User"
                  {
                   
                   btnPaid.isUserInteractionEnabled = false
                   self.btnPaid.setTitle("Free", for: .normal)
                   self.textHeight.constant = 0
                   self.playerHeight.constant = 0
                   self.textTopHeight.constant = 0
                   self.playerTopHeight.constant = 0
                              
                                  
                      
               }
//        imgBG.setGredient()
        
        for view in scrlVW.subviews as [UIView] {
            if let txt = view as? UITextField {
                txt.setLeftPaddingPoints(15, strImage: "")
                txt.setRadius(radius: 10)
                txt.setRadiusBorder(color: UIColor.gray)
            }
        }
        
        for view in scrlVW.subviews as [UIView] {
            if let btn = view as? UIButton {
                if btn != btnAddEvent || btn != btnupcomingrun
                {
                    btn.setRadius(radius: 10)
                    btn.setRadiusBorder(color: UIColor.gray)
                }
            }
        }
        
        vwPopupTop.setRadius(radius: 10)
        btnAddEvent.setRadius(radius: btnAddEvent.frame.height/2)
        btnupcomingrun.setRadius(radius: btnupcomingrun.frame.height/2)
        
        let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        gestureRecognizer.delegate = self
        gestureRecognizer.direction = .right
        self.view.addGestureRecognizer(gestureRecognizer)
        
        getLOCList()
        print(EditParam)
        if IsEdit == "true"
        {
            LocationID =   EditParam["LocationID"] as? String ?? ""
            txtName.text = EditParam["EvenetName"] as? String ?? ""
            let values = EditParam["MaxPlayer"] as? String ?? ""
            self.btnMaxPlayer.setTitle("\(values)", for: .normal)
            
            let strstartdate = EditParam["EventDate"] as? String ?? ""
            let enddate = EditParam["EventFromDate"] as? String ?? ""
            
            
           let dateFormatter1 = DateFormatter()
          dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm:ss"
            
            if strstartdate != "" {
//                let date: Date? = dateFormatter1.date(from: "\(strstartdate as! String)")!
//                btnDateTime.setTitle(dateFormatter1.string(from: date!), for: .normal)
                 let strstartdate = self.EditParam["EventDate"] as? String ?? "" != "0000-00-00 00:00:00" ? self.setDateWithFormat(strDate:self.EditParam["EventDate"] as? String ?? "" ?? "") : ""
                  btnDateTime.setTitle(strstartdate, for: .normal)
                strSelectedDate = self.EditParam["EventDate"] as? String ?? ""
                
            }
            if enddate != ""
            {
              let enddate = self.EditParam["EventFromDate"] as? String ?? "" != "0000-00-00 00:00:00" ? self.setDateWithFormat(strDate:self.EditParam["EventFromDate"] as? String ?? "" ?? "") : ""
                btnEndDate.setTitle(enddate, for: .normal)
                strEndDate = self.EditParam["EventFromDate"] as? String ?? ""
            }
           
            let Loc = EditParam["CLVLTitle"] as? String ?? ""
            self.btnLoc.setTitle("\(Loc)", for: .normal)
            self.strLOCId = EditParam["CLVLID"] as? String ?? ""
            
            
            EventID1 = EditParam["EventID"] as? String ?? ""
            
            let userororganization = UserDefaults.standard.getUserDict()["usertype"] as?
             String ?? ""
             
            if userororganization != "User"
            {
                txtFess.text =  self.EditParam["Amt"] as? String ?? ""
                txtPlayer.text = self.EditParam["PlayerAmt"] as? String ?? ""
                txtFess.isUserInteractionEnabled = false
                txtPlayer.isUserInteractionEnabled = false
            }
            btnAddEvent.setTitle("Update Event", for: .normal)
        }
        
        self.view.layoutIfNeeded()
        
        
    }
    
    @IBAction func btn_upcomingRun(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SeeMoreViewController") as! SeeMoreViewController
    
            nextViewController.LocationID = LocationID
            nextViewController.EventID = EventID1
        nextViewController.rsvpDictList = rsvpListGet1
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    //MARK:- Button Action
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
        validation()
    }
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "MMM dd , yyyy h:mm a"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24

    }
    
    
    
    @IBAction func btnMaxPlayerAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrPlayer = [Int]()
        for i in 1..<100
        {
            arrPlayer.append(i)
        }
        
        if arrPlayer.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Maximum No of Player", rows: arrPlayer as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnMaxPlayer.setTitle("\(values!)", for: .normal)
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnPaidAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
       
       
        ActionSheetStringPicker.show(withTitle: "Select Type", rows: ["Paid" , "Free"] , initialSelection: 0, doneBlock: {
            
            picker, indexes, values in
            self.btnPaid.setTitle("\(values!)", for: .normal)
            if self.btnPaid.currentTitle == "Free" {
                self.textHeight.constant = 0
                self.playerHeight.constant = 0
                self.textTopHeight.constant = 0
                self.playerTopHeight.constant = 0
                
            }
                
            else {
                
                self.textHeight.constant = 50
                self.playerHeight.constant = 50
                self.textTopHeight.constant = 15
                self.playerTopHeight.constant = 15
                
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func btnDateTimeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Select Start Date:", datePickerMode: .dateAndTime, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd , yyyy h:mm a"
            
            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
//            let dateString = String(describing: value!)
//            let date: Date = dateFormatterGet.date(from:dateString)!
            print(dateFormatter.string(from: date!))
            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm"
        //    dateFormatter1.timeZone = TimeZone(identifier: "UTC")
            
            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
            self.strSelectedDate = (dateFormatter1.string(from: date1!))
            print("strSelectedDate: \(String(describing: self.strSelectedDate))")
            
            return
        }, cancel: { ActionStringCancelBlock in return },
           origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func btnEndDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Select End Date:", datePickerMode: .dateAndTime, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            let dateFormatter = DateFormatter()
           // dateFormatter.dateFormat = "MMM dd , yyyy HH:mm"
             dateFormatter.dateFormat = "MMM dd , yyyy h:mm a"
            
            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
            print(dateFormatter.string(from: date!))
            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm"
            
            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
            self.strEndDate = (dateFormatter1.string(from: date1!))
            
            return
        }, cancel: { ActionStringCancelBlock in return },
           origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func btnLOCAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        if levelOfCompositionList.count != 0
//        {
//            ActionSheetStringPicker.show(withTitle: "Select LOC", rows: levelOfCompositionList.map({ $0["CLVLDesc"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
//                picker, indexes, values in
//                self.btnLoc.setTitle("\(values!)", for: .normal)
//                self.strLOCId = self.levelOfCompositionList[indexes]["CLVLID"] as? String ?? ""
//
//                return
//            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
//        }
        
        if levelOfCompositionList.count != 0
               {
                   ActionSheetStringPicker.show(withTitle: "Select LOC", rows: levelOfCompositionList.map({ $0["CLVLTitle"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                       picker, indexes, values in
                       self.btnLoc.setTitle("\(values!)", for: .normal)
                       self.strLOCId = self.levelOfCompositionList[indexes]["CLVLID"] as? String ?? ""
                       
                       return
                   }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
               }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Other method
    func validation()
    {
        view.endEditing(true)
        
        if txtName.text == "" && btnMaxPlayer.currentTitle == "Max Player" && txtFess.text == "" && strSelectedDate == "" && strEndDate == "" && strLOCId == ""
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        else if txtName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter name", isTwoButton: false)
        }
        else if btnMaxPlayer.currentTitle == "Max Player"
        {
            showAlert(uiview: self, msg: "Please select maximum no of player", isTwoButton: false)
        }
        else
        {
            if btnPaid.currentTitle ?? "" == "Paid"
            {
                if txtFess.text == ""
                {
                    showAlert(uiview: self, msg: "Please enter Spectator fees", isTwoButton: false)
                }
                    
                else if txtPlayer.text == ""
                {
                    showAlert(uiview: self, msg: "Please enter Player fees", isTwoButton: false)
                }
                    
                else
                {
                    innerValidation()
                }
            }
            else
            {
                innerValidation()
            }
        }
        
    }
    
    func innerValidation()
    {
        if strSelectedDate == nil
        {
            showAlert(uiview: self, msg: "Please select start date", isTwoButton: false)
        }
        else if strEndDate == nil
        {
            showAlert(uiview: self, msg: "Please select end date", isTwoButton: false)
        }
        else if strLOCId == nil
        {
            showAlert(uiview: self, msg: "Please select LOC", isTwoButton: false)
        }
        else
        {
            addEvent()
        }
    }
    
    @objc func handleSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}
