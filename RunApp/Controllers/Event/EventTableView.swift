//
//  EventTableView.swift
//  RunApp
///
//  Created by My Mac on 07/06/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class tableEventCell: UITableViewCell {

    @IBOutlet weak var lblLoC: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblPublish: UILabel!
    @IBOutlet weak var btnSelectOutlet: UIButton!

    @IBOutlet weak var btnedit: UIButton!
}

class CollectionAdCell: UICollectionViewCell {
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgImage: UIImageView!
}

class EventTableView: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    var CancelLocationID = String()
    var CancelEventID = String()
     var EventId = String()
    
    @IBOutlet weak var btneditevent: UIButton!
    @IBOutlet weak var btnedit: UILabel!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var scrlVW: UIScrollView!

    @IBOutlet weak var tblEventList: UITableView!
    @IBOutlet weak var collectionAdvertView: UICollectionView!
     var eventList = [[String : Any]]()
    
    var timer:Timer = Timer()
    var scrollingTimer = Timer()
    var timerBanner = Timer()
    var scrollIndex = Int()
    var pageNumber = Int()
    
    
    private var currentIndex: Int?
    private var currentMaxItemsCount: Int = 0
    
   
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        tblEventList.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        
        
        Player_Wise_Event()
        vwPopupTop.setRadius(radius: 10)
        tblEventList.tableFooterView = UIView()
        
////        scrollIndex = 0
//        timerBanner = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
   
    func runTimer() {
        timer = Timer(fire: Date(), interval: 5.0, repeats: true, block: { (Timer) in
            //self.vehicleArray.removeAll()
            DispatchQueue.main.async {
                self.checkFireBase()
            }
        })
        
//        RunLoop.current.add(timer!, forMode: .defaultRunLoopMode)
//        self.collectionAdvertView.reloadData()
        
    }
    
    func checkFireBase(){
        //Fetch the latest data and update the data Source of your Table view
        self.collectionAdvertView.reloadData()
    }
    
    
    
    //MARK:- Web Service
    func Player_Wise_Event()
    {
        let parameters = ["PlayerID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
          
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_WISE_EVENT,
                     method: .post,
                     param: parameters,
                     extraHeader: header) { (result) in
                     
                         print(result)
                        
                        if result.getBool(key: "status")
                        {
                            self.eventList = result["data"] as? [[String : Any]] ?? []
                            
                        }
                        
                        self.tblEventList.reloadData()
                       // showToast(uiview: self, msg: result.getString(key: "message"))
        }
    }
    
     //MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableEventCell", for: indexPath) as! tableEventCell
       
        cell.lblEventName.text = eventList[indexPath.row]["EvenetName"] as? String ?? ""
        cell.lblDate.text = eventList[indexPath.row]["EventDate"] as? String ?? ""
        cell.lblEventName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblLocation.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblDate.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblPublish.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblLoC.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        cell.lblLoC.text = eventList[indexPath.row]["CLVLTitle"] as? String ?? ""
        
          let cretedOn = eventList[indexPath.row]["EventDate"] as? String ?? ""
        
        let iseventstarted =  eventList[indexPath.row]["isEventStared"] as? String ?? ""
        print(iseventstarted)
        
        if iseventstarted == "1"
        {
            cell.btnedit.isHidden = true
        }
        else
        {
            cell.btnedit.isHidden = false
        }
        
        
        if cretedOn != "" {
            cell.lblDate.text = cretedOn.toDate5()
        }
        
        cell.btn_Cancel.tag = indexPath.row
        cell.btnedit.tag = indexPath.row
        cell.btn_Cancel.addTarget(self, action: #selector(self.btnCancelEvent), for: .touchUpInside)
         cell.btnedit.addTarget(self, action: #selector(self.btnEditEvent), for: .touchUpInside)
        
        let EventStatus = eventList[indexPath.row]["EventStatus"] as? String ?? ""
        
        if EventStatus == "1"
        {
            cell.lblPublish.text = "Pending"
            cell.btn_Cancel.isHidden = false
            
        }
        else if EventStatus == "2"
        {
            cell.lblPublish.text = "Approved"
            cell.btn_Cancel.isHidden = false
            
        }
        else if EventStatus == "3"
        {
            cell.lblPublish.text = "Rejected"
        }
        else if EventStatus == "4"
        {
            cell.lblPublish.text = "Completed"
        }
        else if EventStatus == "5"
        {
            cell.lblPublish.text = "Deactivated"
        }
        else if EventStatus == "6"
               {
                   cell.btn_Cancel.isHidden = true
                   cell.lblPublish.text = "Cancelled"
               }
        else
        {
        //   cell.lblPublish.text = "Pending"
        }
        
        
        
          cell.lblLocation.text = eventList[indexPath.row]["LocationName"] as? String ?? ""
       
        cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailView") as! EventDetailView
        nextViewController.Event_ID = eventList[indexPath.row]["EventID"] as? String ?? ""
        nextViewController.Event_Name = eventList[indexPath.row]["EvenetName"] as? String ?? ""
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    //MARK: - BUTTON METHODS
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func btnCancelEvent(_ sender : UIButton)
       {
       
        
        CancelLocationID = eventList[sender.tag]["LocationID"] as? String ?? ""
        CancelEventID = eventList[sender.tag]["EventID"] as? String ?? ""
        cancelUserEvent()
        
      }
    @objc func btnEditEvent(_ sender : UIButton)
        {
        
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewEventViewController") as! AddNewEventViewController
            //nextViewController.Event_ID = eventList[sender.tag]["EventID"] as? String ?? ""
            nextViewController.IsEdit = "true"
            nextViewController.EditParam = eventList[sender.tag] as? [String:Any] ?? [:]
            self.navigationController?.pushViewController(nextViewController, animated: true)
         
         
       }
    
     func cancelUserEvent()
        {
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
        
            let parameters = [
                "EventID" : CancelEventID , "LocationID": CancelLocationID ,
                "PlayerID":UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ]
            
            
            
            print(parameters)
            
            callApi(ServiceList.SERVICE_URL+ServiceList.Cancel_UserEvent,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.Player_Wise_Event()
                           
                          
                        }
                        showToast(uiview: self, msg: result.getString(key: "message"))
            })
        }
    
}

extension EventTableView: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
     //MARK:- CollectionView Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionAdCell", for: indexPath) as! CollectionAdCell
       
        var rowIndex = indexPath.row
        let numberOfRecords:Int = 10 - 1

        if (rowIndex < numberOfRecords) {
            
            rowIndex = (rowIndex + 1)
            
        }

        else {
           //rowIndex = 0
             //collectionAdvertView.reloadData()
        }

//        if indexPath.row == 30 - 1 {
//            collectionAdvertView.reloadData()
//        }

        
        scrollingTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(EventTableView.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)

        return cell
    }
    
    @objc func startTimer(theTimer: Timer) {
        
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseOut, animations: {
            self.collectionAdvertView.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int, section: 0), at: .left, animated: false)
        }, completion: nil)
        
        
    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if let collectionVW = scrollView as? UICollectionView {
//            if collectionVW == collectionAdvertView
//            {
//                print("Scroll finished")
//                let pageNumber1 = round(scrollView.contentOffset.x / scrollView.frame.size.width)
//                print("pageNumber: \(pageNumber)")
//                pageNumber = Int(pageNumber1)
//                collectionAdvertView.reloadData()
//
//               // pageControl.currentPage = Int(pageNumber)
//            }
//        }
//    }
  
}


//https://stackoverflow.com/questions/34396108/how-to-implement-horizontally-infinite-scrolling-uicollectionview
