//
//  EventDetailView.swift
//  RunApp
//
//  Created by My Mac on 06/06/19.
//  Copyright © 2019 My Mac. All rights reserved.
//


import UIKit
import NightNight
import AVFoundation

class CollectionPlayerCell: UICollectionViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet var lblname: UILabel!
    
    override func awakeFromNib() {
         lblname.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

class CollectionSpectatorCell: UICollectionViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet var lblname: UILabel!
    
    override func awakeFromNib() {
        lblname.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
    
}

class CollectionAdvertiseCell: UICollectionViewCell {
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        lblName.mixedTextColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont )
//        lblDescription.mixedTextColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont )
    }
}

class EventDetailView: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    @IBOutlet var lbleventname: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var scrlVW: UIScrollView!
    
    @IBOutlet weak var tblPlayerJoined: UICollectionView!
    @IBOutlet weak var tblSpectatorJoined: UICollectionView!
    @IBOutlet weak var tblAdvertising: UICollectionView!
    
    @IBOutlet weak var colletionHeightPlayer: NSLayoutConstraint!
    
    @IBOutlet weak var HeightSpectatorView: NSLayoutConstraint!
    @IBOutlet var lblplayerjoined: UILabel!
    @IBOutlet var lblamt: UILabel!
    @IBOutlet var lblspectatorjoined: UILabel!
    
    
   
    var Event_ID = ""
    var Event_Name = ""
    var eventList = [String : Any]()
//    var player: [Dictionary<String, Any>] = []
//    var spectator: [Dictionary<String, Any>] = []
    var player: NSArray = []
    var spectator: NSArray = []
   
     //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        vwPopupTop.setRadius(radius: 10)
        App_Player_Wise_Event_Details()
        lbleventname.text = Event_Name
//        imgBG.mixedImage = MixedImage(normal: UIImage(named: "imgAppBG1") ?? UIImage(), night: UIImage(named: "imgAppBG1-W") ?? UIImage())
         imgBG.mixedImage = MixedImage(normal: UIImage(named: "imgAppBG1-W") ?? UIImage(), night: UIImage(named: "imgAppBG1") ?? UIImage())
        vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
         lblplayerjoined.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
         lblamt.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
         lblspectatorjoined.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        
         lbleventname.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        
    }
    
    
     //MARK:- Web Service
    func App_Player_Wise_Event_Details()
    {
        let parameters = ["Event_ID" : Event_ID
            
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_WISE_EVENT_DETAILS,
                     method: .post,
                     param: parameters,
                     extraHeader: header) { (result) in
                        
                        print(result)
                        
                        if result.getBool(key: "status")
                        {
                            self.eventList = result["data"] as? [String : Any] ?? [:]
                            self.player = self.eventList["player"] as! NSArray
                            self.spectator = self.eventList["spectator"] as! NSArray
                            
                            
                        }
                        DispatchQueue.main.async {
                            self.tblPlayerJoined.reloadData()
                            self.tblSpectatorJoined.reloadData()
                        }
                        
                       
                       // showToast(uiview: self, msg: result.getString(key: "message"))
        }
    }
    
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        self.view.layoutIfNeeded()
//        self.colletionHeightPlayer.constant = self.tblPlayerJoined.contentSize.height
//        self.HeightSpectatorView.constant = self.tblSpectatorJoined.contentSize.height
//
//    }
    
    //MARK:- CollectionView Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == tblPlayerJoined {
            return player.count
        }
        
       else if collectionView == tblSpectatorJoined {
            return spectator.count
        }
            
        else {
             return 1
        }
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == tblPlayerJoined {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionPlayerCell", for: indexPath) as! CollectionPlayerCell
            
            let dic:NSDictionary = player.object(at: indexPath.row) as! NSDictionary
            
           
            let IsRegOrChecked =  dic.value(forKey: "isEventCheckin") as? String

                        if IsRegOrChecked  == "0"
                        {
                           cell.lblname.textColor = UIColor.red
                       }
                        else{
                            cell.lblname.textColor = UIColor.green
                       }
            
            cell.lblname.text = dic.value(forKey: "username") as? String
            
            let imgURL = ServiceList.IMAGE_URL + (dic.value(forKey: "UserProfile") as! String)
         
               cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                           completed: nil)
            
            cell.imgImage.layer.cornerRadius = cell.imgImage.frame.size.width/2
                                        
            return cell
        }
        
       else if collectionView == tblSpectatorJoined {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionSpectatorCell", for: indexPath) as! CollectionSpectatorCell
            
            let dic:NSDictionary = spectator.object(at: indexPath.row) as! NSDictionary
            
            cell.lblname.text = dic.value(forKey: "username") as? String
            let IsRegOrChecked =  dic.value(forKey: "isEventCheckin") as? String

             if IsRegOrChecked  == "0"
             {
                cell.lblname.textColor = UIColor.red
            }
             else{
                 cell.lblname.textColor = UIColor.green
            }
            
            let imgURL = ServiceList.IMAGE_URL + (dic.value(forKey: "UserProfile") as! String)
            
            cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                      completed: nil)
            
            cell.imgImage.layer.cornerRadius = cell.imgImage.frame.size.width/2
            return cell
        }
            
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionAdvertiseCell", for: indexPath) as! CollectionAdvertiseCell
            return cell
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == tblPlayerJoined {
            return  CGSize(width: collectionView.frame.width/3 - 10, height: 103)
        }
            
        else if collectionView == tblSpectatorJoined  {
            return  CGSize(width: collectionView.frame.width/3 - 10, height: 102)
        }
            
        else {
            return  CGSize(width: collectionView.frame.width/1.5 - 10, height: 51)
        }
    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
     {
        if collectionView == tblPlayerJoined {
            
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            nextViewController.playerListDetail = player.object(at: indexPath.row) as! [String : Any]
           // nextViewController.isfromsearch = true;
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            nextViewController.playerListDetail = spectator.object(at: indexPath.row) as! [String : Any]
           // nextViewController.isfromsearch = true;
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
               do {
                   let asset = AVURLAsset(url: path , options: nil)
                   let imgGenerator = AVAssetImageGenerator(asset: asset)
                   imgGenerator.appliesPreferredTrackTransform = true
                   let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                   let thumbnail = UIImage(cgImage: cgImage)
                   return thumbnail
                   
               } catch let error {
                   print("*** Error generating thumbnail: \(error.localizedDescription)")
                   return nil
               }
           }
    
    //MARK: - BUTTON METHODS
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
