//
//  ViewController.swift
//  RunApp


//
//  Created by My Mac on 05/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
// https://developers.google.com/identity/sign-in/ios/
//https://www.youtube.com/watch?v=MZjgIuTpV0I

// GoogleSignInDemo

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
//import FacebookLogin
import SDWebImage
import SVProgressHUD
import Alamofire
import Firebase
import NightNight
import FBSDKShareKit

class LogInViewController: UIViewController , GIDSignInDelegate , UITextFieldDelegate {
    
    @IBOutlet var baseview: UIView!
    
    @IBOutlet weak var btnrememberme: UIButton!
    @IBOutlet weak var btnpassword: UIButton!
    @IBOutlet weak var btnregister: UIButton!
    @IBOutlet weak var lblline1: UILabel!
    @IBOutlet weak var lblconnect: UILabel!
    @IBOutlet weak var lblline2: UILabel!
    
    
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var imgOutlet: UIImageView!
    @IBOutlet weak var btnGoogle: UIButton!
    
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    
    @IBOutlet weak var btnCheckOutlet: UIButton!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    
    var profileImage = UIImageView()
    
    //MARK:- Web service
   
    
    func normalLogin()
    {
       
        let parameters:[String : Any]
        let DeviceID = UIDevice.current.identifierForVendor?.uuidString ?? ""
        print("DeviceID: \(DeviceID)")
        
        if (StoredData.shared.playerId != nil)
     
        {
          parameters = ["EmailID" : txtUsername.text!,
                        "Password" : txtPassword.text!,
                        "DeviceID" : DeviceID ,
                        "Player_ID" : StoredData.shared.playerId!]
                        
            as [String : Any]
        }
            
        else {
             parameters = ["EmailID" : txtUsername.text!,
                           "Password" : txtPassword.text!,
                           "DeviceID" : DeviceID ,
                           "Player_ID" : "1234"] as [String : Any]
        }
    
        callApi(ServiceList.SERVICE_URL+ServiceList.LOGIN,
                method: .post,
                param: parameters)
        { (result) in
            print(result)
            
            // 5001d120-a341-4d1e-aed7-76eaef79b8a5
            
            if result.getBool(key: "status")
            {
                let defaults = UserDefaults.standard
                if self.btnCheckOutlet.accessibilityLabel == "on" {
                    defaults.set(parameters, forKey: "user_data")
                }
                else
                {
                    defaults.set([:], forKey: "user_data")
                }
                defaults.synchronize()
                
                var dict = result.getDictionary(key: "data")
                for (key, value) in dict {
                    let val : NSObject = value as! NSObject;
                    dict[key] = val.isEqual(NSNull()) ? "" : value
                }
                dict["login_token"] = result.getString(key: "login_token")
                print(dict)
                UserDefaults.standard.setUserDict(value: dict)
                UserDefaults.standard.setIsLogin(value: true)
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
            self.showDialougeWithTitle(andMessage: result.getString(key: "message"))
            
        }
    }
    
    func showDialougeWithTitle(andMessage: String)  {
        let alertController = UIAlertController(title: ConstantVariables.Constants.Project_Name, message:
            andMessage, preferredStyle: .alert)
        //alertController.addAction(UIAlertAction(title: "OK", style: .default))
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        // alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func socialLogin(type:String , dict:[String:Any])
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else
        {
             let parameters:[String : Any]
            
            let DeviceID = UIDevice.current.identifierForVendor?.uuidString ?? ""
            print("DeviceID: \(DeviceID)")
    
            if (StoredData.shared.playerId != nil) {
                parameters = ["register_type" : type,
                              "EmailID" : dict["email"] ?? "" ,
                              "social_id" : dict["id"] ?? "" ,
                              "DeviceID" : DeviceID ,
                              "Player_ID" : StoredData.shared.playerId!] as [String : Any]
            }
                
            else {
                parameters = ["register_type" : type,
                              "EmailID" : dict["email"] ?? "" ,
                              "social_id" : dict["id"] ?? "" ,
                              "DeviceID" : DeviceID ,
                              "Player_ID" : "1234"] as [String : Any]
            }
            
            let imgdata = profileImage.image?.jpegData(compressionQuality: 0.5)!
            
            callApi(ServiceList.SERVICE_URL+ServiceList.SOCIAL_LOGIN,
                    method: .post,
                    param: parameters ,
                    data: [imgdata!] ,
                    dataKey: ["Profile_Image"]) { (result) in
                        
                    print("Result Status: \(result)")
                    print("Bool: \(result.getBool(key: "status"))")
                        
                 if result.getBool(key: "status")
                 {
                    var dict = result.getDictionary(key: "data")
                    for (key, value) in dict {
                        let val : NSObject = value as! NSObject;
                        dict[key] = val.isEqual(NSNull()) ? "" : value
                    }
                    
                    dict["login_token"] = result.getString(key: "login_token")
                    dict["id"] = result.getString(key: "id")

                    print(dict)
                    UserDefaults.standard.setUserDict(value: dict)
                    UserDefaults.standard.setIsLogin(value: true)
                    
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                    
                else
                {
                    var dictdata = dict
                    dictdata["type"] = type
                    
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! signUpViewController
                    nextViewController.dictSocial = dictdata
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                
                showToast(uiview: self, msg: result.getString(key: "message"))
                
            }
            
            
            
            //            SVProgressHUD.show(withStatus: nil)
            //
            //            let username = ServiceList.USERNAME
            //            let password = ServiceList.PASSWORD
            //
            //            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            //            let base64LoginData = loginData.base64EncodedString()
            //
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-SIMPLE-API-KEY" : ServiceList.X_SIMPLE_API_KEY]
            //
            //            Alamofire.request(
            //                URL(string: ServiceList.SERVICE_URL+ServiceList.SOCIAL_LOGIN)!,
            //                method: .post,
            //                parameters: parameters,
            //                headers: headers)
            //
            //                .validate()
            //                .responseJSON { (response) -> Void in
            //                    guard response.result.isSuccess
            //                        else {
            //                            print(response.result.error!)
            //                            SVProgressHUD.dismiss()
            //                            return
            //                    }
            //
            //                    var resData : [String : AnyObject] = [:]
            //                    guard let data = response.result.value as? [String:AnyObject],
            //                        let _ = data["status"]! as? String
            //                        else{
            //                            print("Malformed data received from fetchAllRooms service")
            //                            SVProgressHUD.dismiss()
            //                            return
            //                    }
            //
            //                    resData = data
            //                    if resData["status"] as? String ?? "" == "1"
            //                    {
            //                        var dict = resData["Response"] as! [String : Any]
            //                        for (key, value) in dict {
            //                            let val : NSObject = value as! NSObject;
            //                            dict[key] = val.isEqual(NSNull()) ? "" : value
            //                        }
            //
            //                        print(dict)
            //
            //                        UserDefaults.standard.setUserDict(value: dict)
            //                        print(UserDefaults.standard.getUserDict())
            //                        UserDefaults.standard.setIsLogin(value: true)
            //
            //                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            //                        self.navigationController?.pushViewController(nextViewController, animated: true)
            //                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
            //
            //                    }
            //                    else
            //                    {
            //                        var dictdata = dict
            //                        dictdata["type"] = type
            //
            //                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! signUpViewController
            //                        nextViewController.dictSocial = dictdata
            //                        self.navigationController?.pushViewController(nextViewController, animated: true)
            //                    }
            //
            //                    SVProgressHUD.dismiss()
            //            }
        }
    }
    
    //MARK:- Touches Methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideViewWithAnimation(vw: vwPopupTop, img: imgBlur)
    }
    // ["EmailID": testuser8@gmail.com, "Password": 123456, "DeviceID": 529595B3-B1BC-43FF-83D6-DF157F00B7E0]
    //MARK: - VIEW LIFE CYCLE
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()

        vwPopupTop.setRadius(radius: 10)
        btnGoogle.setRadius(radius: btnGoogle.frame.height/2)
        btnFacebook.setRadius(radius: btnFacebook.frame.height/2)
        
        loginOutlet.layer.masksToBounds = true
        loginOutlet.layer.cornerRadius = 10.0
        loginOutlet.layer.borderWidth = 2.0
        
        
      baseview.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
         vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        
       lblline1.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblline2.mixedTextColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblconnect.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtUsername.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
          txtUsername.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)

//         txtUsername.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        txtPassword.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
         txtPassword.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
//        txtPassword.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont,night:FontColor.dayfont )
//
   // imgOutlet.mixedImage = MixedImage(normal: UIImage(named: "run1") ?? UIImage(), night: UIImage(named: "Run-app-icon") ?? UIImage())
        
          imgOutlet.mixedImage = MixedImage(normal: UIImage(named: "run1") ?? UIImage(), night: UIImage(named: "Image") ?? UIImage())
        
btnpassword.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnrememberme.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
         btnrememberme.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
         btnregister.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        
       btnCheckOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
          btnCheckOutlet.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnCheckOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
       // btnCheckOutlet.setMixedImage(MixedImage(normal:"✔", night:"✓"), forState: .normal)
      
        btnregister.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        
//        .mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
        //loginOutlet.titleLabel?.textColor = UIColor.white
        //loginOutlet.titleLabel?.text = "LOGIN"
    
        //        txtUsername.setRadiusBorder(color: UIColor.white)
        //        txtPassword.setRadiusBorder(color: UIColor.white)
        
        // imgSubmitBG.setRadius(radius: imgSubmitBG.frame.height/2)
        // btnSubmit.setGredientText()
        // btnSubmit.dampAnimation()
        
       
        
    }
    
    // google button:  r.216 g 65. b.50
    // login button:  210, 0 , 4
    // facebook:  51, 79, 142
    
    override func viewWillAppear(_ animated: Bool) {
        
        vwPopupTop.isHidden = true
        imgBlur.isHidden = true
        
        let user_data = UserDefaults.standard.object(forKey: "user_data") as? [String:Any] ?? [:]
        print("user_data: \(user_data)")
        
        /*
         user_data: ["Player_ID": 5001d120-a341-4d1e-aed7-76eaef79b8a5, "EmailID": mouse@gmail.com, "DeviceID": 6ABB9F21-5361-4AE4-95C1-DF9044FD9B44, "Password": 123456]
         */
        
        if user_data.count > 0 {
            txtUsername.text = user_data["EmailID"] as? String
            txtPassword.text = user_data["Password"] as? String
            //StoredData.shared.playerId = user_data["Player_ID"] as? String
            btnCheckOutlet.setTitle("✓", for: .normal)
            btnCheckOutlet.accessibilityLabel = "on"
        }
        
        txtUsername.layer.masksToBounds = true
        txtUsername.layer.cornerRadius = 10.0
//        txtUsername.layer.backgroundColor = UIColor.black.cgColor
        txtUsername.layer.borderWidth = 2.0
       // txtUsername.layer.borderColor = UIColor.white.cgColor
       // txtUsername.setRadiusBorder(color: UIColor.white)
        
        txtPassword.layer.masksToBounds = true
        txtPassword.layer.cornerRadius = 10.0
//      txtPassword.layer.backgroundColor = UIColor.black.cgColor
        txtPassword.layer.borderWidth = 2.0
       // txtPassword.layer.borderColor = UIColor.white.cgColor
        
        btnCheckOutlet.layer.masksToBounds = true
        btnCheckOutlet.layer.cornerRadius = 5.0
       // btnCheckOutlet.layer.backgroundColor = UIColor.black.cgColor
        
        btnCheckOutlet.layer.borderWidth = 2.0
      // btnCheckOutlet.layer.borderColor = UIColor.white.cgColor
        
        //       txtUsername.setBottomBorder()
        //       txtPassword.setBottomBorder()
        
        txtUsername.setLeftPaddingPoints(30, strImage: "contacts")
        txtPassword.setLeftPaddingPoints(30, strImage: "password")
//        if NightNight.theme == .night {
//            txtUsername.setLeftPaddingPoints(30, strImage: "contacts")
//            txtPassword.setLeftPaddingPoints(30, strImage: "password")
//
//        } else {
//            txtUsername.setLeftPaddingPoints(30, strImage: "contacts")
//            txtPassword.setLeftPaddingPoints(30, strImage: "password")
//
 //       }
       
        
    }
    
    //MARK: ACTION METHODS
    
    @IBAction func btnOrgaizationPressed(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpOrganizationView") as! SignUpOrganizationView
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnPlayerPressed(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! signUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
        
    @IBAction func rememberBtnClicked(_ sender: UIButton) {
        if btnCheckOutlet.accessibilityLabel == "on"
        {
            btnCheckOutlet.accessibilityLabel = "off"
            btnCheckOutlet.setTitle("", for: .normal)
        }
        else
        {
            btnCheckOutlet.accessibilityLabel = "on"
            btnCheckOutlet.setTitle("✓", for: .normal)
        }
        
    }
    
    @IBAction func forgotPasswordBtnClicked(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordView") as! ForgotPasswordView
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func faceBookBtnClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let login: LoginManager = LoginManager()
        
        login.logIn(permissions: ["public_profile" , "user_friends" , "email"], from: self) { (result, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }else if (result?.isCancelled)!{
                print(result.debugDescription)
            }else {
                
                /*
                 func getFbId(){
                 if(FBSDKAccessToken.current() != nil){
                 FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,name , first_name, last_name , email,picture.type(large)"]).start(completionHandler: { (connection, result, error) in
                 guard let Info = result as? [String: Any] else { return }
                 
                 
                 if let imageURL = ((Info["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                 //Download image from imageURL
                 }
                 if(error == nil){
                 print("result")
                 }
                 })
                 }
                 }
  */
                
                if((AccessToken.current) != nil) {
                    GraphRequest(graphPath: "me", parameters: ["fields": "id, name , first_name , last_name , picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                        
                        guard let Info = result as? [String: Any] else { return }
                        
                        if let imageURL = ((Info["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                            self.profileImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "no-image-box.png"))
                        }
                        
                        if (error == nil) {
                            print(result!)
                            var Dict : [String : AnyObject] = [:]
                            Dict = result as! [String : AnyObject]
                            
                            print(Dict)
                            self.socialLogin(type: "fb", dict: Dict)
                        }
                    })
                }
            }
        }
    }
    
    /*
     let accessToken = FBSDKAccessToken.current()
     if accessToken != nil {
     
     self.getCurrentUserFbId()
     print("LoggedIn")
     
     } else {
     
     print("Not loggedIn")
     self.loginIntoFacebook()
     }
     */
    
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        validation()
    }
    
    @IBAction func registerBtnClicked(_ sender: UIButton) {
        showViewWithAnimation(vw: vwPopupTop, img: imgBlur)
    }
    
    @IBAction func googleBtnClicked(_ sender: UIButton) {
        //  var error: NSError?
        
        GIDSignIn.sharedInstance().clientID = "895851718642-7k45teilaldn9hlvd0lva2qp6fc38e3u.apps.googleusercontent.com"
        
        GIDSignIn.sharedInstance().delegate = self
      //  GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK: - Google Delegate
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error)
            
            return
        }
        guard let authentication = user.authentication else { return }
        
        let u_id = user.userID
        let u_name = user.profile.name
        let u_fname = user.profile.givenName
        let u_Lname = user.profile.familyName
        //let u_email = user.profile.email
        
        var imageURL = ""
        
        if user.profile.hasImage {
            imageURL = user.profile.imageURL(withDimension: 100).absoluteString
            
               // self.imageView.image = UIImage(data: NSData(contentsOfURL: url)!)
            
            profileImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "no-image-box.png"))
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, error) in
            if let err = error {
                print ("failed to create with google account", err)
                return
            }
            
            var Dict : [String : AnyObject] = [:]
            Dict = ["picture" : user?.user.photoURL as AnyObject,
                    "name" : u_name as AnyObject,
                    "first_name" : u_fname as AnyObject,
                    "last_name" : u_Lname as AnyObject,
                    "email" : user?.user.email as AnyObject ,
                    "id" : u_id as AnyObject
            ]
            
            print(Dict)
            self.socialLogin(type: "google", dict: Dict)
        })
    }
    
    //MARK:- Other method
    func validation()
    {
        self.view.endEditing(true)
        if txtUsername.text == "" && txtPassword.text == ""
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
            
        else if txtUsername.text == ""
        {
            showAlert(uiview: self, msg: "Please enter user name", isTwoButton: false)
        }
            
        else if txtPassword.text == ""
        {
            showAlert(uiview: self, msg: "Please enter password", isTwoButton: false)
        }
            
        else
        {
            normalLogin()
        }
    }
    
    //MARK:- Textfield methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
}

/*
 GIDSignIn.sharedInstance().clientID = "your client id present in googleservice .plist file"
 GIDSignIn.sharedInstance().delegate = self
 https://stackoverflow.com/questions/34368613/custom-google-sign-in-button-ios
 */
