//
//  UpcomingRunViewController.swift
//  RunApp

//
//  Created by My Mac on 08/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
// https://stackoverflow.com/questions/52846785/the-executable-was-signed-with-invalid-entitlements-0xe8008016

import UIKit
import SVProgressHUD
import Alamofire
import ActionSheetPicker_3_0
import Stripe
import NightNight
import FGRoute
import CoreLocation

class collectionRecentCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblLast: UILabel!
    
    override func awakeFromNib() {
        lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLast.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        
    }
   
}

class UpcomingRunViewController: UIViewController ,  UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout , CLLocationManagerDelegate
{

    var lagitude = Double()
    var logitude = Double()
    var locationManager = CLLocationManager()
    var flag = Bool()
    
    
    
    
    var TreadingID = String()
    
    
    @IBOutlet weak var imglocationclr: UIImageView!
    
    @IBOutlet weak var lblseemore: UIButton!
    @IBOutlet weak var lblupcomingrun: UILabel!
    @IBOutlet weak var lblseemorepost: UILabel!
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var tblUpcomingView: UITableView!
    
    @IBOutlet weak var btnAddEventOutlet: UIButton!
    
    @IBOutlet weak var CollectionRecentView: UICollectionView!
    
    @IBOutlet weak var vwPopup: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    
    @IBOutlet weak var btnSpectatorOutlet: UIButton!
    
    @IBOutlet weak var btnPayOutlet: UIButton!
    
    @IBOutlet weak var constTblUpcomingHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblSince: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    
    @IBOutlet weak var btnCheckInOutlet: UIButton!
    @IBOutlet var btncheckmark: UIButton!
    
    
    var featureList = [[String : Any]]()
    var rsvpList = [[String : Any]]()
    var rsvpListGet = [[String : Any]]()
    var dictLocation = [String : Any]()
     var treadingList = [[String : Any]]()
    
    var LocationID: String?
    var SpectatorArray = [String]()
    var EventID = ""
    var Amt = ""
    var PlayerAmt = ""
    var AmountGet = ""
    var PlayerType = ""
    var Eventstatus = ""
    
    
    var RSVPTitle = ""
    
    let payObject = PaymentViewController()
    
    @IBOutlet weak var lblPaid: UILabel!
    
    // MARK:- TOUCHES BEGAN
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideViewWithAnimation(vw: vwPopup, img: imgBlur)
    }
    
    // MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
      if CLLocationManager.locationServicesEnabled() == true {
                        
                        if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                            locationManager.requestWhenInUseAuthorization()
                        }
                        
                        locationManager.desiredAccuracy = kCLLocationAccuracyBest
                        locationManager.delegate = self
                        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        locationManager.requestWhenInUseAuthorization()
                        locationManager.startUpdatingLocation()
                        locationManager.requestLocation()
                        flag = true
                        // mapVW.mapType = .hybrid
                    } else {
        
                        print("Please turn on location services or GPS")
        
                    }
        
//        if Eventstatus == "0"
//                   {
//                       imglocationclr.image = UIImage(named: "redLocation")
//                   }
//                   else if Eventstatus == "1"
//                   {
//                      imglocationclr.image = UIImage(named: "map_green")
//                   }
//                   else
//                   {
//                      imglocationclr.image = UIImage(named: "yellowLocation")
//                   }

        
        if Eventstatus == "0"
                          {
                              imglocationclr.image = UIImage(named: "redLocation")
                          }
                          else if Eventstatus == "1"
                          {
                              imglocationclr.image = UIImage(named: "yellowLocation")
                          }
                          else
                          {
                             imglocationclr.image = UIImage(named: "map_green")
                          }
              
        
        vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
       // lblupcomingrun.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblName.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblseemorepost.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         lblAddress.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         lblSince.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         lblLoc.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblPaid.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
      //  let attributedString = NSMutableAttributedString(string: lblseemore.titleLabel!.text!)
//        attributedString.setMixedAttributes(
//            [NNForegroundColorAttributeName: MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)],
//            range: NSRange(location: 0, length: attributedString.length)
//        )
       // lblseemore.setAttributedTitle(attributedString, for: .normal)
        
       
    
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "h:mm a 'on' MMMM dd, yyyy"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        let dateString = formatter.string(from: Date())
        print(dateString)   // "4:44 PM on June 23, 2016\n"
        
       // SpectatorArray = ["Spectator","Player","Both"]
        
         SpectatorArray = ["Spectator","Player"]
        // print("LocationID: \(LocationID)")
        
        btnSpectatorOutlet.setRadius(radius: 12)
        btnSpectatorOutlet.setRadiusBorder(color: UIColor.gray)
        btnSpectatorOutlet.roundCorners(corners: .allCorners, radius: 2)
       
        vwPopup.setRadius(radius: 10)
       // btnAddEventOutlet.imageView?.changeImageViewImageColor(color: UIColor.gray)
       // app_Event_Status_Api()
        
        btnPayOutlet.setRadius(radius: btnPayOutlet.frame.height/2)
        self.view.layoutIfNeeded()
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
       
        tblUpcomingView.tableFooterView = UIView()
        getTrendingHeadline()
        self.view.layoutIfNeeded()
        
    }
    override func viewWillAppear(_ animated: Bool) {
         appGetRsvpLocation()
        AppCheckInList()
        
    }
    override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.viewDidLayoutSubviews()
        
    
        self.view.layoutIfNeeded()
    }
    
    //MARK:- Collection View Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return treadingList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionRecentCell", for: indexPath) as! collectionRecentCell
//        cell.lblName.isHidden = true
//        cell.lblLast.isHidden = true
//        return cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionRecentCell", for: indexPath) as! collectionRecentCell
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        let imgURL = ServiceList.IMAGE_URL + "\(treadingList[indexPath.row]["Treading_Image"] as? String ?? "")"
        cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,completed: nil)
        cell.lblName.text = treadingList[indexPath.row]["Treading_Title"] as? String
        cell.lblLast.text = treadingList[indexPath.row]["Treading_Desc"] as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        TreadingID = "\(treadingList[indexPath.row]["Treading_ID"] as? String ?? "")"
                   let trendingurl = "\(treadingList[indexPath.row]["Treading_URL"] as? String ?? "")"
                   ClickCount()
        
        if (trendingurl != "") {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! webView
            nextViewController.urlString = trendingurl
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        
                   
//                   if let url = URL(string:trendingurl) {
//                                 UIApplication.shared.open(url)
//                             }
    }
    //MARK:- Web service
    
    func getTrendingHeadline()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
            ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TREADING_HEADLINE,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        print(result.getBool(key: "status"))
                        self.treadingList = result["data"] as? [[String:Any]] ?? []
                        self.CollectionRecentView.reloadData()
                    }
        })
    }
    
    func appGetRsvpLocation()
    {
        let parameters = ["LocationID": LocationID ?? "","EventID":EventID,"PlayerID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""
                      ] as [String : Any]

        callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_RSVP_LOCATION,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        //let data = result["data"] as? [[String:Any]] ?? []
                        
                    let GetData:NSDictionary = result["data"] as! NSDictionary
                     self.rsvpListGet = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                        print("rsvpListGet:\(self.rsvpListGet)")
                        let jsonarr:NSArray = GetData.value(forKey: "event") as! NSArray
                        if jsonarr.count > 0
                        {
                             let dict1:NSDictionary = jsonarr.lastObject as! NSDictionary
                            self.rsvpList = [[String : Any]]()
                            self.rsvpList.append(dict1 as! [String : Any])
                            
                        }
                       
                     //   let dict2:NSDictionary = jsonarr.object(at: jsonarr.count-2) as! NSDictionary
                       
                       // self.rsvpList.append(dict2 as! [String : Any])
                       
                        
                        self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                         self.parseLocation(LocationDict: self.dictLocation)
                    }
                    else
                    {
                        showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                    
                    DispatchQueue.main.async {
                        self.tblUpcomingView.reloadData()
                      //  self.constTblUpcomingHeight.constant = self.tblUpcomingView.contentSize.height
                    }
        })
    }

    func parseLocation(LocationDict : [String:Any]) {
        
        self.lblName.text = LocationDict["LocationName"] as? String ?? ""
        self.lblAddress.text = LocationDict["LocationAddress"] as? String ?? ""
        //self.EventID = LocationDict["EventID"] as? Int ?? 0
        let cretedOn = LocationDict["RegistrationDate"] as? String ?? ""
        let Since = cretedOn.toDate3()
        self.lblSince.text = "Run site Since: \(Since)"
        
        //                        let imgPin = placeList[i]["product_image"] as? String ?? ""
        //                            let Distance = placeList[i]["product_image"] as? String ?? ""
        self.lblLoc.text = "\(LocationDict["LocMode"] as? String ?? "")"
        
        
        
        print("LocationDict: \(LocationDict)")
    }
    
    @IBAction func BtnCheckedInAction(_ sender: UIButton) {
        
        if btnCheckInOutlet.currentTitle != "Checked In"
        {
            AppCheckInAdd()
        }
      else
        {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "checkinlist") as! checkinlist;
            nextViewController.LocationID = self.LocationID
           self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
         {
            if flag {
                flag = false
                if let location = locations.last
                {
                    //print("Found user's location: \(location)")
                    print("latitude: \(location.coordinate.latitude)")
                    print("longitude: \(location.coordinate.longitude)")
                    lagitude = location.coordinate.latitude
                    logitude = location.coordinate.longitude
                    locationManager.stopUpdatingLocation()
                   // getlocationinfo()
                }
            }
                
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
               print("Unable to access your current location")
           }
    
    func AppCheckInList()
       {
           let parameters = ["Location_ID" : LocationID! ,
                             "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
               ] as [String : Any]
           
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_LIST,
                   method: .post,
                   param: parameters ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           self.btnCheckInOutlet.setTitle("Checked In", for: .normal)
                           self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
                         // self.imgLocation.image = UIImage(named: "map_green")
                       }
                           
                       else {
                           self.btnCheckInOutlet.setTitle("Check In", for: .normal)
                           self.btnCheckInOutlet.setTitleColor(UIColor.red, for: .normal)
                          // self.imgLocation.image = UIImage(named: "redLocation")
                       }
           })
       }
    
      func AppCheckInAdd()
        {
            let parameters = ["Location_ID" : LocationID! ,
                              "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "Lat" : lagitude,
                              "Long" : logitude
                              ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_ADD,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                           showToast(uiview: self, msg: result.getString(key: "message"))
                            self.btnCheckInOutlet.setTitle("Checked In", for: .normal)
                        self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
                        self.imglocationclr.image = UIImage(named: "map_green")
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "checkinlist") as! checkinlist;
                        nextViewController.LocationID = self.LocationID
                 self.navigationController?.pushViewController(nextViewController, animated: true)
                 
                        
                        }
                            
                        else {
                            showToast(uiview: self, msg: result.getString(key: "message"))
    //                        self.btnCheckInOutlet.setTitle(""Checked In", for: .normal)
    //                        self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
    //                        self.imgLocation.image = UIImage(named: "map_green")
                            
                        }
                        
            })
        }
//    func appSpectatorJoin()
//    {
//        if EventID != "" {
//            let parameters = ["Location_Id": LocationID ?? "" ,
//                              "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
//                              "Event_ID": EventID,
//
//                ] as [String : Any]
//
//            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
//                          ] as [String : Any]
//
//            callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_JOIN,
//                    method: .post,
//                    param: parameters ,
//                    extraHeader: header ,
//                    completionHandler: { (result) in
//                        print(result)
//                        self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
//                        showToast(uiview: self, msg: result.getString(key: "message"))
//                        if result.getBool(key: "status")
//                        {
//
//
//                        }
//                        else
//                        {
//                           // showToast(uiview: self, msg: result.getString(key: "message"))
//                        }
//                         self.appGetRsvpLocation()
//            })
//        }
//
//        else {
//            print("Event id is Empty")
//        }
//
//    }
    
    
    func appSpectatorJoin()
      {
          if EventID != "" {
              let parameters = ["Location_Id": LocationID ?? "" ,
                                "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                "Event_ID": EventID, "PlayerType" : PlayerType
                                
                  ] as [String : Any]
              
              let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ] as [String : Any]
              
              callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerJoin,
                      method: .post,
                      param: parameters ,
                      extraHeader: header ,
                      completionHandler: { (result) in
                          print(result)
                          self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                          showToast(uiview: self, msg: result.getString(key: "message"))
                          if result.getBool(key: "status")
                          {
                              
                             
                          }
                          else
                          {
                             // showToast(uiview: self, msg: result.getString(key: "message"))
                          }
                           self.appGetRsvpLocation()
              })
          }
              
          else {
              print("Event id is Empty")
          }
          
      }
    
    func appEventCheckin()
        {
            if EventID != "" {
                let parameters = ["Location_Id": LocationID ?? "" ,
                                  "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                  "Event_ID": EventID, "PlayerType" : PlayerType,"Lat":lagitude,"Long":logitude
                                  
                    ] as [String : Any]
                
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                              ] as [String : Any]
                
                callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerEventCheckIn,
                        method: .post,
                        param: parameters ,
                        extraHeader: header ,
                        completionHandler: { (result) in
                            print(result)
                            self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                            showToast(uiview: self, msg: result.getString(key: "message"))
                            if result.getBool(key: "status")
                            {
                                
                               
                            }
                            else
                            {
                               // showToast(uiview: self, msg: result.getString(key: "message"))
                            }
                             self.appGetRsvpLocation()
                })
            }
                
            else {
                print("Event id is Empty")
            }
            
        }
    
    func appCancelSpectator()
     {
         if EventID != "" {
             let parameters = ["Location_Id": LocationID ?? "" ,
                               "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                               "Event_ID": EventID
                 ] as [String : Any]
             
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_CANCEL,
                     method: .post,
                     param: parameters ,
                     extraHeader: header ,
                     completionHandler: { (result) in
                         print(result)
                         self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                         showToast(uiview: self, msg: result.getString(key: "message"))
                         if result.getBool(key: "status")
                         {
                             
                          //   self.btnPayOutlet.setTitle("PAY NOW", for: .normal)
                         }
                         else
                         {
                            // showToast(uiview: self, msg: result.getString(key: "message"))
                         }
                          self.appGetRsvpLocation()
                        
             })
         }
             
         else {
             print("Event id is Empty")
         }
         
     }
 
    func app_Event_Status_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else
        {
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-SIMPLE-API-KEY" : ServiceList.X_SIMPLE_API_KEY,
                "USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.APP_EVENT_STATUS)!,
                              method : .get, parameters : nil,
                              encoding : JSONEncoding.default ,
                              headers : headers).responseData {
                                dataResponse in
                                guard dataResponse.result.isSuccess
                                    else {
                                        print(dataResponse.result.error!)
                                        SVProgressHUD.dismiss()
                                        return
                                }
                                
                                self.parseData(dataResponse.data!)
            }
        }
    }
    
    //MARK:- Parse ResponseData
    
    func parseData(_ data : Data){
        do {
            let readableJSON = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String :AnyObject]
            print("readableJSON : \(readableJSON)")
            
            self.featureList = readableJSON["datafeture"] as? [[String:Any]] ?? []
            //self.tblViewPlayers.reloadData()
            showToast(uiview: self, msg: readableJSON["message"]! as! String)
            SVProgressHUD.dismiss()
        }
            
        catch {
            print(error)
        }
    }

     // MARK: BUTTON METHODS
    
    @IBAction func btnSeeMorePressed(_ sender: UIButton) {
       
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SeeMoreViewController") as! SeeMoreViewController
        nextViewController.LocationID = LocationID
        nextViewController.EventID = EventID
        nextViewController.rsvpDictList = rsvpListGet
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddLocationAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnAddNewEventAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewEventViewController") as! AddNewEventViewController
        nextViewController.LocationID = LocationID
        nextViewController.EventID1 = EventID
        nextViewController.rsvpListGet1 = rsvpListGet
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    @IBAction func btnSpectatorAction(_ sender: UIButton) {
        self.view.endEditing(true)
               
        
        if SpectatorArray.count != 0
                {
                    ActionSheetStringPicker.show(withTitle: "Select Type", rows: SpectatorArray, initialSelection: 0, doneBlock: {
                        picker, indexes, values in
                        
                        self.btnSpectatorOutlet.setTitle("\(values!)", for: .normal)
                        
                        if self.btnSpectatorOutlet.currentTitle == "Spectator" {
                            self.AmountGet = self.Amt
                            self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
                            self.PlayerType = "Spectator"
                        }
                            
                        else if self.btnSpectatorOutlet.currentTitle == "Player" {
                            self.AmountGet = self.PlayerAmt
                            self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
                            self.PlayerType = "Player"
                        }
                        
                        else {
                            self.AmountGet = self.PlayerAmt
                            self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
                            self.PlayerType = "Both"
                        }
                        return
                    }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                }
    }
    
    @IBAction func btnPayActionClicked(_ sender: UIButton) {
        
        
            if RSVPTitle == "RSVP"
            {
                
            if self.rsvpList[sender.tag]["EventType"] as! String == "Paid"
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
            nextViewController.Amount = AmountGet
            nextViewController.EventID = EventID
            nextViewController.PlayerType = PlayerType
            nextViewController.LocationID = LocationID
            nextViewController.rsvpDictList1 = rsvpList[sender.tag]

            self.navigationController?.pushViewController(nextViewController, animated: true)
            
            
        }
        else
        {
            //self.btnPayOutlet.setTitle("Join", for: .normal)
            appSpectatorJoin()
            
        }
        }
           

        else if RSVPTitle == "EventCheckIn" {
            if self.rsvpList[sender.tag]["EventType"] as! String == "Paid"
                {
                if self.rsvpList[sender.tag]["IsPayment"] as! Int == 0
                {
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                    nextViewController.Amount = AmountGet
                    nextViewController.EventID = EventID
                    nextViewController.PlayerType = PlayerType
                    nextViewController.LocationID = LocationID
                    nextViewController.rsvpDictList1 = rsvpList[sender.tag]
                    
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                else{
                     appEventCheckin()
                    
                    }
                }
                else
                {
                    //self.btnPayOutlet.setTitle("Join", for: .normal)
                    appEventCheckin()
                }
        }
        
        
        
        
        
            }
        
     

        
//        let cardParams = STPCardParams()
//        cardParams.number = ...
//            cardParams.expMonth = ...
//            cardParams.expYear = ...
//            cardParams.cvc = ...
//        if STPCardValidator.validationStateForCard(cardParams) == .Valid {
//            // the card is valid.
//        }
//
//        STPAPIClient.shared().createToken(withCard: cardParams, completion: { (token, error) -> Void in
//            if error != nil {
//                self.hideProgress()
//                self.showAlert(self, message: "Internet is not working")
//                print(error)
//                return
//            }
//            let params : [String : AnyObject] = ["Token": token!.tokenId as AnyObject, "Amount": paymentAmount as AnyObject, "CardHolderName": AppVars.RiderName as AnyObject]
//
//            Alamofire.request(url + "/api/postcharge", method: .post, parameters: params, encoding: JSONEncoding.default, headers: [ "Authorization": "Bearer " + token]).responseJSON { response in
//                switch response.result {
//                case .failure(_):
//                    self.hideProgress()
//                    self.showAlert(self, message: "Internet is not working")
//                case .success(_):
//                    let dataString:NSString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)!
//                    if (dataString as? String) != nil {
//                        self.showAlert(self, message: "Your payment has been successful")
//                    } else {
//                        self.showAlert(self, message: "Your payment has not been successful. Please, try again")
//                    }
//                }
//            }
//        })
  //  }
    
    func decimal(with string: String) -> NSDecimalNumber {
      
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        return formatter.number(from: string) as? NSDecimalNumber ?? 0
    }

    @objc func btnRsvpAction(button: UIButton) {
        
        btnPayOutlet.tag = button.tag
        
         RSVPTitle =   button.currentTitle!
         
          
        
        self.Amt = rsvpList[button.tag]["Amt"] as? String ?? ""
        self.PlayerAmt = rsvpList[button.tag]["PlayerAmt"] as? String ?? ""
        self.EventID = rsvpList[button.tag]["EventID"] as? String ?? ""
        self.PlayerType = "Spectator"
        self.AmountGet = self.Amt
        self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
        
        let BookingStatus = rsvpList[button.tag]["BookingStatus"] as? String ?? ""
        
        
        if self.rsvpList[button.tag]["EventType"] as! String == "Paid"
        {
           self.btnPayOutlet.setTitle("PAY NOW", for: .normal)
        }
        else
        {
            self.btnPayOutlet.setTitle("JOIN", for: .normal)
        }
        
        if BookingStatus == "0" {
            showViewWithAnimation(vw: vwPopup, img: imgBlur)
        }
            
        else {
            showAlert(uiview: self, msg: "Booking Closed", isTwoButton: false)
        }
        
            let RSVPD = rsvpList[button.tag]["RSVPD"] as? String ?? ""
        
        
        if RSVPTitle == "Cancel"
                      {
                          appCancelSpectator()
                      }
               
        

    }
    
    
   
    
    func completeCharge(EventID:String, PlayerID: String, Amount: String) {
   
        let parameters: Parameters=[
            "description": "My first payment",
            "amount": Int(Amount)! * 100,
            "currency":Constants.DefultCurrency,
            "source":"tok_visa",
            "customer":EventID
        ]
        
        print("Parameters : ",parameters)
        
        let headers = [
            
            "authorization": Constants.KEY_TEST,
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "fc7c5030-35b5-4974-64a3-54a48f0ded96"
        ]
        
        print("headers : ",headers)
        
        let manager = Alamofire.SessionManager.default
        let url = URL(string:  Constants.StripeChargeURL)!
        
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers : headers).responseJSON() { response in
            switch (response.result) {
            case .success:
              
                print("JsonResponse printed \(response)")
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
}

class UpcomingTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPm: UILabel!
    @IBOutlet weak var lblYouth: UILabel!
    @IBOutlet weak var btnRsvp: UIButton!
    @IBOutlet var lblspactatorprice: UILabel!
    @IBOutlet var lblplayerprice: UILabel!
    
    @IBOutlet weak var lblLOC: UILabel!
    
    
    
    override func awakeFromNib() {
        
        lblDate.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblYouth.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblspactatorprice.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblplayerprice.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblLOC.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
    }
}

class RecentTableViewCell: UITableViewCell {
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblHesOnFire: UILabel!
    @IBOutlet weak var lblHopenDin: UILabel!
    @IBOutlet weak var lblFressRetro: UILabel!
}

extension UpcomingRunViewController: UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return rsvpList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "upcomingRunCell", for: indexPath) as! UpcomingTableViewCell
            self.view.layoutIfNeeded()
            //Amt
            cell.btnRsvp.layer.cornerRadius = 5.0
            cell.btnRsvp.layer.masksToBounds = true
            cell.btnRsvp.tag = indexPath.row
//            self.EventID = rsvpList[indexPath.row]["EventID"] as? String ?? ""
//            self.Amt = rsvpList[indexPath.row]["Amt"] as? String ?? ""
//            self.PlayerAmt = rsvpList[indexPath.row]["PlayerAmt"] as? String ?? ""
            //PlayerAmt
        cell.lblYouth.text = (rsvpList[indexPath.row]["EvenetName"] as? String ?? "")
        cell.lblLOC.text =  "LOC:" + (rsvpList[indexPath.row]["CLVLTitle"] as? String ?? "")
        
        cell.lblspactatorprice.text = "Spectator Price: " + String(rsvpList[indexPath.row]["Amt"] as? String ?? "")
        cell.lblplayerprice.text = "Player Price: " + String(rsvpList[indexPath.row]["PlayerAmt"] as? String ?? "")
        
            let cretedOn = rsvpList[indexPath.row]["EventDate"] as? String ?? ""
            
            print("EventID: \(self.EventID)")
            
            if cretedOn != "" {
                 cell.lblDate.text = cretedOn.toDate5()
            }
            
            if UserDefaults.standard.getUserDict()["usertype"] as? String ?? "" != "Organization" {
                 
                cell.btnRsvp.isHidden = false
             
                cell.btnRsvp.addTarget(self, action: #selector(btnRsvpAction), for: .touchUpInside)
               
                
                let RSVPD = rsvpList[indexPath.row]["RSVPD"] as? String ?? ""
                
                if RSVPD == "1"
                {
//                     cell.btnRsvp.setTitle("RSVP'd", for: .normal)
//                    cell.btnRsvp.isEnabled = false
                    
                     cell.btnRsvp.setTitle("Cancel", for: .normal)
                     cell.btnRsvp.isEnabled = true
                     
                }
                else
                {
                    cell.btnRsvp.setTitle("RSVP", for: .normal)
                    cell.btnRsvp.isEnabled = true
                  
                }
                
                let IsEventOneHour = rsvpList[indexPath.row]["IsEventOneHour"] as? Int ?? 3
                    
                    if IsEventOneHour == 1
                {
                    cell.btnRsvp.setTitle("EventCheckIn", for: .normal)
                   
                    //btncheckmark.isHidden = true
                   // btnCheckInOutlet.isHidden = true
                    
                }
                let IsEventCheckedIn = rsvpList[indexPath.row]["IsCheckedIn"] as? Int ?? 3
                                  
                                  if IsEventCheckedIn == 1
                              {
                                  cell.btnRsvp.setTitle("CheckedIn", for: .normal)
                                  cell.btnRsvp.isEnabled = false
                                
                              }
                
                
            }
                
            else
            {
                cell.btnRsvp.isHidden = true
            }
        
        
        
        cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night: FontColor.dayfont)
        
        
        
        constTblUpcomingHeight.constant = tblUpcomingView.contentSize.height + 30
            self.view.layoutIfNeeded()
            return cell
    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheFounderController") as! TheFounderController
//            nextViewController.upcoming = "upcoming"
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//
//    }
    
    
    
    func ClickCount()
             {
              let parameters = [
               "PlayerID":UserDefaults.standard.getUserDict()["id"] as? String ?? "","SponsorID":TreadingID,"IPAddress":FGRoute.getIPAddress()!,"AdType" : "TR"
               ] as [String : Any]
              let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ] as [String : Any]
              
              callApi(ServiceList.SERVICE_URL+ServiceList.Sponser_click_count,
                      method: .post,
                      param: parameters,
                      extraHeader: header,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                              
                          }
              })
       

          }
       
    
    
    
    
}


 /*
 //https://stackoverflow.com/questions/32621980/stripe-stppaymentcardtextfield-how-to-add-zip-postal-code-field
 
 */
