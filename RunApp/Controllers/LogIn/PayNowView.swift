//
//  PayNowView.swift
//  RunApp
//
//  Created by My Mac on 26/07/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire

class PayNowView: UIViewController , UITextFieldDelegate  {
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var scrlVW: UIScrollView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var btnPayOutlet: UIButton!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblShootingStar: UILabel!
    @IBOutlet weak var lblGround: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtMontYear: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    
    @IBOutlet weak var txtzipcode: UITextField!
    
    @IBOutlet weak var txtnameoncard: UITextField!
    
    
    var rsvpDictList1 = [String : Any]()
    
    
    var EventID = ""
    var Amount = ""
    var chargeValue = ""
    var PlayerType = ""
    var LocationID: String?
    var Cardname : String?
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        print("PlayerType: \(PlayerType)")
        print(rsvpDictList1)
        txtCardNumber.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtMontYear.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtCvv.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        
        lblAmount.text = "$\(Amount)"
        vwPopupTop.setRadius(radius: 10)
        cardView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        btnPayOutlet.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
        
        lblGround.text = rsvpDictList1["LocationName"] as? String ?? ""
        lblShootingStar.text = rsvpDictList1["EvenetName"] as? String ?? ""
       // lblAmount.text =  "$" + "\(rsvpDictList1["PlayerAmt"] as? String ?? "")"
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(handleChange(sender:)), for: .valueChanged)
        txtMontYear.inputView = datePickerView
        
    }
    
    @objc func handleChange(sender: UIDatePicker){
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        txtMontYear.text = dateFormatter.string(from: sender.date)
        //txtCvv.becomeFirstResponder()
    }
    
    //MARK:- BUTTON METHODS
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayAction(_ sender: UIButton) {
        validation()
    }
    
    @IBAction func btnSecureStripAction(_ sender: UIButton) {
        print("Stripe.com")
    }
    
    //MARK:- Other Method
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text!
        
        if text.utf16.count == 16 {
            switch textField {
            case txtCardNumber:
                txtMontYear.becomeFirstResponder()
            default:
                break
            }
        }
            
        else if text.utf16.count == 7 {
            switch textField {
            case txtMontYear:
                txtCvv.becomeFirstResponder()
            default:
                break
            }
        }
            
//        else if text.utf16.count == 3 {
//            switch textField {
//            case txtCvv:
//                txtCvv.resignFirstResponder()
//            default:
//                break
//            }
//        }
            
        else {
            print("TextField Not 1")
        }
    }
    
    func validation()
    {
        self.view.endEditing(true)
        
        let CarNumber = txtCardNumber.text!
        let expiry = txtMontYear.text!
        let zipcode = txtzipcode.text!
        Cardname = txtnameoncard.text
        
        let cvv = txtCvv.text!
        
        if txtCardNumber.text == "" && txtMontYear.text == "" && txtCvv.text == ""
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        
        else if txtCardNumber.text == ""
        {
            showAlert(uiview: self, msg: "Please enter card Number", isTwoButton: false)
        }
            
//        else if CarNumber.count < 2
//        {
//            showAlert(uiview: self, msg: "Name should have minimum 2 characters And maximum 15 characters", isTwoButton: false)
//        }
            
        else if CarNumber.count > 16
        {
            showAlert(uiview: self, msg: "Card Number should have maximum 16 characters", isTwoButton: false)
        }
        
        else if txtMontYear.text == ""
        {
            showAlert(uiview: self, msg: "Please enter Month & year", isTwoButton: false)
        }
            
        else if txtCvv.text == ""
        {
            showAlert(uiview: self, msg: "Please enter CVV Number", isTwoButton: false)
        }
          
            else if txtzipcode.text == ""
            {
                showAlert(uiview: self, msg: "Please enter ZIPCODE", isTwoButton: false)
            }
        else {
            
            print("Month : \(UInt(String(expiry.prefix(2)))!)")
            print("Year : \(UInt(String(expiry.suffix(2)))!)")
            
            let cardparameters = STPCardParams()
            cardparameters.number = CarNumber
           // cardparameters.name = cardholdername
            cardparameters.expMonth = UInt(String(expiry.prefix(2)))!
            cardparameters.expYear =  UInt(String(expiry.suffix(2)))!
            cardparameters.cvc = cvv
            cardparameters.address.postalCode = zipcode
            
            
            
            AppDelegate.shared().ShowHUD()
            STPAPIClient.shared().createToken(withCard: cardparameters, completion: { (token, error) -> Void in

                if error != nil {
                    AppDelegate.shared().HideHUD()
                    self.handleError(error: error! as NSError)
                    return
                } else {
                    print("token:\(token!)")
                    print(STPCard.string(from: (token?.card!.brand)!))
                    
                    self.completeCharge(EventID: self.EventID, PlayerID: UserDefaults.standard.getUserDict()["id"] as? String ?? "", Amount: self.Amount, source: token!)
                
                }
            })
        }
    }
    
    func handleError(error: NSError) {
        
        let alert = UIAlertController(title: "Please Try Again", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getPaymentApi(EventID:String, PlayerID: String, Amount: String,token: STPToken, ChargeId: String, PlayerType: String ,CardName : String)
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_PAYMENT,
                method: .post,
                param: ["EventID" : EventID, "PlayerID": PlayerID, "Amount": Amount, "Token": token, "Charge" : ChargeId, "PlayerType" : PlayerType , "CardName": Cardname! ] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
        
                        self.txtCardNumber.text = ""
                        self.txtMontYear.text = ""
                        self.txtCvv.text = ""
                        
                        
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                            nextViewController.LocationID = self.LocationID
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                        showToast(uiview: self, msg: result.getString(key: "message"))
                        
                       
                        
                    }
                    
                    AppDelegate.shared().HideHUD()
        })
    }
    
    func completeCharge(EventID:String, PlayerID: String, Amount: String,source: STPToken ) {
        
        let parameters: Parameters=[
            "description": "My first payment",
            "amount": Int(Amount)! * 100,
            "currency":ConstantVariables.Constants.DefultCurrency,
            "source": source,
        ]
        
        print("Parameters : ",parameters)
        
        let headers = [
            
            "authorization": ConstantVariables.Constants.KEY_TEST,
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "fc7c5030-35b5-4974-64a3-54a48f0ded96"
        ]
        
        print("headers : ",headers)
        
        let manager = Alamofire.SessionManager.default
        let url = URL(string: ConstantVariables.Constants.StripeChargeURL)!
        
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers : headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                
               // print("JsonResponse printed \(response)")
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                self.chargeValue = jsonResponse["id"] as? String ?? ""
                
                self.getPaymentApi(EventID: self.EventID, PlayerID: PlayerID, Amount: self.Amount, token: source, ChargeId: self.chargeValue, PlayerType: self.PlayerType,CardName:self.Cardname!)
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
