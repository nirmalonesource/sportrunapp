//
//  ForgotPasswordView.swift
//  RunApp
//
//  Created by My Mac on 26/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import  NightNight


class ForgotPasswordView: UIViewController , UITextFieldDelegate
{
    
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var imgRunapp: UIImageView!
    @IBOutlet var baseview: UIView!
    @IBOutlet weak var lblforgotpassword: UILabel!
    @IBOutlet weak var lblenteryouremail: UILabel!
    
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    //MARK:- Web service
    
    func forgotPassword1()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            let parameters = ["email" : txtEmail.text!] as [String : Any]
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-SIMPLE-API-KEY" : ServiceList.X_SIMPLE_API_KEY]
            
            Alamofire.request(
                URL(string: ServiceList.SERVICE_URL+ServiceList.FORGOT_PASSWORD)!,
                method: .post,
                parameters: parameters,
                headers: headers)
                .validate()
                .responseJSON { (response) -> Void in
                    guard response.result.isSuccess
                        else {
                            print(response.result.error!)
                            SVProgressHUD.dismiss()
                            return
                    }
                    
                    var resData : [String : AnyObject] = [:]
                    guard let data = response.result.value as? [String:AnyObject],
                        let _ = data["status"]! as? String
                        else{
                            print("Malformed data received from fetchAllRooms service")
                            SVProgressHUD.dismiss()
                            return
                    }
                    
                    resData = data
                    if resData["status"] as? String ?? "" == "1"
                    {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    showToast(uiview: self, msg: resData["message"]! as! String)
                    SVProgressHUD.dismiss()
            }
        }
    }
    
    func forgotPassword()
    {
        let parameters = ["email" : txtEmail.text!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.FORGOT_PASSWORD,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        
                        let alertController = UIAlertController(title: "", message: result["message"]! as? String, preferredStyle:UIAlertController.Style.alert)

                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                          { action -> Void in
                            // Put your code here
                            self.navigationController?.popViewController(animated: true)

                          })
                        self.present(alertController, animated: true, completion: nil)
                       
                       
                        //showToast(uiview: self, msg: result["message"]! as! String)
                    }
                   
        })
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        txtEmail.setRadius(radius: 10)
        txtEmail.setRadiusBorder(color: UIColor.white)
        txtEmail.setLeftPaddingPoints(15, strImage: "")
        btnSend.setRadius(radius: btnSend.frame.height/2)
        
        
        
        lblforgotpassword.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
        lblenteryouremail.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
        txtEmail.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont )
     imgRunapp.mixedImage = MixedImage(normal: UIImage(named: "run1") ?? UIImage(), night: UIImage(named: "Run-app-icon") ?? UIImage())
     baseview.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
       // imgRunapp.mixedImage = MixedImage(normal:UIImage(named: "run")!, night: UIImage(named: "Run-app-icon")!)
      backbtn.setMixedImage(MixedImage(normal:"left-arrow-B", night:"left-arrow"), forState: .normal)
        txtEmail.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
    
        
     
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Textfield Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- Button Action
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtEmail.text == ""
        {
            showAlert(uiview: self, msg: "Please enter email", isTwoButton: false)
        }
        else if !txtEmail.isValidEmail()
        {
            showAlert(uiview: self, msg: "Please enter valid email", isTwoButton: false)
        }
        else
        {
            forgotPassword()
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
