//
//  ReportUserViewController.swift
//  RunApp
//
//  Created by Mac on 01/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import NightNight

class ReportUserViewController: UIViewController {

    
    var reason = ""
    var reportedId = ""
    @IBOutlet weak var reportuser: UIButton!
   
    var levelOfCompositionList = [[String : Any]]()
    
    @IBOutlet weak var txtfeedback: UITextView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
       
        reportuser.layer.borderColor = UIColor.white.cgColor
        reportuser.layer.borderWidth = 2
        
        txtfeedback.layer.borderColor = UIColor.white.cgColor
        txtfeedback.layer.borderWidth = 2
        
         getREPORTList()
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        reportuser.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        txtfeedback.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
           
        
        
        // Do any additional setup after loading the view.
    }
    
    
     @IBAction func btnLOCAction(_ sender: UIButton) {
        
        
           self.view.endEditing(true)
           if levelOfCompositionList.count != 0
           {
               ActionSheetStringPicker.show(withTitle: "Select Reason", rows: levelOfCompositionList.map({ $0["Reason"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                   picker, indexes, values in
                   self.reportuser.setTitle("\(values!)", for: .normal)
                   print("values: \(values!)")
                self.reason = self.levelOfCompositionList[indexes]["ReasonID"] as? String ?? ""
                   //self.reason = "\(values!)"
                
      
                   return
               }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
           }
        
        
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getREPORTList()
       {
        
          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
        
          
        
           callApi(ServiceList.SERVICE_URL+ServiceList.report_reason_list_get_API,
                   method: .get,
                extraHeader: header,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           self.levelOfCompositionList = result["data"] as? [[String:Any]] ?? []
                   
                           for i in 0..<self.levelOfCompositionList.count {
                               
                                   if self.reason == self.levelOfCompositionList[i]["ReasonID"] as? String ?? ""
                                   {
                                     self.reportuser.setTitle("\(self.levelOfCompositionList[i]["Reason"] as? String ?? "")", for: .normal)
                            }
                               
                           }
                           
                       }
           })
        
       }
       

    @IBAction func btn_back(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
        
    }
    
     func reportUser()
        {
        let parameters = [
                              "CreationUserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "Reason" :reason ,
                              "ReportUserID" : reportedId,
                              "Comment" : txtfeedback.text ?? ""
                          ]

            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ]
    //        let url = ServiceList.SERVICE_URL+ServiceList.CHANGE_PASSWORD_LOGIN
    //        let headers = [
    //            "Content-Type":"application/x-www-form-urlencoded",
    //            "authorization" : "apiKey"
    //        ]
    //
    //        let configuration = URLSessionConfiguration.default
    //        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
    //        //let params : [String : Any] = ["param1":param1,"param2":param2]
    //
    //        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
    //
    //            if let JSON = response.result.value {
    //                print("JSON: \(JSON)")
    //
    //            } else{
    //                print("Request failed with error: ",response.result.error ?? "Description not available :(")
    //            }
    //        }
            callApi(ServiceList.SERVICE_URL+ServiceList.reportUser,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)

                        if result.getBool(key: "status")
                        {

                            showToast(uiview: self, msg: result.getString(key: "message"))
           
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                            
            
                        }
                        showToast(uiview: self, msg: result.getString(key: "message"))

            })
        }
       
    
    @IBAction func btn_submit(_ sender: Any) {
        
        if reason == ""{
            showToast(uiview: self, msg:"Please Select Proper Reason")

        }
        else if   txtfeedback.text == ""
        {
          showToast(uiview: self, msg:"Please Provide some comment")
        
                
            }
            else{
            
                reportUser()
            }
        }
        
    
}
