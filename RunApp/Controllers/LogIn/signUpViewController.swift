//
//  signUpViewController.swift
//  RunApp
//
//  Created by My Mac on 07/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
// https://stackoverflow.com/questions/52840945/iphone-has-denied-the-launch-request

import UIKit
import Foundation
import SVProgressHUD
import Alamofire
import ActionSheetPicker_3_0
import ObjectMapper
import CoreLocation
import EasyTipView
import NightNight
import FirebaseAnalytics
import Firebase

class CollectionViewCell: UICollectionViewCell {
   
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnOutlet: UIButton!

    //@IBOutlet weak var viwewPop: UIView!
}

class signUpViewController: UIViewController , UICollectionViewDelegate ,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , UINavigationControllerDelegate, UIImagePickerControllerDelegate , UITextFieldDelegate , CLLocationManagerDelegate , EasyTipViewDelegate
{
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet var baseview: UIView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var lblcurrentteam: UITextField!
    @IBOutlet weak var lblhowwillyou: UILabel!
    @IBOutlet weak var imgrightarrow: UIImageView!
    @IBOutlet weak var lblheight: UILabel!
    @IBOutlet weak var imgrigtharrow1: UIImageView!
    @IBOutlet weak var rightarrow3: UIImageView!
    @IBOutlet weak var imagcalender: UIImageView!
    @IBOutlet weak var lblbirthdate: UILabel!
    @IBOutlet weak var lblposition: UILabel!
    @IBOutlet weak var rightarrow4: UIImageView!
    @IBOutlet weak var lblLOC: UILabel!
    @IBOutlet weak var lbldistance: UILabel!
    @IBOutlet weak var imagelocation: UIImageView!
    @IBOutlet weak var lblacceptterms: UILabel!
    
    
    
    
    
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("Dismiss EasyTip")
    }
    
    
    
    
    
    
    @IBOutlet weak var collectionPosition: UICollectionView!
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var imgOutlet: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPasword: UITextField!
    @IBOutlet weak var txtHomeTown: UITextField!
    @IBOutlet weak var txtSchool: UITextField!
    
    @IBOutlet weak var btnPlayer: UIButton!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnInches: UIButton!
    
    @IBOutlet weak var btnDateOutlet: UIButton!
    @IBOutlet weak var btnFeetOutlet: UIButton!
    
    @IBOutlet weak var btnCheckOutlet: UIButton!
    @IBOutlet weak var viewDistance: UIView!
    @IBOutlet weak var btnProfessionOutlet: UIButton!
    
    @IBOutlet weak var signUpOutlet: UIButton!
    
    var positionList = [[String : Any]]()
    var levelOfCompositionList = [[String : Any]]()
    var userTypeList = [[String : Any]]()
    var dictSocial = [String : Any]()
    var dictEdit = [[String : Any]]()
    var picker = UIImagePickerController()
    var isImage = Bool()
    var strDOB , strDistance , strUserTypeId , strLOCId : String?
    var GetDOB , GetDistance, GetUserTypeId , GetPositionId , GetLOCId : String?
    var selectedPosition = [String]()
    
    @IBOutlet weak var btnFirstSubOutlet: UIButton!
    @IBOutlet weak var btnSecondSubOutlet: UIButton!
    @IBOutlet weak var btnThirdSubOutlet: UIButton!
    @IBOutlet weak var btnFourthSubOutlet: UIButton!
    @IBOutlet weak var btnFifthSubOutlet: UIButton!
    @IBOutlet weak var btnSixSubOutlet: UIButton!
    @IBOutlet weak var ProgressiveOutlet: UIProgressView!
    @IBOutlet weak var lblChangeOutlet: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var imgLocationView: NSLayoutConstraint!
    @IBOutlet weak var heightPassword: NSLayoutConstraint!
    @IBOutlet weak var heightConfirmPassword: NSLayoutConstraint!
    
    @IBOutlet weak var topLeadingPassword: NSLayoutConstraint!
    @IBOutlet weak var lblStarPassword: UILabel!
    
    @IBOutlet weak var lblStarConfirmPassword: UILabel!
    @IBOutlet weak var topLeadingConfirmPassword: NSLayoutConstraint!
    
    let geocoder = CLGeocoder()
    var Latitude = Double()
    var Longitude = Double()
    
    var locationManager:CLLocationManager!
    var userData = [String: Any]()
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    
    var preferences = EasyTipView.Preferences()
    var easyTipView : EasyTipView!
    
    let viewDemo = UIView()

    //MARK:- Web service
   
    func getPositionList()
    {
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_POSITION_LIST,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.positionList = result["data"] as? [[String:Any]] ?? []
                        
                        print("positionList: \(self.positionList)")
                        
                        for i in 0..<self.positionList.count {
                            if self.GetPositionId == self.positionList[i]["PositionID"] as? String ?? ""
                            {
                                print("PositionID Matched")
                            }
                        }
                        
                        self.collectionPosition.reloadData()
                    }
        })
    }
    
    func getLOCList()
    {
      
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOC_LIST,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.levelOfCompositionList = result["data"] as? [[String:Any]] ?? []
                
                        for i in 0..<self.levelOfCompositionList.count {
                            if self.GetLOCId == self.levelOfCompositionList[i]["CLVLID"] as? String ?? ""
                            {
                                self.btnProfessionOutlet.setTitle("\(self.levelOfCompositionList[i]["CLVLTitle"] as? String ?? "")", for: .normal)
                            }
                        }
                        
                    }
        })
    }
    
    func getUserTypeList()
    {
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_USER_TYPE_LIST,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.userTypeList = result["data"] as? [[String:Any]] ?? []
                        for i in 0..<self.userTypeList.count {
                            if self.strUserTypeId == self.userTypeList[i]["RoleID"] as? String ?? ""
                            {
                                print("Matched")
                                self.btnPlayer.setTitle("\(self.userTypeList[i]["RoleName"] as? String ?? "")", for: .normal)
                            }
                        }
                    }
        })
    }
    
   
    //pod 'EasyTipView', '~> 1.0.2'
    
    func RegisterService()
    {
        
        var parameters:[String : Any]
        print(StoredData.shared.playerId)
        print(UserDefaults.standard.value(forKey:"playerId") ?? "")
       
    if (StoredData.shared.playerId != nil)
{
            parameters = ["FirstName" : txtName.text!,
                           "Password" : txtPasword.text!,
                           "EmailID" : txtEmail.text!,
                           "School" : txtSchool.text!,
                           "Hometown" : txtHomeTown.text!,
                           "RoleID" : strUserTypeId!,
                           "HeightFT" : btnFeetOutlet.currentTitle!,
                           "HeightInch" : btnInches.currentTitle!,
                           "BirthDate" : strDOB!,
                           "PossitionID" : selectedPosition.joined(separator: ","),
                           "CLVLID" : strLOCId!,
                           "DefaultDistance" : strDistance ?? "0",
                            "Username" : txtUserName.text!,
                            "usertype" : "normal",
                            "social_id" : "",
                            "ConfirmPassword":txtConfirmPassword.text! ,
                            "Score":0 ,
                            "Points":0 ,
                            "Latitude": Latitude ,
                            "Longitude" : Longitude ,
                            "Player_ID" : StoredData.shared.playerId!
                        
                ] as [String : Any]
        }
            
        else {
             parameters = ["FirstName" : txtName.text!,
                              "Password" : txtPasword.text!,
                              "EmailID" : txtEmail.text!,
                              "School" : txtSchool.text!,
                              "Hometown" : txtHomeTown.text!,
                              "RoleID" : strUserTypeId!,
                              "HeightFT" : btnFeetOutlet.currentTitle!,
                              "HeightInch" : btnInches.currentTitle!,
                              "BirthDate" : strDOB!,
                              "PossitionID" : selectedPosition.joined(separator: ","),
                              "CLVLID" : strLOCId!,
                              "DefaultDistance" : strDistance ?? "0",
                              "Username" : txtUserName.text!,
                              "usertype" : "normal",
                              "social_id" : "",
                              "ConfirmPassword":txtConfirmPassword.text! ,
                              "Score":0 ,
                              "Points":0 ,
                              "Latitude": Latitude ,
                              "Longitude" : Longitude ,
                              "Player_ID" : "1234"
                
                ] as [String : Any]
        }
        
        if dictSocial.count > 0
        {
            parameters["usertype"] = dictSocial["type"] as? String
            parameters["social_id"] = dictSocial["id"] as? String
        }
        
        let imgdata = imgOutlet.image?.jpegData(compressionQuality: 0.5)!
        
        self.callApi(ServiceList.SERVICE_URL+ServiceList.ADD_REGISTER_API,
                     method: .post,
                     param: parameters,
                     data: [imgdata!],
                     dataKey: ["Profile_Image"]) { (result) in  //file_name
                        
                       // print(result)
                        
                        if result.getBool(key: "status")
                        {
                            var dict = result.getDictionary(key: "data")
                            for (key, value) in dict
                            {
                                let val : NSObject = value as! NSObject;
                                dict[key] = val.isEqual(NSNull()) ? "" : value
                            }
                            
                            dict["login_token"] = result.getString(key: "login_token")
                            dict["id"] = result.getString(key: "id")
                            
                            print(dict)
                            UserDefaults.standard.setUserDict(value: dict)
                            UserDefaults.standard.setIsLogin(value: true)
                            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
                            nextViewController.IsFromReg = "true"
                
            self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                            
                            
//                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
//                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        
                       // showToast(uiview: self, msg: result.getString(key: "message"))
                        showAlert(uiview: self, msg: result.getString(key: "message"), isTwoButton: false)
        }
    }
    
    func UpdateServiceApi()
    {
        var parameters = ["FirstName" : txtName.text!,
                          "UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "EmailID" : txtEmail.text!,
                          "School" : txtSchool.text!,
                          "Hometown" : txtHomeTown.text!,
                          "RoleID" : strUserTypeId!,
                          "HeightFT" : btnFeetOutlet.currentTitle!,
                          "HeightInch" : btnInches.currentTitle!,
                          "BirthDate" : btnDateOutlet.currentTitle!,
                          "PossitionID" : selectedPosition.joined(separator: ","),
                          "CLVLID" : GetLOCId!,
                          "DefaultDistance" : GetDistance!,
                          "Username" : txtUserName.text!,
                          "usertype" : "normal",
                          "social_id" : "",
                          "Score":0 ,
                          "Points":0 ,
                          "Latitude": Latitude ,
                          "Longitude" : Longitude
            
            ] as [String : Any]
        
        if dictSocial.count > 0
        {
            parameters["usertype"] = dictSocial["type"] as? String
            parameters["social_id"] = dictSocial["id"] as? String
        }
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        let imgdata = imgOutlet.image?.jpegData(compressionQuality: 0.5)!
        
        self.callApi(ServiceList.SERVICE_URL+ServiceList.EDIT_REGISTER_USER,
                     method: .post,
                     param: parameters,
                     extraHeader: header ,
                     data: [imgdata!],
                     dataKey: ["Profile_Image"]) { (result) in
                        
                        // print(result)
                    
                        if result.getBool(key: "status")
                        {
                            self.ShowAlertUpdate(msg: result.getString(key: "message"), isTwoButton: false)
                        }
                            
                        else {
                          //  showToast(uiview: self, msg: result.getString(key: "message"))
                            showAlert(uiview: self, msg: result.getString(key: "message"), isTwoButton: false)
                        }
        }
    }
    
    //MARK:- ALERT CONTROLLER
    func ShowAlertUpdate(msg : String , isTwoButton : Bool) {
        
        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: msg)
        
        let action1 = EMAlertAction(title: "Ok", style: .normal) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            // Perform Action
        }
        
        alert.addAction(action: action1)
        if isTwoButton {
            alert.addAction(action: action2)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- ViewCircular
    
    func makeViewCircular(view: UIView) {
        view.layer.cornerRadius = view.bounds.size.width / 2.0
        view.clipsToBounds = true
        view.layer.borderWidth = 1
      //  view.layer.borderColor = UIColor.white.cgColor
    }
    
    func makeColorChange(view: [UIButton],color: UIColor) {
        
        for btn in view {
            btn.layer.backgroundColor = color.cgColor
        }
    }
    
    
    //MARK:- VIEW LIFE CYCLE

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        //var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "Futura-Medium", size: 13)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = UIColor(hue:0.46, saturation:0.99, brightness:0.6, alpha:1)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        preferences.animating.dismissDuration = 1.5
        
        // Above easy tip
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc func btnCellTipAction(_ sender : UIButton)
    {
         print("Cell is Pressed")
//         let color = UIColor(hue:0.46, saturation:0.99, brightness:0.6, alpha:1)
//         viewDemo.backgroundColor = color
//         viewDemo.frame = CGRect(x: 50, y: 50, width: 50, height: 50)
        let section = sender.tag / 100
        let row = sender.tag % 100
        let indexPath = NSIndexPath(row: row, section: section)
        
        GetPositionId = ""
        
        if selectedPosition.contains(positionList[indexPath.row]["PositionID"] as? String ?? "")
        {
            selectedPosition = selectedPosition.filter{$0 != positionList[indexPath.row]["PositionID"] as? String }
        }
            
        else
        {
            selectedPosition.append(positionList[indexPath.row]["PositionID"] as? String ?? "")
            var preferences = EasyTipView.Preferences()
            //preferences.drawing.font = UIFont(name: "Futura-Medium", size: 13)!
            preferences.drawing.backgroundColor = UIColor(hue:0.58, saturation:0.1, brightness:1, alpha:1)
            preferences.drawing.foregroundColor = UIColor.darkGray
            preferences.drawing.textAlignment = NSTextAlignment.center
            
            preferences.animating.dismissTransform = CGAffineTransform(translationX: 100, y: 0)
            preferences.animating.showInitialTransform = CGAffineTransform(translationX: -100, y: 0)
            preferences.animating.showInitialAlpha = 0
            preferences.animating.showDuration = 1
            preferences.animating.dismissDuration = 1
           // preferences.animating.dismissOnTap = false
            //preferences.drawing.arrowPosition = .bottom
            let PositionInfo = positionList[indexPath.row]["PositionInfo"] as? String ?? ""
    
            easyTipView = EasyTipView(text: PositionInfo, preferences: preferences)
            
            easyTipView.show(forView: sender, withinSuperview: nil)
            
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(callback), userInfo: nil, repeats: false)
        }
        
        collectionPosition.reloadData()
    }
    
    @objc func callback() {
        easyTipView.dismiss()
        
        print("done")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        baseview.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        txtUserName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtUserName.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtPasword.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtPasword.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtConfirmPassword.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtConfirmPassword.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtHomeTown.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtHomeTown.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtName.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtEmail.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtEmail.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtSchool.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtSchool.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblcurrentteam.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblcurrentteam.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
       // ProgressiveOutlet.mixedBackgroundColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnFirstSubOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnSecondSubOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnThirdSubOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnFourthSubOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnFifthSubOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnSixSubOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         ProgressiveOutlet.mixedTrackTintColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        
//        imgrightarrow.mixedImage = MixedImage(normal: UIImage(named: "right-arrow") ?? UIImage(), night: UIImage(named: "right-arrowGray") ?? UIImage())
//         rightarrow3.mixedImage = MixedImage(normal: UIImage(named: "right-arrow") ?? UIImage(), night: UIImage(named: "right-arrowGray") ?? UIImage())
//         rightarrow3.mixedImage = MixedImage(normal: UIImage(named: "right-arrow") ?? UIImage(), night: UIImage(named: "right-arrowGray") ?? UIImage())
//         rightarrow3.mixedImage = MixedImage(normal: UIImage(named: "right-arrow") ?? UIImage(), night: UIImage(named: "right-arrowGray") ?? UIImage())
        
        btnPlayer.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnPlayer.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        
        btnFeetOutlet.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnFeetOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnInches.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnInches.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnDateOutlet.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnDateOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnProfessionOutlet.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnProfessionOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnback.setMixedImage(MixedImage(normal:"left-arrow-B", night:"left-arrow"), forState: .normal)
        btnCheckOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
        btnCheckOutlet.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnCheckOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
       // btnCheckOutlet.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        //btnCheckOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
        
        
        lblChangeOutlet.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblLOC.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblheight.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lbldistance.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblposition.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblbirthdate.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblhowwillyou.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        lblacceptterms.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(signUpViewController.tapFunction))
        lblLOC.isUserInteractionEnabled = true
        lblLOC.addGestureRecognizer(tap)
        
        self.view.layoutIfNeeded()
        makeViewCircular(view: btnFirstSubOutlet)
        makeViewCircular(view: btnSecondSubOutlet)
        makeViewCircular(view: btnThirdSubOutlet)
        makeViewCircular(view: btnFourthSubOutlet)
        makeViewCircular(view: btnFifthSubOutlet)
        makeViewCircular(view: btnSixSubOutlet)
        self.view.layoutIfNeeded()
        
        
        
        
        
        
        if dictEdit.count > 0 {
            lblChangeOutlet.text = "Update Account"
            self.view.layoutIfNeeded()
            heightPassword.constant = 0
            heightConfirmPassword.constant = 0
            topLeadingPassword.constant = 0
            topLeadingConfirmPassword.constant = 0
            lblStarPassword.isHidden = true
            lblStarConfirmPassword.isHidden = true
            self.view.layoutIfNeeded()
            
            txtEmail.isEnabled = false
            txtEmail.isUserInteractionEnabled = false
            
                signUpOutlet.setTitle("Update Profile", for: .normal)
                let imgURL = ServiceList.IMAGE_URL + dictEdit[0].getString(key: "UserProfile")
            
             if imgURL != "" {
                imgOutlet.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                      completed: nil)
                isImage = true
            }
       
              txtName.text = dictEdit[0].getString(key: "FirstName")
              txtUserName.text = dictEdit[0].getString(key: "username")
              txtPasword.text = dictEdit[0].getString(key: "password")
              txtHomeTown.text = dictEdit[0].getString(key: "Hometown")
              txtConfirmPassword.text = dictEdit[0].getString(key: "FirstName")
              txtSchool.text = dictEdit[0].getString(key: "School")
              txtEmail.text = dictEdit[0].getString(key: "email")
            
              btnInches.setTitle(dictEdit[0].getString(key: "HeightInch"), for: .normal)
              // btnDateOutlet.setTitle(dictEdit[0].getString(key: "DOB"), for: .normal)
              btnFeetOutlet.setTitle(dictEdit[0].getString(key: "HeightFT"), for: .normal)
              GetLOCId = dictEdit[0].getString(key: "CLVLID")
              strUserTypeId = dictEdit[0].getString(key: "RoleID")
              //GetPositionId = dictEdit[0].getString(key: "PositionID")
              GetDistance = dictEdit[0].getString(key: "DefaultDistance")
              GetDOB = dictEdit[0].getString(key: "DOB")
              GetPositionId = dictEdit[0].getString(key: "PossitionID")
              GetDistance = dictEdit[0].getString(key: "DefaultDistance")
              let usertype = dictEdit[0].getString(key: "usertype")
              btnPlayer.setTitle(usertype, for: .normal)
             self.btnProfessionOutlet.setTitle("\(dictEdit[0].getString(key: "CLVLTitle"))", for: .normal)
        
            print("GetDistance: \(GetDistance!)")
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd , yyyy"
            
            if GetDOB != "" {
                let date: Date? = dateFormatterGet.date(from: "\(GetDOB as! String)")!
                btnDateOutlet.setTitle(dateFormatter.string(from: date!), for: .normal)
            }
            
            if GetDistance == "0" {
                ProgressiveOutlet.progress = 1/6*0
                
                if btnFirstSubOutlet.backgroundColor == UIColor.white {
                    makeColorChange(view: [btnFirstSubOutlet], color: UIColor.red)
                    
                }
                    
                else {
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
                }
            }
                
            else if GetDistance == "10" {
                ProgressiveOutlet.progress = 1/6*1.2
                
                if btnSecondSubOutlet.backgroundColor == UIColor.white {
                    
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet], color: UIColor.red)
                    
                }
                    
                else {
                    makeColorChange(view: [btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
                }
            }
                
            else if GetDistance == "20" {
                ProgressiveOutlet.progress = 1/6*2.4
                
                if btnThirdSubOutlet.backgroundColor == UIColor.white {
                    
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet], color: UIColor.red)
                }
                    
                else {
                    
                    makeColorChange(view: [btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
                }
            }
                
            else if  GetDistance == "30" {
                ProgressiveOutlet.progress = 1/6*3.5
                
                if btnFourthSubOutlet.backgroundColor == UIColor.white {
                    
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet], color: UIColor.red)
                }
                    
                else {
                    makeColorChange(view: [btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
                }
            }
                
            else if GetDistance == "40" {
                ProgressiveOutlet.progress = 1/6*4.7
                
                if btnFifthSubOutlet.backgroundColor == UIColor.white {
                    
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet], color: UIColor.red)
                }
                    
                else {
                    makeColorChange(view: [btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
                }
            }
                
            else {
               
                ProgressiveOutlet.progress = 1/6*5.8
                
                if btnSixSubOutlet.backgroundColor == UIColor.white {
                    
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.red)
                    
                }
                    
                else {
                    makeColorChange(view: [btnSixSubOutlet], color: UIColor.white)
                }
            }
            
            UIView.animate(withDuration: 0.5, animations: {
                if self.GetDistance == "0" {
                    self.strDistance = "0"
                    self.imgLocation.isHidden = true
                    self.imgLocationView.constant = self.btn1.frame.origin.x - 5
                }
                else if self.GetDistance == "10" {
                    self.strDistance = "10"
                    self.imgLocation.isHidden = false
                    self.imgLocation.image = UIImage(named: "A")
                    self.imgLocationView.constant = self.btn2.frame.origin.x - 5
                }
                else if self.GetDistance == "20" {
                    self.strDistance = "20"
                    self.imgLocation.isHidden = false
                    self.imgLocation.image = UIImage(named: "B")
                    self.imgLocationView.constant = self.btn3.frame.origin.x - 5
                }
                else if self.GetDistance == "30" {
                    self.strDistance = "30"
                    self.imgLocation.isHidden = false
                    self.imgLocation.image = UIImage(named: "C")
                    self.imgLocationView.constant = self.btn4.frame.origin.x - 5
                }
                else if self.GetDistance == "40" {
                    self.strDistance = "40"
                    self.imgLocation.isHidden = false
                    self.imgLocation.image = UIImage(named: "D")
                    
//                    var frm: CGRect = self.imgLocation.frame
//                    frm.origin.x = self.btn5.frame.origin.x - 5
//                    frm.origin.y = 21.0 //frm.origin.y
//                    frm.size.width = frm.size.width
//                    frm.size.height = frm.size.height
//                    self.imgLocation.frame = frm
                     //self.imgLocation.frame.origin.x = self.btn5.frame.origin.x - 5
                    self.imgLocationView.constant = self.btn5.frame.origin.x - 5
                }
                else {
                    self.strDistance = "50"
                    self.imgLocation.isHidden = false
                    self.imgLocation.image = UIImage(named: "E")
                    self.imgLocationView.constant = self.btn6.frame.origin.x - 5
                }
            }, completion: nil)
            
           }
        
        cornerRediusFun()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedHide(tapGestureRecognizer:)))
        imgOutlet.isUserInteractionEnabled = true
        imgOutlet.addGestureRecognizer(tapGestureRecognizer)
        btnCamera.setRadius(radius: btnCamera.frame.height/2)
        imgOutlet.setRadius(radius: imgOutlet.frame.height/2)
        btnCheckOutlet.accessibilityLabel = "off"
        
        for view in vwMain.subviews as [UIView] {
            if let txt = view as? UITextField {
//                txt.setRadius(radius: 10)
//                txt.setRadiusBorder(color: UIColor.white)
                txt.setLeftPaddingPoints(15, strImage: "")
                txt.layer.cornerRadius = 10
                txt.layer.borderWidth = 2.0
                
            }
        }
        
        picker.delegate = self
        
        if dictSocial.count > 0
        {
            txtEmail.text = dictSocial["email"] as? String
            txtUserName.text = dictSocial["name"] as? String
            txtName.text = dictSocial["first_name"] as? String
            var imgURL = String()
            if dictSocial["type"] as? String ?? "" == "fb"
            {
                imgURL = ((dictSocial["picture"] as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as? String ?? ""
            }
            else
            {
                imgURL = dictSocial["picture"] as? String ?? ""
            }
            
            isImage = true
            imgOutlet.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                  completed: nil)
        }
        
        getPositionList()
        getLOCList()
        getUserTypeList()
    
         // let address = "Rio de Janeiro, Brazil"
         //let address = "8787 Snouffer School Rd, Montgomery Village, MD 20879"
        
        let address = "Ashley River Rd, Charleston, SC, USA"
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error ?? "")
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                self.Latitude = coordinates.latitude
                self.Longitude = coordinates.longitude
                print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
            }
        })
    }
   @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
    let AllDesc = NSMutableString()
    for index in 0..<levelOfCompositionList.count
    {
        AllDesc.append("\(levelOfCompositionList[index]["CLVLTitle"] as! String)  -  \(levelOfCompositionList[index]["CLVLDesc"] as! String)\n\n ")
    }
    
    let alertController = UIAlertController(title: ConstantVariables.Constants.Project_Name, message:
        String(AllDesc), preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
        
    }))
    
    self.present(alertController, animated: true, completion: nil)
    }
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        var address = ""
        
        let userLocation :CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        self.Latitude = userLocation.coordinate.latitude
        self.Longitude = userLocation.coordinate.longitude
        
//        self.Latitude = 40.647593
//        self.Longitude = -73.943995
    
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            
            let placemark = placemarks
            if placemark?.count ?? 0>0 {
                let placemark = placemarks![0]
//                print(placemark.locality!)
//                print(placemark.administrativeArea!)
//                print(placemark.country!)
                print(placemark.addressDictionary as Any)
                
                            // Location name
                            if let locationName = placemark.addressDictionary!["Name"] as? String {
                                //print(locationName)
                                address = locationName + ","
                            }
                
                            // Street address
                            if let SubLocality = placemark.addressDictionary!["SubLocality"] as? String {
                               // print(street)
                                 address += SubLocality + ","
                            }
               
                            // City
                            if let city = placemark.addressDictionary!["City"] as? String {
                                //print(city)
                                 address += city + ","
                            }
                
                              // State
                              if let State = placemark.addressDictionary!["State"] as? String {
                                address += State + ","
                             }

                            // Country
                            if let country = placemark.addressDictionary!["Country"] as? String {
                                //print(country)
                                 address += country
                            }
                
//                self.txtHomeTown.text = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
                
                self.txtHomeTown.text = address
                
                self.txtHomeTown.text = "Brooklyn, NY, USA"
                self.locationManager.stopUpdatingLocation()
            }
        }
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    

 //        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
 //        let geoCoder = CLGeocoder()
 //         locationManager.stopUpdatingLocation()
 //        let location = CLLocation(latitude: locValue.latitude,
 //                                  longitude: locValue.longitude)
 //
 //
 
 
 
    
    //MARK:- Gesture
    
    @objc func imageTappedHide(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
        openGalleryCameraPicker(picker: picker)
    }
    
    //MARK:- COLLETION METHODS
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return positionList.count
    }
    
    // lblName viwewPop CollectionViewCell
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColletionCell", for: indexPath) as! CollectionViewCell
        cell.lblName.text = positionList[indexPath.row]["Position"] as? String
        
        cell.btnOutlet.tag = indexPath.row
        cell.btnOutlet.addTarget(self, action: #selector(self.btnCellTipAction(_:)), for: .touchUpInside)
        cell.btnOutlet.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        
        let arr:NSArray = (GetPositionId?.components(separatedBy: ",") as NSArray? ?? [])
        
       
        for i in 0..<arr.count {
            
          //  let formatter = NumberFormatter()
            let str1 =  arr.object(at: i) as? String
            let str2 =  positionList[indexPath.row]["PositionID"] as? String
            
            if str1  == str2 {
                
                cell.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.nav_red)
                
             // let number = formatter.string(from: str1)
                selectedPosition.append(str1!)
            }
        }
        
//        if GetPositionId == positionList[indexPath.row]["PositionID"] as? String {
//            cell.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.nav_red)
//            selectedPosition.append(GetPositionId!)
//        }
            
         if selectedPosition.contains(positionList[indexPath.row]["PositionID"] as? String ?? "")
        {
            cell.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.nav_red)
        }
            
        else
        {
            cell.backgroundColor = UIColor.clear
        }
        
//        cell.setRadius(radius: 10)
//        cell.setRadiusBorder(color: UIColor.white)
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 2.0
        cell.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        cell.lblName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//          GetPositionId = ""
//
//        if selectedPosition.contains(positionList[indexPath.row]["PositionID"] as? String ?? "")
//        {
//            selectedPosition = selectedPosition.filter{$0 != positionList[indexPath.row]["PositionID"] as? String}
//        }
//
//        else
//        {
//            selectedPosition.append(positionList[indexPath.row]["PositionID"] as? String ?? "")
//        }
//
//        collectionPosition.reloadData()
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let sizeName = positionList[indexPath.row]["Position"] as? String ?? ""
        let expectedWidth = heightForLable(text: sizeName, font: UIFont.boldSystemFont(ofSize: 15), height: 40)
        return CGSize(width: expectedWidth + 30, height: 40)
    }

    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //    {
    //       return  CGSize(width: collectionView.frame.width/3 - 10, height: 70)
    //    }
    
    func cornerRediusFun() {
        signUpOutlet.setRadius(radius: signUpOutlet.frame.height/2)
        signUpOutlet.setRadius(radius: signUpOutlet.frame.height/2)
        
        btnCheckOutlet.setRadius(radius: 5)
       // btnCheckOutlet.setRadiusBorder(color: UIColor.white)
        btnCheckOutlet.layer.borderWidth = 2.0
//        btnPlayer.setRadius(radius: 10)
//        btnPlayer.setRadiusBorder(color: UIColor.white)
        btnPlayer.layer.cornerRadius = 10
        btnPlayer.layer.borderWidth = 2.0
        
//        btnInches.setRadius(radius: 10)
        //btnInches.setRadiusBorder(color: UIColor.white)
        btnInches.layer.cornerRadius = 10
        btnInches.layer.borderWidth = 2.0
        
        
//        btnFeetOutlet.setRadius(radius: 10)
       // btnFeetOutlet.setRadiusBorder(color: UIColor.white)
          btnFeetOutlet.layer.cornerRadius = 10
          btnFeetOutlet.layer.borderWidth = 2.0
        
        btnDateOutlet.layer.cornerRadius = 10
        btnDateOutlet.layer.borderWidth = 2.0
        
       // btnDateOutlet.setRadius(radius: 10)
       // btnDateOutlet.setRadiusBorder(color: UIColor.white)
        
        btnProfessionOutlet.layer.cornerRadius = 10
        btnProfessionOutlet.layer.borderWidth = 2.0
   //     btnProfessionOutlet.setRadius(radius: 10)
        //btnProfessionOutlet.setRadiusBorder(color: UIColor.white)
        
        btnCamera.layer.masksToBounds = true
        btnCamera.layer.cornerRadius = 2.0
        btnCamera.layer.borderWidth = 2.0
        btnCamera.layer.borderColor = UIColor.white.cgColor
        
    }
    
    //MARK:- BUTTON METHODS
    
    @IBAction func btnProgressClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            ProgressiveOutlet.progress = 1/6*0
            
            if btnFirstSubOutlet.backgroundColor == UIColor.white {
                makeColorChange(view: [btnFirstSubOutlet], color: UIColor.red)
            }
                
            else {
                makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
            }
        }
            
        else if sender.tag == 2 {
            ProgressiveOutlet.progress = 1/6*1.2
            
            if btnSecondSubOutlet.backgroundColor == UIColor.white {
                
                makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet], color: UIColor.red)
            }
                
            else {
                makeColorChange(view: [btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
            }
        }
            
        else if sender.tag == 3 {
            ProgressiveOutlet.progress = 1/6*2.4
            
            if btnThirdSubOutlet.backgroundColor == UIColor.white {
                
                makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet], color: UIColor.red)
            }
                
            else {
                
                makeColorChange(view: [btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
            }
        }
            
        else if sender.tag == 4 {
            ProgressiveOutlet.progress = 1/6*3.5
            
            if btnFourthSubOutlet.backgroundColor == UIColor.white {
                
                makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet], color: UIColor.red)
            }
            else {
                makeColorChange(view: [btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
            }
        }
            
        else if sender.tag == 5 {
            ProgressiveOutlet.progress = 1/6*4.7
            
            if btnFifthSubOutlet.backgroundColor == UIColor.white {
                
                makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet], color: UIColor.red)
            }
                
            else {
                makeColorChange(view: [btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.white)
            }
        }
            
        else {
            ProgressiveOutlet.progress = 1/6*5.8
            
            if btnSixSubOutlet.backgroundColor == UIColor.white {
                
                makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.red)
            }
                
            else {
                makeColorChange(view: [btnSixSubOutlet], color: UIColor.white)
            }
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            if sender.tag == 1 {
                self.strDistance = "0"
                self.imgLocation.isHidden = true
            }
            else if sender.tag == 2 {
                self.strDistance = "10"
                self.GetDistance = "10"
                self.imgLocation.isHidden = false
                self.imgLocation.image = UIImage(named: "A")
            }
            else if sender.tag == 3 {
                self.strDistance = "20"
                self.GetDistance = "20"
                self.imgLocation.isHidden = false
                self.imgLocation.image = UIImage(named: "B")
            }
            else if sender.tag == 4 {
                self.strDistance = "30"
                self.GetDistance = "30"
                self.imgLocation.isHidden = false
                self.imgLocation.image = UIImage(named: "C")
            }
            else if sender.tag == 5 {
                self.strDistance = "40"
                self.GetDistance = "40"
                self.imgLocation.isHidden = false
                self.imgLocation.image = UIImage(named: "D")
            }
            else {
                self.strDistance = "50"
                self.GetDistance = "50"
                self.imgLocation.isHidden = false
                self.imgLocation.image = UIImage(named: "E")
            }
            self.imgLocation.frame.origin.x = sender.frame.origin.x - 5
        }, completion: nil)
        
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func stringFromAny(_ value:Any?) -> String {
        if let nonNil = value, !(nonNil is NSNull) {
            return String(describing: nonNil)
        }
        return ""
    }
    @IBAction func btnBirthDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Select Date:", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            dateFormatterGet.locale = Locale.current
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd , yyyy"
            
            let date: Date? = dateFormatterGet.date(from:"\(value!)")
            print(dateFormatter.string(from: date!))
            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd/MM/yyyy"
            
            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
            self.strDOB = (dateFormatter1.string(from: date1!))
            
            print("Selected : \(self.strDOB!)")
            
            return
        }, cancel: { ActionStringCancelBlock in return },
           origin: sender.superview!.superview)
        datePicker?.maximumDate = Date()
       // datePicker.setValue(UIColor.black, forKeyPath: "textColor")
        datePicker?.show()
        
        
    }
    
    @IBAction func btnLOCAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if levelOfCompositionList.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Select LOC", rows: levelOfCompositionList.map({ $0["CLVLTitle"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnProfessionOutlet.setTitle("\(values!)", for: .normal)
                print("values: \(values!)")
                self.strLOCId = self.levelOfCompositionList[indexes]["CLVLID"] as? String ?? ""
     self.GetLOCId = self.levelOfCompositionList[indexes]["CLVLID"] as? String ?? ""
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
       // validation()
        let btnTitle = sender.titleLabel?.text
        if btnTitle == "SIGN UP" {
            validation()
        }
            
        else {
            editValidation()
        }
    }
    
  
    
    @IBAction func btn_termsandconditions(_ sender: Any) {
   
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as! TermsAndConditionViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    //  Remove "User " "Admin" from select User Type in register,  only show Player / Spectator
    
    @IBAction func btnUserTypeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if userTypeList.count != 0
        {
            
           let pkr = ActionSheetStringPicker.show(withTitle: "Select user type", rows: userTypeList.map({ $0["RoleName"] ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnPlayer.setTitle("\(values!)", for: .normal)
                self.strUserTypeId = self.userTypeList[indexes]["RoleID"] as? String ?? ""
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
           // pkr.sette = UIColor.black
           // pkr?.setValue(UIColor.blue, forKeyPath: "textColor")
        }
    }
    
    @IBAction func btnHeightFeetAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrFeet = [Int]()
        for i in 3..<11
        {
            arrFeet.append(i)
        }
        
        if arrFeet.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Height in feet", rows: arrFeet as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnFeetOutlet.setTitle("\(values!)", for: .normal)
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnHeightInchesAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrInches = [Int]()
        for i in 1..<13
        {
            arrInches.append(i)
        }
        
        if arrInches.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Height in inch", rows: arrInches as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnInches.setTitle("\(values!)", for: .normal)
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnCheckTermsAction(_ sender: UIButton) {
        if btnCheckOutlet.accessibilityLabel == "on"
        {
            btnCheckOutlet.accessibilityLabel = "off"
            btnCheckOutlet.setTitle("", for: .normal)
        }
        else
        {
            btnCheckOutlet.accessibilityLabel = "on"
            btnCheckOutlet.setTitle("✓", for: .normal)
        }
    }
    
    func heightForLable(text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 1
        label.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.width
    }
    
    //MARK:- Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            self.imgOutlet.contentMode = .scaleAspectFill
            self.imgOutlet.layer.masksToBounds = true
            self.imgOutlet.image = image
            self.isImage = true
        }
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    
    //MARK:- Textfield Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    //MARK:- Other Method
    
    func validation()
    {
        self.view.endEditing(true)
        
        let name = txtName.text!
        let user_name = txtUserName.text!
        
        if !isImage && txtName.text == "" && txtUserName.text == "" && txtEmail.text == "" && txtPasword.text == "" && txtConfirmPassword.text == "" && txtSchool.text == "" && btnPlayer.currentTitle == "Select user type" &&  btnProfessionOutlet.currentTitle == "Select LOC" && btnDateOutlet.currentTitle == "Select birthdate" && selectedPosition.count == 0 && btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        else if !isImage
        {
            showAlert(uiview: self, msg: "Please select profile image", isTwoButton: false)
        }
            
        else if txtName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter first name", isTwoButton: false)
        }
            
        else if name.count < 2
        {
            showAlert(uiview: self, msg: "Name should have minimum 2 characters And maximum 18 characters", isTwoButton: false)
        }
            
        else if name.count > 19
        {
            showAlert(uiview: self, msg: "Name should have maximum 18 characters", isTwoButton: false)
        }
            
        else if txtUserName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter User name", isTwoButton: false)
        }
            
        else if user_name.count < 2
        {
            showAlert(uiview: self, msg: "User name should have minimum 2 characters", isTwoButton: false)
        }
            
        else if user_name.count > 18
        {
            showAlert(uiview: self, msg: "User name should have maximum 18 characters", isTwoButton: false)
        }
            
        else if txtEmail.text == ""
        {
            showAlert(uiview: self, msg: "Please enter email", isTwoButton: false)
        }
        else if !txtEmail.isValidEmail()
        {
            showAlert(uiview: self, msg: "Please enter valid email", isTwoButton: false)
        }
        else if txtPasword.text == ""
        {
            showAlert(uiview: self, msg: "Please enter password", isTwoButton: false)
        }
        else if txtConfirmPassword.text == ""
        {
            showAlert(uiview: self, msg: "Please enter confirm password", isTwoButton: false)
        }
        else if txtPasword.text?.lowercased() != txtConfirmPassword.text?.lowercased()
        {
            showAlert(uiview: self, msg: "Both password does not match", isTwoButton: false)
        }
//        else if txtHomeTown.text == ""
//        {
//            showAlert(uiview: self, msg: "Please enter hometown", isTwoButton: false)
//        }
        else if btnPlayer.currentTitle == "Select user type"
        {
            showAlert(uiview: self, msg: "Please select user type", isTwoButton: false)
        }
//        else if btnFeetOutlet.currentTitle == "Feet"
//        {
//            showAlert(uiview: self, msg: "Please select height in feet", isTwoButton: false)
//        }
//        else if btnInches.currentTitle == "Inches"
//        {
//            showAlert(uiview: self, msg: "Please select height in inches", isTwoButton: false)
//        }
        else if btnDateOutlet.currentTitle == "Select birthdate"
        {
            showAlert(uiview: self, msg: "Please select birthdate", isTwoButton: false)
        }
        else if selectedPosition.count == 0
        {
            showAlert(uiview: self, msg: "Please select atleast one position", isTwoButton: false)
        }
        else if btnProfessionOutlet.currentTitle == "Select LOC"
        {
            showAlert(uiview: self, msg: "Please select LOC", isTwoButton: false)
        }
        else if btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please accept terms & condition", isTwoButton: false)
        }
        else
        {

            let address = txtHomeTown.text!
           
            geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                if((error) != nil){
                    print("Error", error ?? "")
                }
                if let placemark = placemarks?.first {
                    let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                    self.Latitude = coordinates.latitude
                    self.Longitude = coordinates.longitude
                    print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                    
                }
                self.RegisterService()
            })
        }
    }
    
    func editValidation()
    {
        self.view.endEditing(true)
        
        let name = txtName.text!
        let user_name = txtUserName.text!
        
        if !isImage && txtName.text == "" && txtUserName.text == "" && txtEmail.text == "" && txtSchool.text == "" && btnPlayer.currentTitle == "Select user type" &&  btnProfessionOutlet.currentTitle == "Select LOC" && btnDateOutlet.currentTitle == "Select birthdate" && selectedPosition.count == 0 && btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        else if !isImage
        {
            showAlert(uiview: self, msg: "Please select profile image", isTwoButton: false)
        }
        else if txtName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter first name", isTwoButton: false)
        }
            
        else if name.count < 2
        {
            showAlert(uiview: self, msg: "Name should have minimum 2 characters And maximum 18 characters", isTwoButton: false)
        }
            
        else if name.count > 18
        {
            showAlert(uiview: self, msg: "Name should have maximum 18 characters", isTwoButton: false)
        }
            
        else if txtUserName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter User name", isTwoButton: false)
        }
            
        else if user_name.count < 2
        {
            showAlert(uiview: self, msg: "User name should have minimum 2 characters", isTwoButton: false)
        }
            
        else if user_name.count > 18
        {
            showAlert(uiview: self, msg: "User name should have maximum 18 characters", isTwoButton: false)
        }
            
        else if txtEmail.text == ""
        {
            showAlert(uiview: self, msg: "Please enter email", isTwoButton: false)
        }
            
        else if !txtEmail.isValidEmail()
        {
            showAlert(uiview: self, msg: "Please enter valid email", isTwoButton: false)
        }
            
//        else if txtHomeTown.text == ""
//        {
//            showAlert(uiview: self, msg: "Please enter hometown", isTwoButton: false)
//        }
        else if btnPlayer.currentTitle == "Select user type"
        {
            showAlert(uiview: self, msg: "Please select user type", isTwoButton: false)
        }
//        else if btnFeetOutlet.currentTitle == "Feet"
//        {
//            showAlert(uiview: self, msg: "Please select height in feet", isTwoButton: false)
//        }
//        else if btnInches.currentTitle == "Inches"
//        {
//            showAlert(uiview: self, msg: "Please select height in inches", isTwoButton: false)
//        }
        else if btnDateOutlet.currentTitle == "Select birthdate"
        {
            showAlert(uiview: self, msg: "Please select birthdate", isTwoButton: false)
        }
        else if selectedPosition.count == 0
        {
            showAlert(uiview: self, msg: "Please select atleast one position", isTwoButton: false)
        }
        else if btnProfessionOutlet.currentTitle == "Select LOC"
        {
            showAlert(uiview: self, msg: "Please select LOC", isTwoButton: false)
        }
        else if btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please accept terms & condition", isTwoButton: false)
        }
        else
        {
            
            let address = txtHomeTown.text!
            
            geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                if((error) != nil){
                    print("Error", error ?? "")
                }
                if let placemark = placemarks?.first {
                    let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                    self.Latitude = coordinates.latitude
                    self.Longitude = coordinates.longitude
                    print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                    
                }
                 self.UpdateServiceApi()
                
            })
        }
    }
    
}

/*
 
extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}
 
*/

/*
 struct Team {
 let teamName:String
 let image:String
 let nextMatch:String
 let matches:[Match]?
 let fixtures:[Fixture]?
 }
 
 struct Match {
 let oppositeTeam:String
 let matchTimings:String
 let matchId:String
 }
 
 struct Fixture {
 let oppositeTeam:String
 let oppositeTeamScore:String
 let HomeTeamScore:String
 let HomeTeamCards:String
 let oppositeTeamCards:String
 let fixturesId:String
 }
 */
// parsing Data

var dataArray = [
    ["teamName":"Arsenal",
     "image":"imageName",
     "nextMatch":"in 2 days",
     "matches":[
        ["oppositeTeam":"teamName",
         "matchTimings":"121212",
         "matchId":"ID 213432"],
        ["oppositeTeam":"teamName",
         "matchTimings":"121212",
         "matchId":"ID 213432"]
        ],
     "fixtures":[
        ["oppositeTeam":"teamName",
         "oppositeTeamScore":"7",
         "HomeTeamScore":"4",
         "HomeTeamCards":"True",
         "oppositeTeamCards":"false",
         "fixturesId":"ID 213432"],
        
        ]
    ],["teamName":"Chelsea",
       "image":"imageName",
       "nextMatch":"in 2 days",
       "matches":[["oppositeTeam":"teamName",
                   "matchTimings":"121212",
                   "matchId":"ID 213432"],["oppositeTeam":"teamName",
                                           "matchTimings":"121212",
                                           "matchId":"ID 213432"]
        ],"fixtures":[["oppositeTeam":"teamName",
                       "oppositeTeamScore":"7",
                       "HomeTeamScore":"4",
                       "HomeTeamCards":"True",
                       "oppositeTeamCards":"false",
                       "fixturesId":"ID 213432"],["oppositeTeam":"teamName",
                                                  "oppositeTeamScore":"7",
                                                  "HomeTeamScore":"4",
                                                  "HomeTeamCards":"True",
                                                  "oppositeTeamCards":"false",
                                                  "fixturesId":"ID 213432"]
        ]
    ],["teamName":"India",
       "image":"imageName",
       "nextMatch":"null",
       "matches":[],
       "fixtures":[]
    ]]
// */


/*
 func loadData()
 {
 callApi(Path.getUserProfile, method: .post, param: ["UserProfileID": userId]) { (result) in
 
 print(result)
 if result.getBool(key: "success")
 {
 let data = result.getDictionary(key: "user_data")
 self.userData = data
 self.lblName.text = data.getString(key: "FirstName") + " " + data.getString(key: "LastName")
 self.lblUserType.text = data.getInt(key: "RegisterUserType") == 0 ? "Trendsetter" : "Stylist / Desiner"
 self.imgProfile.setImage(data.getString(key: "UserProfileImage"))
 let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.gesterHandeler1(_:)))
 self.imgProfile.isUserInteractionEnabled = true
 self.imgProfile.addGestureRecognizer(tapGesture)
 
 self.btnFollow.isSelected = data.getInt(key: "isFollowing") != 0
 self.lblFollow.text = data.getInt(key: "isFollowing") == 0 ? "Follow" : "Following"
 
 self.btnFollow.isHidden = data.getString(key: "Id") == StoredData.shared.userId
 self.lblFollow.isHidden = data.getString(key: "Id") == StoredData.shared.userId
 if data.getString(key: "Id") == StoredData.shared.userId
 {
 self.title = "MY PROFILE"
 self.navigationItem.rightBarButtonItems = [self.barButtonItem, self.barButtonItem2]
 self.navigationItem.leftBarButtonItems = [self.barButtonMenuBack]
 
 //SideBar
 if self.revealViewController() != nil
 {
 self.revealViewController().delegate = self
 self.menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
 self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
 self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
 }
 
 }
 else
 {
 self.title = "PROFILE"
 self.menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
 self.menuButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
 self.navigationItem.leftBarButtonItems = [self.barButtonMenuBack]
 self.navigationItem.rightBarButtonItems = [self.barButtonItem2]
 }
 }
 }
 }
 */


/*
 
 class ExampleCollectionView: UICollectionViewController {
 
 var popTip = SwiftPopTipView()
 
 override func viewDidLoad() {
 super.viewDidLoad()
 self.popTip.animation = .Slide
 self.popTip.popColor = UIColor.whiteColor()
 self.popTip.textColor = UIColor(netHex: 0x474747)
 self.popTip.dismissTapAnywhere = false
 let tapAnyWhere = UITapGestureRecognizer(target: self, action: "dismissPopTip")
 tapAnyWhere.cancelsTouchesInView = false
 self.view.addGestureRecognizer(tapAnyWhere)
 
 }
 
 func dismissPopTip() {
 if self.popTip.isShowing {
 self.popTip.dismissAnimated(true)
 }
 }
 
 override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
 println("did select cell \(indexPath.row)")
 let cell = collectionView.cellForItemAtIndexPath(indexPath)
 self.dismissPopTip()
 self.popTip.message = "Test poptip at index: \(indexPath.row)"
 self.popTip.presentAnimatedPointingAtView(cell!, inView: collectionView, autodismissAtTime: 1.5)
 }
 }
 
 */
class ColoredDatePicker: UIDatePicker {
    var changed = false
    override func addSubview(_ view: UIView) {
        if !changed {
            changed = true
            self.setValue(UIColor.black, forKey: "textColor")
        }
        super.addSubview(view)
    }
}
