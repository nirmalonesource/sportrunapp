//
//  TermsAndConditionViewController.swift
//  RunApp
//
//  Created by Mac on 30/04/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import WebKit

class TermsAndConditionViewController: UIViewController ,  WKUIDelegate {

  
    @IBOutlet weak var webViewContainer: UIView!
      var webView: WKWebView!
      var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

         let webConfiguration = WKWebViewConfiguration()
               let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: 0.0, height: self.webViewContainer.frame.size.height))
               self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
               webView.translatesAutoresizingMaskIntoConstraints = false
               self.webViewContainer.addSubview(webView)
               webView.topAnchor.constraint(equalTo: webViewContainer.topAnchor).isActive = true
               webView.rightAnchor.constraint(equalTo: webViewContainer.rightAnchor).isActive = true
               webView.leftAnchor.constraint(equalTo: webViewContainer.leftAnchor).isActive = true
               webView.bottomAnchor.constraint(equalTo: webViewContainer.bottomAnchor).isActive = true
               webView.heightAnchor.constraint(equalTo: webViewContainer.heightAnchor).isActive = true
               webView.uiDelegate = self
               
               //let myURL = URL(string: "http://pukaro.org/terms-and-conditions")
               let myURL =  Bundle.main.url(forResource: "RUN Mobile-App-Terms-and-Conditions-converted", withExtension: "pdf")
               let myRequest = URLRequest(url: myURL!)
               webView.load(myRequest)
        
        
       
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btn_back(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
