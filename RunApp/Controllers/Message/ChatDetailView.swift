//
//  ChatDetailView.swift
//  RunApp
///Volumes/Untitled/Run APP git/RunApp.xcodeproj
//  Created by My Mac on 06/12/19.
//  Copyright © 2019 My Mac. All rights reserved.
//


import UIKit
import Alamofire
import SDWebImage
import SocketIO
import NightNight

class sendercell: UITableViewCell {
    @IBOutlet var imgsender: UIImageView!
    @IBOutlet var lblsender: UILabel!
    @IBOutlet var lblsendertime: UILabel!
    
   
    
    override func awakeFromNib() {
           lblsendertime.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
       }
    
}



class receivercell: UITableViewCell {
    
    @IBOutlet var imgreceiver: UIImageView!
    @IBOutlet var lblreceiver: UILabel!
    @IBOutlet var lblreceivertime: UILabel!
    
    override func awakeFromNib() {
             lblreceivertime.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
         }
}

class GroupPlayercell: UITableViewCell {
    
    @IBOutlet var imgplayername: UIImageView!
    @IBOutlet var lblplayer: UILabel!
    @IBOutlet var lblsendertime: UILabel!
    
   
    
    override func awakeFromNib() {
        
            lblplayer.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
           
       }
    
}


class ChatDetailView: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tblplayerlist: UITableView!
    @IBOutlet var tblchat: UITableView!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var btn_sendmsg: UIButton!
    @IBOutlet var lbltyping: UILabel!
    @IBOutlet weak var txt_message: UITextField!
    @IBOutlet var img_bottomprofile: UIImageView!
    @IBOutlet var vwpopuptop: UIView!

    
    
    @IBOutlet weak var btn_groupmemebers: UIButton!
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
      
    }
    
    
    
    
    var SavedGroupPlayersListArr = [[String:Any]]()
    var GroupListArr = [[String:Any]]()
    
    var isGroupChat : String? = ""
    
    var dataDict : NSDictionary! = nil
    var chatBubblesArray1:NSArray = []
    var chatHistory:[[String:Any]] = []
    var MessageArray:[[String:Any]] = []
    var dic_DataDetail : [String: Any]?
    
    var getbookingid:String? = ""
    var getridename:String? = ""
    var getstatus:String? = ""
    
    
    var GroupId:String = ""
    var MemberId:String = ""
    
    
    var ReceiverID:String = ""
    var messagesArray : NSMutableArray = []
    var SenderID : String = ""
    var currentUserName : String = ""
    var currentUserimg : String = ""
    var ReciverType : String = ""
    var DATAS: String = ""
    var UserimgURL: String = ""
    
    var socket: SocketIOClient!
    var socketManager:SocketManager!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        vwpopuptop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        tblchat.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        txt_message.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
         tblplayerlist.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
        
               txt_message.setLeftPaddingPoints(30, strImage: "")
               txt_message.layer.masksToBounds = true
               txt_message.layer.cornerRadius = 20.0
               txt_message.setRadiusBorder(color: UIColor.red)
        
        
        
        if dic_DataDetail != nil
        {
            print("dic_DataDetail BB ========>>>>>> ",dic_DataDetail as Any)
            print("dic_DataDetail AA ========>>>>>> ",dic_DataDetail!["BookingStatus"] as Any)
        }
        else
        {
            
        }
        
        
        UserimgURL = UserDefaults.standard.string(forKey: "userprofileimg") ?? ""
        
        if isGroupChat == "true"
        {
            
            btn_groupmemebers.isHidden = false
            
            
            self.GroupId =  dic_DataDetail!["GroupID"] as? String ?? ""
            self.MemberId =  UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            
            
            
            
            
                   lblusername.text = dic_DataDetail!["GroupTitle"] as? String ?? ""
                   let imgURL = dic_DataDetail!["ImageURL"] as? String ?? ""
                          
                    img_Profile.sd_setImage(with: URL(string :(imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                    img_Profile.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    img_Profile.setRadius(radius: img_Profile.frame.height/2)
                    let imageView = RoundedImageView()
                    imageView.image = img_Profile.image
                   
            
                   img_bottomprofile.sd_setImage(with: URL(string : (UserimgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                   img_bottomprofile.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                   img_bottomprofile.setRadius(radius: img_bottomprofile.frame.height/2)
                 
                   
                   getGroupmessages()
                    
                   print(chatHistory)
                   
                  
                  
                  
                   img_Profile.layer.cornerRadius = img_Profile.frame.size.width/2
                   img_Profile.clipsToBounds = true
                   
                   //---------------- SOCKET CONFIG -------------------------
            
            socketManager = SocketManager(socketURL: URL(string: ServiceList.groupsocketURL)!, config: [.log(false), .compress])
                    socket = socketManager.defaultSocket
                  // socket = socketManager.socket(forNamespace: "swift")
                   socket.on(clientEvent: .connect) {
                    data, ack in
                       print("socket connected")
                    self.socket.emit("set-name", ["groupID":self.GroupId,"memberID":self.MemberId,"type":"App"])
                   }
                   
                  
                   socket.on("typing") {data, ack in
                       print("typing")
                       self.lbltyping.isHidden = false
                   }
                   socket.on("stopTyping") {data, ack in
                       print("stopTyping")
                       self.lbltyping.isHidden = true
                   }
                   
                   socket.on(clientEvent: .disconnect) {data, ack in
                       print("socket disconnect")
                   }
                  
                   socket.on("group chat message") {data, ack in
                       //guard let cur = data[0] as? Double else { return }
                    

                    print("DATA:",data)
                    
                        let dic:NSMutableDictionary = [:]
                        dic.setValue(self.MemberId, forKey: "MemberID")
                        dic.setValue(self.GroupId, forKey: "GroupID")
                        dic.setValue(data[0], forKey: "Message")
                                      
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let DateInFormat = dateFormatter.string(from: Date())
                dic.setValue(DateInFormat, forKey: "UpdatedAt")
                                      
                self.messagesArray.add(dic)
                                      
              self.tblchat.reloadData()
              self.scrollToBottom(animated: true)
                    
                    
                      
                   }
                   
                   socket.connect()
            
            
            
        }
      else
        {
        
        btn_groupmemebers.isHidden = true
       
            
        self.SenderID =  UserDefaults.standard.getUserDict()["id"] as? String ?? ""
        self.ReceiverID =  dic_DataDetail!["id"] as? String ?? ""
        lblusername.text = dic_DataDetail!["FirstName"] as? String ?? ""
        let imgURL = dic_DataDetail!["ImageURL"] as? String ?? ""
               
              img_Profile.sd_setImage(with: URL(string :(imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
              img_Profile.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
              img_Profile.setRadius(radius: img_Profile.frame.height/2)
              let imageView = RoundedImageView()
              imageView.image = img_Profile.image
        
        img_bottomprofile.sd_setImage(with: URL(string : (UserimgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
        img_bottomprofile.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        img_bottomprofile.setRadius(radius: img_bottomprofile.frame.height/2)
      
        
         getmessage()
        print(chatHistory)
        
       
       
       
        img_Profile.layer.cornerRadius = img_Profile.frame.size.width/2
        img_Profile.clipsToBounds = true
        
        //---------------- SOCKET CONFIG -------------------------
 
        socketManager = SocketManager(socketURL: URL(string: ServiceList.SocketUrl)!, config: [.log(false), .compress])
         socket = socketManager.defaultSocket
       // socket = socketManager.socket(forNamespace: "swift")
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            self.socket.emit("set-name", ["senderID": self.SenderID,"receiverID":self.ReceiverID,"type":"App"])
        }
        
       
        socket.on("typing") {data, ack in
            print("typing")
            self.lbltyping.isHidden = false
        }
        socket.on("stopTyping") {data, ack in
            print("stopTyping")
            self.lbltyping.isHidden = true
        }
        
        socket.on(clientEvent: .disconnect) {data, ack in
            print("socket disconnect")
        }
       
        socket.on("chat message") {data, ack in
            //guard let cur = data[0] as? Double else { return }
            print("DATA:",data)
            let dic:NSMutableDictionary = [:]
            dic.setValue(self.ReceiverID, forKey: "SenderId")
            dic.setValue(self.SenderID, forKey: "ReceiverId")
            dic.setValue(data[0], forKey: "Message")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let DateInFormat = dateFormatter.string(from: Date())
            dic.setValue(DateInFormat, forKey: "UpdatedAt")
            
            self.messagesArray.add(dic)
            
            self.tblchat.reloadData()
            self.scrollToBottom(animated: true)
        }
        
        socket.connect()
        
        }
        
       
            
        txt_message.addTarget(self, action: #selector(ChatDetailView.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        txt_message.addTarget(self, action: #selector(ChatDetailView.textFieldDidEnd(_:)), for: UIControl.Event.editingDidEnd)
        lbltyping.isHidden = true
            
            
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        
       if isGroupChat == "true"
       {
        let parameter = ["groupID": GroupId,
                         "memberID": MemberId ,
                         "msg": txt_message.text!.encodeEmoji,
                         ] as [String : Any]
        self.socket.emit("typing", parameter)
        
        }
        
       else
       {
        let parameter = ["senderID": self.SenderID,
                         "receiverID":self.ReceiverID ,
                         "msg": txt_message.text!.encodeEmoji,
                         "BookingID":self.getbookingid ?? "0"] as [String : Any]
        self.socket.emit("typing", parameter)
        
    }
    }
    
    @objc func textFieldDidEnd(_ textField: UITextField) {
       
        if isGroupChat == "true"
        {
            
            
         let parameter = ["groupID": GroupId,
                          "memberID": MemberId ,
                          "msg": txt_message.text!.encodeEmoji,
                          ] as [String : Any]
         self.socket.emit("typing", parameter)
         
         }
        else
        {
        let parameter = ["senderID": self.SenderID,
                         "receiverID":self.ReceiverID ,
                         "msg": txt_message.text!.encodeEmoji,
                         "BookingID":self.getbookingid ?? "0"] as [String : Any]
        self.socket.emit("stopTyping", parameter)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return false
        
    }
    
    @IBAction func btn_groupmemebers(_ sender: Any) {
        
        getGroupmembers()
        tblplayerlist.isHidden = false
        tblchat.isHidden = true
         
    }
    
    func getmessage()
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string:ServiceList.Getchathistory)
        print("Url printed \(String(describing: url))")
        print(chatHistory)
        
        
        let param:Parameters = [
            
            "senderID":SenderID,
            "receiverID":ReceiverID,
            
        ]
        print(param)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(url!, method:.post, parameters:param, headers: nil).responseJSON() { response in
            switch (response.result) {
            case .success:
                let jsonResponse = response.result.value as! NSDictionary
                AppDelegate.shared().HideHUD()
                print("JSON:",jsonResponse)
                let myInt: Bool = jsonResponse["status"]  as! Bool
                if(myInt == true)
                {
                    let datamsg : NSArray   = jsonResponse.value(forKeyPath: "messages") as! NSArray
                    
                    self.messagesArray =  (datamsg ).mutableCopy() as! NSMutableArray
                    
                    self.tblchat.reloadData()
                    self.scrollToBottom(animated: true)

                    AppDelegate.shared().HideHUD()
                }
                else
                {
                    AppDelegate.shared().HideHUD()
                }
                break
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    @IBAction func btn_closePlayerlist(_ sender: Any) {
        
        tblchat.isHidden = false
        tblplayerlist.isHidden = true
        
    }
    func getGroupmembers()
    {
       
        
        
        let parameters = [ "GroupID":GroupId
                              
        ]
        
          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                 ]
        
                   callApi(ServiceList.SERVICE_URL+ServiceList.GetGroupMemebers,
                           method: .post,
                           param: parameters ,
                           extraHeader: header ,
                           completionHandler: { (result) in
                               print(result)

                               if result.getBool(key: "status")
                               {
                                   
                                self.GroupListArr = result["data"] as? [[String:Any]] ?? []
                                self.SavedGroupPlayersListArr = result["data"] as? [[String:Any]] ?? []
                                
                                self.tblplayerlist.reloadData()
                                    
       //                            self.txtOldPasssword.text = ""
       //                            self.txtNewPassword.text = ""
       //                            self.txtConfirmPassword.text = ""
       //                            self.navigationController?.popViewController(animated: true)
                               }
                               showToast(uiview: self, msg: result.getString(key: "message"))
          
                   })
    }
    
    
    
    
    func getGroupmessages()
      {
          AppDelegate.shared().ShowHUD()
          let url = URL(string:ServiceList.GetGroupChatHistory)
          print("Url printed \(String(describing: url))")
          print(chatHistory)
          
          
          let param:Parameters = [
                                  "groupID":GroupId ,
                                  "senderID":MemberId ,
          ]
          print(param)
          let manager = Alamofire.SessionManager.default
          manager.session.configuration.timeoutIntervalForRequest = 120
          
          manager.request(url!, method:.post, parameters:param, headers: nil).responseJSON() { response in
              switch (response.result) {
              case .success:
                  let jsonResponse = response.result.value as! NSDictionary
                  AppDelegate.shared().HideHUD()
                  print("JSON:",jsonResponse)
                  let myInt: Bool = jsonResponse["status"]  as! Bool
                 
                  if(myInt == true)
                  {
                      let datamsg : NSArray   = jsonResponse.value(forKeyPath: "messages") as! NSArray
                      
                      self.messagesArray =  (datamsg).mutableCopy() as! NSMutableArray
                      
                      self.tblchat.reloadData()
                      self.scrollToBottom(animated: true)

                      AppDelegate.shared().HideHUD()
                  }
                  else
                  {
                      AppDelegate.shared().HideHUD()
                  }
                  
              break
              case .failure(let error):
                  AppDelegate.shared().HideHUD()
                  if error._code == NSURLErrorTimedOut {
                      
                    //HANDLE TIMEOUT HERE
                    
                  }
                  print("\n\nAuth request failed with error:\n \(error)")
                  break
              }
          }
      }
    
    @IBAction func btn_SENDMSG_A(_ sender: Any) {
        
      if isGroupChat == "true"
      {
       
            if txt_message.text != ""
            {
                let parameter = ["groupID": self.GroupId,
                                 "memberID":self.MemberId ,
                                 "msg": txt_message.text!.encodeEmoji,
                    ] as [String : Any]
                
                self.socket.emit("group chat message", parameter)
                
                let dic:NSMutableDictionary = [:]
                dic.setValue(Int(self.MemberId), forKey: "MemberID")
                
                dic.setValue(self.GroupId, forKey: "GroupID")
                dic.setValue(txt_message.text!.encodeEmoji, forKey: "Message")
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let DateInFormat = dateFormatter.string(from: Date())
                dic.setValue(DateInFormat, forKey: "MsgOn")
                
                self.messagesArray.add(dic)
                
                self.tblchat.reloadData()
                self.scrollToBottom(animated: true)
                txt_message.text = ""
            }
            else
            {
                AppDelegate.shared().ShowAlert(title: "", msg: "Please Enter Message")
            }
                
      }
        else
      {
        
        if txt_message.text != ""
        {
            let parameter = ["senderID": self.SenderID,
                             "receiverID":self.ReceiverID ,
                             "msg": txt_message.text!.encodeEmoji,
                ] as [String : Any]
            
            self.socket.emit("chat message", parameter)
            
            let dic:NSMutableDictionary = [:]
            dic.setValue(self.SenderID, forKey: "SenderId")
            dic.setValue(self.ReceiverID, forKey: "ReceiverId")
            dic.setValue(txt_message.text!.encodeEmoji, forKey: "Message")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let DateInFormat = dateFormatter.string(from: Date())
            dic.setValue(DateInFormat, forKey: "UpdatedAt")
            
            self.messagesArray.add(dic)
            
            self.tblchat.reloadData()
            self.scrollToBottom(animated: true)
            txt_message.text = ""
            
        }
        else
        {
            AppDelegate.shared().ShowAlert(title: "", msg: "Please Enter Message")
        }
        }
        
    }
   
    func scrollToBottom(animated: Bool) {
        let section = self.tblchat.numberOfSections
        let row = self.tblchat.numberOfRows(inSection: self.tblchat.numberOfSections - 1) - 1;
        guard (section > 0) && (row > 0) else{ // check bounds
            return
        }
        let indexPath = IndexPath(row: row-1, section: section-1)
        self.tblchat.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}

extension ChatDetailView : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblchat {
        return messagesArray.count
        }
        else
        {
            return GroupListArr.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == tblchat
        {
        
        if isGroupChat == "true"
        {
            let msgDic : NSDictionary = messagesArray.object(at: indexPath.row) as! NSDictionary
                   
                   let senderid1 = msgDic.value(forKey: "MemberID") as! Int
                   
                   let senderid = String(senderid1)
                   
            var msgText = msgDic.value(forKey: "Message") as? String ?? ""
            
         //   msgText = msgText.encodeEmoji
            
            
            
                    let UpdatedAt = msgDic.value(forKey: "MsgOn") as! String
                   
                   let inputFormatter = DateFormatter()
                   inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                   let showDate = inputFormatter.date(from: UpdatedAt)
                   inputFormatter.dateFormat = "yyyy-MM-dd hh:mm"
                   let resultString = inputFormatter.string(from: showDate!)
                   print(resultString)
                   
                   if senderid == self.MemberId
                   {
                       let cell:receivercell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! receivercell
                    
                    
                 
                    
                    cell.imgreceiver.sd_setImage(with: URL(string : (UserimgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                    cell.imgreceiver.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    cell.imgreceiver.setRadius(radius: cell.imgreceiver.frame.height/2)
                    let imageView = RoundedImageView()
                    imageView.image = cell.imgreceiver.image
                    
                    cell.lblreceiver.text = (msgText.decodeEmoji)
                      // cell.lblreceiver.text = msgText
                       cell.lblreceivertime.text = resultString
                       return cell
                       
                   } else {
                       let cell:sendercell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! sendercell
                         
                    let imgURL =  msgDic.value(forKey: "ImageURL") as? String ?? ""
                                              cell.imgsender.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                                              cell.imgsender.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                                              cell.imgsender.setRadius(radius: cell.imgsender.frame.height/2)
                                              let imageView = RoundedImageView()
                                              imageView.image = cell.imgsender.image
                    print(msgText)
                       cell.lblsender.text = (msgText.decodeEmoji)
                       cell.lblsendertime.text = resultString
                       return cell
                   }
        }
        
        
        
      else
        {
        
        
        let msgDic : NSDictionary = messagesArray.object(at: indexPath.row) as! NSDictionary
        
        let senderid = msgDic.value(forKey: "SenderId") as! String
        
        
        
        let msgText = msgDic.value(forKey: "Message") as! String
            
            
         let UpdatedAt = msgDic.value(forKey: "UpdatedAt") as! String
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let showDate = inputFormatter.date(from: UpdatedAt)
        inputFormatter.dateFormat = "yyyy-MM-dd hh:mm"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        
        if senderid == self.SenderID  {
            
            let cell:receivercell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! receivercell
                         cell.imgreceiver.sd_setImage(with: URL(string : (UserimgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                         cell.imgreceiver.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                         cell.imgreceiver.setRadius(radius: cell.imgreceiver.frame.height/2)
                         let imageView = RoundedImageView()
                         imageView.image = cell.imgreceiver.image
            cell.lblreceiver.text = msgText.decodeEmoji
            cell.lblreceivertime.text = resultString
            return cell
            
        } else
        {
          
            
            let cell:sendercell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! sendercell
              let imgURL = dic_DataDetail!["ImageURL"] as? String ?? ""
                                   cell.imgsender.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                                   cell.imgsender.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                                   cell.imgsender.setRadius(radius: cell.imgsender.frame.height/2)
                                   let imageView = RoundedImageView()
                                   imageView.image = cell.imgsender.image
            cell.lblsender.text = msgText.decodeEmoji
            cell.lblsendertime.text = resultString
            return cell
        }
        
    }
   //
        
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupPlayercell", for: indexPath) as! GroupPlayercell
                             
                              let playerData = GroupListArr[indexPath.row]
                              cell.lblplayer.text = playerData["FirstName"] as? String
                           //   cell.lblmsg.text = playerData["LastMessage"] as? String
                              let imgurl = playerData["ImageURL"] as? String ?? ""
                              cell.imgplayername.setRadius(radius: cell.imgplayername.frame.height/2)
                              cell.imgplayername.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options:.progressiveLoad, completed: nil)
                          
                         
                                    
                             // let imgURL = ServiceList.IMAGE_URL + imgurl
                              
                             // cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
                              
                              return cell
                      }
                      
            
        }
        
        
    
    
}


