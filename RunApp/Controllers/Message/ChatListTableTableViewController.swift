//
//  ChatListTableTableViewController.swift
//  RunApp
//
//  Created by My Mac on 19/12/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
//class ChatListTableTableViewController: UITableView,UISearchResultsUpdating {
    
//}
class ChatListTableTableViewController: UITableViewController,UISearchResultsUpdating {
    
   // let tableData = ["One","Two","Three","Twenty-One"]
     var ChatListArr = [[String:Any]]()
     var filteredTableData = [[String:Any]]()
   // var filteredTableData = [String]()
    var resultSearchController = UISearchController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            tableView.tableHeaderView = controller.searchBar

            return controller
        })()

        // Reload the table
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getchatlist()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
       // 1
       // return the number of sections
       return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      // 2
      // return the number of rows
      if  (resultSearchController.isActive) {
          return filteredTableData.count
      } else {
          return ChatListArr.count
      }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      // 3
        let cell:ChatListCell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell", for: indexPath) as! ChatListCell

      if (resultSearchController.isActive) {
           let playerData = filteredTableData[indexPath.row]
                                  cell.lblPlayerName.text = playerData["FirstName"] as? String
                                  cell.lblmsg.text = playerData["LastMessage"] as? String
                              let imgurl = playerData["ImageURL"] as? String ?? ""
                                  cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
                                  cell.imgUser.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)

          return cell
      }
      else {
          let playerData = ChatListArr[indexPath.row]
                         cell.lblPlayerName.text = playerData["FirstName"] as? String
                         cell.lblmsg.text = playerData["LastMessage"] as? String
                     let imgurl = playerData["ImageURL"] as? String ?? ""
                         cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
                         cell.imgUser.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
          return cell
      }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if (resultSearchController.isActive) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
                           nextViewController.dic_DataDetail = filteredTableData[indexPath.row]
               //                      nextViewController.currentUserName = lblUserName.text ?? ""
               //                      nextViewController.currentUserimg = playerDetailResult[0]["UserProfile"] as? String ?? ""
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                   //  print("Comment Clicked: \(buttonRow)")
            
        }
        else
         {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
                           nextViewController.dic_DataDetail = ChatListArr[indexPath.row]
               //                      nextViewController.currentUserName = lblUserName.text ?? ""
               //                      nextViewController.currentUserimg = playerDetailResult[0]["UserProfile"] as? String ?? ""
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                   //  print("Comment Clicked: \(buttonRow)")
        }
                
               
        }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)

        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (ChatListArr as NSArray).filtered(using: searchPredicate)
        filteredTableData = array as! [[String : Any]]

        self.tableView.reloadData()
    }
    
    func getchatlist()
    {
        let parameters = [ : ] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_CHATLIST,
                method: .get,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.ChatListArr = result["data"] as? [[String:Any]] ?? []
                        self.tableView.reloadData()
                    }
        })
        
    }
    
}
