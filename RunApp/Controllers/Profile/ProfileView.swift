//
//  ProfileView.swift
//  RunApp

//  Created by My Mac on 30/07/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SVProgressHUD
import Foundation
import MobileCoreServices
import Photos
import AVKit
import AVFoundation
import NightNight
import FirebaseAnalytics
import Firebase
import NetworkExtension
import FGRoute

class Name {
    let nameTitle: String
    let image: String
    var friendId: String = ""
    let homeTown: String
    let Score: String
    
    init(nameTitle: String, image: String, friendId: String = "", homeTown: String, Score: String) {
        self.nameTitle = nameTitle
        self.image = image
        self.friendId = friendId
        self.homeTown = homeTown
        self.Score = Score
    }
    
    var titleFirstLetter: String {
        return String(self.nameTitle[self.nameTitle.startIndex]).uppercased()
    }
}

class MainCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgOutlet: UIImageView!
    
}

class FootBollColletionCell: UICollectionViewCell {
    @IBOutlet weak var imgFootBollImage: UIImageView!
}

class CoinsCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgCoins: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var pagePoint: UIPageControl!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    
    override func awakeFromNib() {
         lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

class ProfileGalleryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnVideo: UIButton!
    
    
    override func awakeFromNib() {
        lblCount.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
       //  btnMessage.setMixedImage(MixedImage(normal:"chat", night:"chat-W"), forState: .normal)
    }
}

class indexingTableCell: UITableViewCell {
    @IBOutlet weak var BtnConfirmFriend: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLast: UILabel!
    @IBOutlet weak var imagUser: UIImageView!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnTopConfirm: NSLayoutConstraint!
    
    @IBOutlet weak var btnDeclineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnDeclineBotom: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLast.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

class historyTableCell: UITableViewCell {
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        lblHistory.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        //lblDate.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

struct Product {
    var product_name: String?
    var product_address: String?
    var product_image: String?
    
    init(product_name: String, product_address: String ,product_image: String) {
        self.product_name = product_name
        self.product_address = product_address
        self.product_image = product_image
    }
}

class ProfileView: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {

    var sponserUrl = String()
    var ipAddress = String()
    var sponserID = String()
    var reportID = String()
    
    var isFavorite : Bool = false
    fileprivate var currentVC: UIViewController?
    var videoURLArray = [URL]()
    
    
    @IBOutlet weak var btnreportuser: UIButton!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    @IBOutlet weak var imgSponserBaner: UIImageView!
    @IBOutlet weak var lblTotalPoint: UILabel!
    @IBOutlet weak var lblBadgeDescription: UILabel!
    @IBOutlet weak var lblSponserName: UILabel!
    @IBOutlet weak var lblBadgeTitle: UILabel!
    @IBOutlet weak var imgBadge: UIImageView!
    @IBOutlet weak var vwPopup2ndView: UIView!
    @IBOutlet weak var CollectionViewFootBoll: UICollectionView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblHomeTown: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var lblLOC: UILabel!
    @IBOutlet weak var lblBadge: UILabel!
    
    @IBOutlet weak var lbldistance: UILabel!
    
    
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var vwPopupDetailView: UIView!
    @IBOutlet weak var CollectionCoinsView: UICollectionView!
    
    @IBOutlet weak var collectionGallery: UICollectionView!
    var BoolIs = false
    var BoolFriendIs = false
    
    var arr_images = [UIImage]()
    var imagArray = [UIImage]()
    var imgURL = String()
    
    @IBOutlet weak var BtnCharBadges: UIButton!
    @IBOutlet weak var BtnSkillBadge: UIButton!
    @IBOutlet weak var BtnTeamBadges: UIButton!
    @IBOutlet weak var btnAddFriendOutlet: UIButton!
    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet weak var tableIndexingView: UITableView!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var btnNomination: UIButton!
    @IBOutlet weak var imgOnPopView: UIImageView!
    @IBOutlet weak var imgBlur: UIImageView!
    var strDistance : String?
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    
    var dictUser = [String : Any]()
    var selectedIndex = Int()
    var playerListDetail = [String : Any]()
    var friendGetList = [[String : Any]]()
    var friendRequestList = [[String : Any]]()
    
    var playerDetailResult = [[String:Any]]()
    var CoinsArray = [[String:Any]]()
    var HistoryArray = [[String:Any]]()
    
    var BadgeDesArray = [[String:Any]]()
    var ListCharecterArray = [[String:Any]]()
    
    @IBOutlet weak var btnSettingOutlet: UIButton!
    @IBOutlet weak var btnCartOutlet: UIButton!
    var upcoming = ""
    var id = ""
    var PhotoID = ""
    
    var GetLOCId : String?
    var BadgeID : String?
    
    var carsDictionary = [String: [String]]()
    var carSectionTitles = [String]()
    var cars = [String]()
    var levelOfCompositionList = [[String : Any]]()
    
    var names: [Name] = []
    var sortedFirstLetters: [String] = []
    var sections: [[Name]] = [[]]
    
    var getPhotoDict = [[String : Any]]()
    
    @IBOutlet weak var heightPostLbl: NSLayoutConstraint!
    
    @IBOutlet weak var btn2Width: NSLayoutConstraint!
    @IBOutlet weak var btn1Width: NSLayoutConstraint!
    
    @IBOutlet weak var charWidth: NSLayoutConstraint!
    
    @IBOutlet weak var btnLeadingOutlet: NSLayoutConstraint!
    
    @IBOutlet weak var btnCenterLeading: NSLayoutConstraint!
    @IBOutlet weak var btn2Trailing: NSLayoutConstraint!
    
    @IBOutlet weak var tblIndexingTop: NSLayoutConstraint!
    
    @IBOutlet weak var btn2Leading: NSLayoutConstraint!
    
    @IBOutlet weak var viewDistance: UIView!
    @IBOutlet weak var btnFirstSubOutlet: UIButton!
    @IBOutlet weak var btnSecondSubOutlet: UIButton!
    @IBOutlet weak var btnThirdSubOutlet: UIButton!
    @IBOutlet weak var btnFourthSubOutlet: UIButton!
    @IBOutlet weak var btnFifthSubOutlet: UIButton!
    @IBOutlet weak var btnSixSubOutlet: UIButton!
    
    @IBOutlet weak var btnSeventhOutlet: UIButton!
    
    
    @IBOutlet weak var ProgressiveOutlet: UIProgressView!
    @IBOutlet weak var lblChangeOutlet: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var imgLocationleading: NSLayoutConstraint!
    @IBOutlet weak var lbldistanceleading: NSLayoutConstraint!
    var GetDistance : String?
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var lblTotalNominate: UILabel!
    
    var CountLevel = 0
    var TotalPoints = 0
    
    @IBOutlet weak var imgFrequence: UIImageView!
    @IBOutlet weak var imgPropGiven: UIImageView!
    @IBOutlet weak var imgPropReceived: UIImageView!
    @IBOutlet weak var imgBadge_nominations: UIImageView!
    @IBOutlet weak var imgShares: UIImageView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    let picker = UIImagePickerController()
    
    var arr = [[String: [Int]]]()
    
    let GetId = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
    
    var isfromsearch = Bool()
    var UserType : String?
    var tappedButtonsTags = [Int]()

    @IBOutlet weak var profileSubImageBack: UIView!
    
    @IBOutlet var btnprofilelike: UIButton!
    
    
    @IBOutlet weak var btnReportuser: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        
        
    
        
        
        btnprofilelike?.contentMode = UIView.ContentMode.scaleToFill
        
        
       // imgBG.mixedImage = MixedImage(normal: UIImage(named: "imgAppBG1") ?? UIImage(), night: UIImage(named: "imgAppBG1-W") ?? UIImage())
        lblUserName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        btnSettingOutlet.setMixedImage(MixedImage(normal:"setting", night:"setting-W"), forState: .normal)
        lblHomeTown.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblSchool.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblPost.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLOC.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblHeight.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
       // btnCartOutlet.setMixedImage(MixedImage(normal:"shopping-cart", night:"shopping-cart-W"), forState: .normal)
        
        imgBadge.mixedImage = MixedImage(normal: UIImage(named: "setting") ?? UIImage(), night: UIImage(named: "setting-W") ?? UIImage())
        lblBadgeTitle.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblSponserName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblBadgeDescription.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        btn1.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
         btn2.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
         btn3.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
         btn4.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
         btn5.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
         btn6.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
        lblTotalPoint.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblTotalNominate.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
//         vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
//         vwPopupDetailView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
        vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
        vwPopupDetailView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
        
      //  self.view.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.nightfont)
        
       // vwPopup2ndView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
//         vwPopup2ndView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
//        tblHistory.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
        vwPopup2ndView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
        tblHistory.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
        
        
        if NightNight.theme == .night {
            btnCartOutlet.imageView?.changeImageViewImageColor(color: UIColor.white)
           
        } else {
             btnCartOutlet.imageView?.changeImageViewImageColor(color: UIColor.black)
          
        }
        
       
        picker.delegate = self
        makeViewCircular(view: btnFirstSubOutlet)
        makeViewCircular(view: btnSecondSubOutlet)
        makeViewCircular(view: btnThirdSubOutlet)
        makeViewCircular(view: btnFourthSubOutlet)
        makeViewCircular(view: btnFifthSubOutlet)
        makeViewCircular(view: btnSixSubOutlet)
         makeViewCircular(view:btnSeventhOutlet)
        self.view.layoutIfNeeded()
        
        vwPopupDetailView.setRadius(radius: 10)
        btnNomination.setRadius(radius: 10)
        btnDeleteOutlet.titleLabel?.font = .boldSystemFont(ofSize: 30)
        
        
        btnAddFriendOutlet.setRadius(radius: btnAddFriendOutlet.frame.height/2)
        
        carSectionTitles = [String](carsDictionary.keys)
        carSectionTitles = carSectionTitles.sorted(by: { $0 < $1 })
        
        if playerListDetail.count > 0 {
            
            id = playerListDetail["id"] as? String ?? ""
            reportID = id
            GetLOCId = playerListDetail["CLVLID"] as? String ?? ""
            print("id: \(id)")
            btnSettingOutlet.isHidden = true
            btnCartOutlet.isHidden = true
            btnAddFriendOutlet.isHidden = false
            btnprofilelike.isHidden = false
            btnreportuser.isHidden = false
            
            if id ==  UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            {
               
                btnAddFriendOutlet.isHidden = true
                btnprofilelike.isHidden = true
                btnSettingOutlet.isHidden = false
                btnreportuser.isHidden = true
                //btnCartOutlet.isHidden = false
                btnCartOutlet.isHidden = true
                btnreportuser.isHidden = true
            }
        }
            
        else if upcoming == "upcoming" {
            
            btnreportuser.isHidden = false
            btnSettingOutlet.isHidden = true
            btnCartOutlet.isHidden = true
            btnAddFriendOutlet.isHidden = false
            btnprofilelike.isHidden = false
            id = playerListDetail["id"] as? String ?? ""
            GetLOCId = playerListDetail["CLVLID"] as? String ?? ""
            print("id: \(id)")
        }
            
        else if id != "" {
            
            btnreportuser.isHidden = false
            btnSettingOutlet.isHidden = true
            btnCartOutlet.isHidden = true
            btnAddFriendOutlet.isHidden = false
            btnprofilelike.isHidden = false
            
        }
            
        else {
            
            id = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            print("id: \(id)")
            btnSettingOutlet.isHidden = false
             btnreportuser.isHidden = true
           // btnCartOutlet.isHidden = false
             btnCartOutlet.isHidden = true
            btnAddFriendOutlet.isHidden = true
            btnprofilelike.isHidden = true
            
        }
        
        //btnAddFriendOutlet.imageView?.changeImageViewImageColor(color: .green)
        vwPopupDetailView.isHidden = true
        tableIndexingView.isHidden = true
        tblHistory.isHidden = true
        CollectionViewFootBoll.isHidden = true
        CollectionCoinsView.isHidden = true
        self.view.layoutIfNeeded()
        //        imgBG.setGredient()
        vwPopupTop.setRadius(radius: 10)
        print("playerListDetail: \(playerListDetail)")
        
        APP_USER_PROFILE_DETAIL()
        
        
        UserType = UserDefaults.standard.getUserDict()["usertype"] as? String ?? ""
        if UserType == "Organization" {
            print("Organization Login")
        }
        else
        {
            print("Player Login")
        }
        
        arr_images = [#imageLiteral(resourceName: "img_info") ,#imageLiteral(resourceName: "img_gallery") , #imageLiteral(resourceName: "img_award") , #imageLiteral(resourceName: "img_user") , #imageLiteral(resourceName: "img_recent")]
        
        // getLOCList()
        
        myCollectionView.reloadData()
        
        if GetId == id {
            btnNomination.isHidden = true
            BtnCharBadges.isHidden = true
        }
            
        else {
            btnNomination.isHidden = false
        }
        
        let firstLetters = names.map { $0.titleFirstLetter }
        let uniqueFirstLetters = Array(Set(firstLetters))
        
        sortedFirstLetters = uniqueFirstLetters.sorted()
        sections = sortedFirstLetters.map { firstLetter in
            return names
                .filter { $0.titleFirstLetter == firstLetter }
                .sorted { $0.nameTitle < $1.nameTitle }
        }
        
        App_Crops()
        appGetPhoto()
        self.view.layoutIfNeeded()
        
//        self.roundingUIView(self.imgUser, cornerRadiusParam: 10)
//        self.roundingUIView(self.profileSubImageBack, cornerRadiusParam: 20)
        
        BtnTeamBadges.backgroundColor = UIColor.clear
        BtnSkillBadge.backgroundColor = UIColor.clear
        BtnCharBadges.backgroundColor = UIColor.clear
        
     //   AppFriendsGetList()
    }
    
    @IBAction func btn_reportuser(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportUserViewController") as! ReportUserViewController
        nextViewController.reportedId = reportID
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        myCollectionView.reloadData()
    }
    private func roundingUIView( aView: UIView!, cornerRadiusParam: CGFloat!) {
        aView.clipsToBounds = true
        aView.layer.cornerRadius = cornerRadiusParam
    }

        //MARK:- Web service
    
    func AppAddProfileLike()
    {
        let header = ["USER-ID":UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_LIKE,
                method: .post,
                param: ["OppositeID" : id] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    self.APP_USER_PROFILE_DETAIL()
        })
    }
        
        func AppFriendsGetList()
        {
            let header = ["USER-ID":UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_FRIEND_GET,
                    method: .post,
                    param: ["User_ID" : id] ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        
                        self.friendGetList.removeAll()
                        if result.getBool(key: "status")
                        {
                            self.friendGetList = result["data"] as? [[String:Any]] ?? []
                            
                            for i in 0..<self.friendGetList.count {
                                
                                let name = self.friendGetList[i]["FirstName"] as? String ?? ""
                                let homeTown  = self.friendGetList[i]["Hometown"] as? String ?? ""
                                let icon = self.friendGetList[i]["UserProfile"] as? String ?? ""
                                let friendsId = self.friendGetList[i]["Friends_ID"] as? String ?? ""
                                let Score = self.friendGetList[i]["Score"] as? String ?? ""
                                
                                self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown,Score: Score))
                            }
                            
                            let firstLetters = self.names.map { $0.titleFirstLetter }
                            let uniqueFirstLetters = Array(Set(firstLetters))
                            
                            self.sortedFirstLetters = uniqueFirstLetters.sorted()
                            self.sections = self.sortedFirstLetters.map { firstLetter in
                                return self.names
                                    .filter { $0.titleFirstLetter == firstLetter }
                                    .sorted { $0.nameTitle < $1.nameTitle }
                            }
                            
                            DispatchQueue.main.async {
                                self.tableIndexingView.reloadData()
                            }
                        }
            })
        }
    
        func APP_FRIENDS_REQUEST_GET()
        {
            let header = ["USER-ID": id /* UserDefaults.standard.getUserDict()["id"] as? String ?? "" */,
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_FRIENDS_REQUEST_GET,
                    method: .post,
                    param: ["User_ID" : id] ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                      //  self.friendGetList.removeAll()
                        self.names.removeAll()
                        self.sections.removeAll()
                        self.sortedFirstLetters.removeAll()
                        
                        if result.getBool(key: "status")
                        {
                            self.friendRequestList = result["data"] as? [[String:Any]] ?? []
                            
                            self.names.removeAll()
                            for i in 0..<self.friendRequestList.count {
                                
                                let name = self.friendRequestList[i]["FirstName"] as? String ?? ""
                                let homeTown  = self.friendRequestList[i]["Hometown"] as? String ?? ""
                                let icon = self.friendRequestList[i]["UserProfile"] as? String ?? ""
                                let friendsId = self.friendRequestList[i]["Friends_ID"] as? String ?? ""
                                let Score = self.friendRequestList[i]["Score"] as? String ?? ""
                                
                                self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown,Score: Score))
                               // self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown))
                            }
                            
                            let firstLetters = self.names.map { $0.titleFirstLetter }
                            let uniqueFirstLetters = Array(Set(firstLetters))
                            
                            self.sortedFirstLetters = uniqueFirstLetters.sorted()
                            self.sections = self.sortedFirstLetters.map { firstLetter in
                                return self.names
                                    .filter { $0.titleFirstLetter == firstLetter }
                                    .sorted { $0.nameTitle < $1.nameTitle }
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.tableIndexingView.reloadData()
                        }
            })
        }
        
        func APP_USER_PROFILE_DETAIL()
        {
            var getId = ""
            
            let userId = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            
            if userId == id {
                getId = "0"
                
            }
                
            else {
                getId = id
                
            }
            
            let parameters = ["FromID" : getId ,
                              "UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                ] as [String : Any]
            
      let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                  "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_USER_PROFILE ,
                    method: .post ,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        //print(result)
                        if result.getBool(key: "status")
                        {
                            self.playerDetailResult = result["data"] as? [[String:Any]] ?? []
                            print("playerDetailResult: \(self.playerDetailResult)")
                            self.setDataPlayerDetail()
                        }
            })
        }
        
        func setDataPlayerDetail()
        {
            GetLOCId = playerDetailResult[0]["CLVLTitle"] as? String ?? ""
            
            print("GetLOCId: \(GetLOCId ?? "")")
            lblLOC.text = "LOC: \(GetLOCId ?? "")"
            
            lblUserName.text = playerDetailResult[0]["username"] as? String
            lblHomeTown.text = "Home Town : \(playerDetailResult[0]["Hometown"] as? String ?? "")"
            //lblBadge.text = playerDetailResult[0]["Points"] as? String ?? ""
          //  lblScore.text = playerDetailResult[0]["Points"] as? String ?? ""
            lblScore.text = "\(playerDetailResult[0]["Score"] as? Int ?? 0)"
            lblBadge.text = "\(playerDetailResult[0]["Score"] as? Int ?? 0)"
            lblPoint.text = playerDetailResult[0]["Total_Points"] as? String ?? ""
            let school = "\(playerDetailResult[0]["School"] as? String ?? "")"
            
            let heightFt = playerDetailResult[0]["HeightFT"] as? String ?? ""
            let HeightInch = playerDetailResult[0]["HeightInch"] as? String ?? ""
            
            lblHeight.text = "Height: \(heightFt)′ \(HeightInch)″"
        
            //6′ 0″
        
            let status = playerDetailResult[0]["status"] as? Int
            
            if status == 0 {
                btnAddFriendOutlet.setTitle("Add Friend", for: .normal)
            }
                
            else if status == 1 {
                btnAddFriendOutlet.setTitle("Cancel request", for: .normal)
            }
                
            else {
                btnAddFriendOutlet.setTitle("Friend", for: .normal)
            }
            
            if school != "" {
                lblSchool.text = "School : \(playerDetailResult[0]["School"] as? String ?? "")"
            }
                
            else {
                lblSchool.isHidden = true
               // lblLOC.isHidden = true
            }
            
            let imgURL = ServiceList.IMAGE_URL + "\(playerDetailResult[0]["UserProfile"] as? String ?? "")"
        
             imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
             imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
             imgUser.setRadius(radius: imgUser.frame.height/2)
             let imageView = RoundedImageView()
             imageView.image = imgUser.image
            
            let  IsPlayerProfileLike = playerDetailResult[0]["IsPlayerProfileLike"] as! Int
            if IsPlayerProfileLike == 1 {
                btnprofilelike.setImage(UIImage(named: "filledhands"), for: .normal)
                btnprofilelike.isEnabled = false
            }
        }
        
        func getLOCList()
        {
            callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOC_LIST,
                    method: .get,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.levelOfCompositionList = result["data"] as? [[String:Any]] ?? []
                            
                            for i in 0..<self.levelOfCompositionList.count {
                                if self.GetLOCId == self.levelOfCompositionList[i]["CLVLID"] as? String ?? ""
                                {
                                    self.lblLOC.text = "Loc: \(self.levelOfCompositionList[i]["CLVLDesc"] as? String ?? "")"
                                    print("CLVLID: \(self.levelOfCompositionList[i]["CLVLDesc"] as? String ?? "")")
                                }
                            }
                        }
            })
        }
    
        func appGetPhoto()
        {
            let parameters = ["PlayerID" : id , "To_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_PHOTO,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        self.getPhotoDict.removeAll()
                        
                        if result.getBool(key: "status")
                        {
                            self.getPhotoDict = result["data"] as? [[String:Any]] ?? []
                            
                            print("Photo_ID: \(self.getPhotoDict)")
                            
                            DispatchQueue.main.async {
                              self.collectionGallery.reloadData()
                            }
                        }
                            
                        else {
                           // showToast(uiview: self, msg: result.getString(key: "message"))
                        }
            })
        }
        
        func addNomination(BadgeID: String)
        {
            let parameters = ["To_ID" : id ,
                              "Form_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                              "BadgeID" : BadgeID
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_NOMINATE_ADD,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            print("message: \(result.getString(key: "message"))")
                        }
                        
                        showToast(uiview: self, msg: result.getString(key: "message"))
            })
        }
        
        func addFriendApi()
        {
            let parameters = ["To_ID" : id ,
                              "Form_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.ADD_FRIENDS,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            let arrDict = result["data"] as? [String:Any] ?? [:]
                            print("Data: \(arrDict)")
                            self.btnAddFriendOutlet.setTitle("Cancel request", for: .normal)
                        }
                        showToast(uiview: self, msg: result.getString(key: "message"))
            })
        }
    
    func addCancelFriendApi()
    {
        let parameters = ["To_ID" : id ,
                          "Form_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_CANCLE_FRIEND,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let arrDict = result["data"] as? [String:Any] ?? [:]
                        print("Data: \(arrDict)")
                        self.btnAddFriendOutlet.setTitle("Add friend", for: .normal)
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func appBadgeDescription(BadgeID:String , PlayerID:String)
        {
           
            let parameters = ["BadgeID" : BadgeID ,
                              "PlayerID" : PlayerID
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_BADGE_DESCRIPTION,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            print("message: \(result.getString(key: "message"))")
                            
                                self.sponserUrl = ""
                            
                            let GetData:NSDictionary = result["data"] as! NSDictionary
                            
                            self.lblBadgeTitle.text = (((GetData.value(forKey: "badge") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "BadgeName") as? String ?? ""
                            
                            
                           // self.GetDistance = (((GetData.value(forKey: "distance") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "DefaultDistance") as? String ?? ""
                            
                            
                let getdistance = GetData.value(forKey: "totalnominate") as? Int ?? 150000
                            self.GetDistance = String(getdistance)
                            
                self.distanceShow(GetDistance: self.GetDistance ?? "")
                            
                let imgURL1:String = (((GetData.value(forKey: "badge") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "BadgeURL1") as? String ?? ""
                            let imgURL = ServiceList.IMAGE_URL + imgURL1
                            
                            self.imgBadge.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
                            
                            self.lblBadgeDescription.text = (((GetData.value(forKey: "badge") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "Description") as? String ?? ""
                            
                            let sponser = (GetData.value(forKey: "sponser") as! NSArray)
                            self.lblTotalPoint.text = "\(GetData.value(forKey: "totalnominate") ?? 0)"
                            self.lblTotalNominate.text =  "\(GetData.value(forKey: "LevelName") ?? 0000)"
                            
                            
                            if sponser.count > 0 {
                                self.lblSponserName.text = (sponser.object(at: 0) as AnyObject).value(forKey: "SponserTitle") as? String ?? ""
                                let imgURL2:String = (sponser.object(at: 0) as AnyObject).value(forKey: "SponserImage") as? String ?? ""
                                
                                self.sponserID = (sponser.object(at: 0) as AnyObject).value(forKey: "SponserID") as? String ?? "0"
                                
//                                let imgURL3 = ServiceList.IMAGE_URL + imgURL2
//                                self.imgSponserBaner.sd_setImage(with: URL(string : (imgURL3.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "no-image-box"), options: .progressiveLoad, completed: nil)
                                
                                self.sponserUrl = (sponser.object(at: 0) as AnyObject).value(forKey: "SponserURL") as? String ?? ""
                                print("sponserurl",self.sponserUrl)
                                
                                let imgURL3 = ServiceList.ADMIN_IMAGE_URL + imgURL2
                                self.imgSponserBaner.sd_setImage(with: URL(string : (imgURL3.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "no-image-box"), options: .progressiveLoad, completed: nil)
                                
                                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
                                self.imgSponserBaner.isUserInteractionEnabled = true
                                self.imgSponserBaner.addGestureRecognizer(tapGestureRecognizer)
                                
                            }
                            else
                            {
                               self.lblSponserName.text = ""
                            self.imgSponserBaner.sd_setImage(with: URL(string : ""), placeholderImage: #imageLiteral(resourceName: "no-image-box"), options: .progressiveLoad, completed: nil)
                            }
                            
                            // self.lblSponserName.text  = sponser.value(forKey: "SponserTitle") as? String ?? ""
                            
                            //                            self.lblSponserName.text = (((GetData.value(forKey: "sponser") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "SponserTitle") as? String ?? ""
                            
                            //                            let imgURL2:String = (((GetData.value(forKey: "sponser") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "SponserImage") as? String ?? ""
                            // let imgURL3 = ServiceList.IMAGE_URL + imgURL2
                            
                            // let imgURL3 = ServiceList.IMAGE_URL + imgURL2
                            
                            //                            self.imgSponserBaner.sd_setImage(with: URL(string : (imgURL3.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "no-image-box"), options: .progressiveLoad, completed: nil)
                            
                        //    self.lblTotalPoint.text = (((GetData.value(forKey: "totalpoints") as! NSArray).object(at: 0)) as AnyObject).value(forKey: "Total_Points") as? String ?? ""
                         
                                                        
                            
                        }
                        
                       // showToast(uiview: self, msg: result.getString(key: "message"))
            })
        }
        @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
        {

             guard let tracker = GAI.sharedInstance().defaultTracker else { return }

            tracker.send(GAIDictionaryBuilder.createEvent(withCategory: "ui_action", action: "button_press", label: "play", value: nil).build() as? [AnyHashable : Any])
             GAI.sharedInstance().dispatch()
            
           
            
//            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//            AnalyticsParameterItemID: "id-\(title!)",
//            AnalyticsParameterItemName: title!,
//            AnalyticsParameterContentType: "cont"
//            ])
            
    
            
            ipAddress = FGRoute.getIPAddress()!
            
            
            ClickCount()
            print(sponserID)
            
            if (sponserUrl != "") {
                       let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! webView
                nextViewController.urlString = sponserUrl
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                   }
            
//           if let url = URL(string:sponserUrl) {
//               UIApplication.shared.open(url)
//           }
            // Your action
        }
        func distanceShow(GetDistance:String) {
            
            let getDistance = Int(GetDistance)
            
            
//            if GetDistance == "0" {
//
//                ProgressiveOutlet.progress = 1/6*0
//
//                if btnFirstSubOutlet.backgroundColor == UIColor.white {
//                    makeColorChange(view: [btnFirstSubOutlet], color: UIColor.red)
//
//                    makeColorChange(view: [btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
//
//            }
//
//            else if GetDistance == "10" {
//
//                ProgressiveOutlet.progress = 1/6*1.2
//
//                if btnSecondSubOutlet.backgroundColor == UIColor.white {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet], color: UIColor.red)
//
//                    makeColorChange(view: [btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
//
//            }
//
//            else if GetDistance == "20" {
//                ProgressiveOutlet.progress = 1/6*2.4
//
//                if btnThirdSubOutlet.backgroundColor == UIColor.white {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet], color: UIColor.red)
//                    makeColorChange(view: [btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
//            }
//
//            else if GetDistance == "30" {
//                ProgressiveOutlet.progress = 1/6*3.5
//
//                if btnFourthSubOutlet.backgroundColor == UIColor.white {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet], color: UIColor.red)
//                    makeColorChange(view: [btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
//            }
//
//            else if GetDistance == "40" {
//                ProgressiveOutlet.progress = 1/6*4.7
//
//                if btnFifthSubOutlet.backgroundColor == UIColor.white {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet], color: UIColor.red)
//                    makeColorChange(view: [btnSixSubOutlet], color: UIColor.darkGray)
//                }
//            }
//
//            else {
//                ProgressiveOutlet.progress = 1/6*5.8
//
//                if btnSixSubOutlet.backgroundColor == UIColor.white {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.red)
//                }
//            }
//
//            UIView.animate(withDuration: 0.5, animations: {
//                if self.GetDistance == "0" {
//                    self.strDistance = "0"
//                    self.imgLocation.isHidden = true
//                    self.imgLocationleading.constant = self.btn1.frame.origin.x - 5
//                }
//                else if self.GetDistance == "10" {
//                    self.strDistance = "10"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "A")
//                    self.imgLocationleading.constant = self.btn2.frame.origin.x - 5
//                }
//                else if self.GetDistance == "20" {
//                    self.strDistance = "20"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "B")
//                    self.imgLocationleading.constant = self.btn3.frame.origin.x - 5
//                }
//                else if self.GetDistance == "30" {
//                    self.strDistance = "30"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "C")
//                    self.imgLocationleading.constant = self.btn4.frame.origin.x - 5
//                }
//                else if self.GetDistance == "40" {
//                    self.strDistance = "40"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "D")
//                    self.imgLocationleading.constant = self.btn5.frame.origin.x - 5
//                }
//                else {
//                    self.strDistance = "50"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "E")
//                    self.imgLocationleading.constant = self.btn6.frame.origin.x - 5
//                }
//            }, completion: nil)
            
            if getDistance! < 250 {
                           
                
                
                           ProgressiveOutlet.progress = 1/7*0
                           
                           if btnFirstSubOutlet.backgroundColor == UIColor.white
                           {
                            if getDistance == 50{
                               makeColorChange(view: [btnFirstSubOutlet], color: UIColor.red)
                            
                               makeColorChange(view: [btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet,btnSeventhOutlet], color: UIColor.darkGray)
                            }
                            
                           }
                           
                       }
                           
            else if getDistance! < 750 {
                           
                           ProgressiveOutlet.progress = 1/7*1.2
                           
                           if btnSecondSubOutlet.backgroundColor == UIColor.white {
                               
                            if getDistance! == 250
                            {
                               makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet], color: UIColor.red)
                               
                               makeColorChange(view: [btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet,btnSeventhOutlet], color: UIColor.darkGray)
                           }
                }
                           
                       }
                           
                       else if getDistance! < 3000
            
            {
                           ProgressiveOutlet.progress = 1/7*2.4
                           
                if getDistance! == 750 {
                
                           if btnThirdSubOutlet.backgroundColor == UIColor.white {
                               
                               makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet], color: UIColor.red)
                               makeColorChange(view: [btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet,btnSeventhOutlet], color: UIColor.darkGray)
                           }
                }
                      
            
            }
                           
            else if getDistance! < 12000 {
                           ProgressiveOutlet.progress = 1/7*3.5
                           
                   if getDistance! == 3000
                   {
                
                           if btnFourthSubOutlet.backgroundColor == UIColor.white {
                               
                               makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet], color: UIColor.red)
                               makeColorChange(view: [btnFifthSubOutlet,btnSixSubOutlet,btnSeventhOutlet], color: UIColor.darkGray)
                           }
                }
                       }
                           
                       else if getDistance! < 25000 {
                           ProgressiveOutlet.progress = 1/7*4.7
                           
                
                if getDistance! == 12000
                {
                
                           if btnFifthSubOutlet.backgroundColor == UIColor.white {
                               
                               makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet], color: UIColor.red)
                               makeColorChange(view: [btnSixSubOutlet,btnSeventhOutlet], color: UIColor.darkGray)
                           }
                }
                      
            }
                           
            else if getDistance! < 50000 {
                           ProgressiveOutlet.progress = 1/6*5.8
                           
                           if btnSixSubOutlet.backgroundColor == UIColor.white {
                            if getDistance! == 25000{
                            
                            
                               makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.red)
                                makeColorChange(view: [btnSeventhOutlet], color: UIColor.darkGray)
                           }
                }
                       }
            
            else{
               // ProgressiveOutlet.progress = 1/6*5.8
                
                if btnSixSubOutlet.backgroundColor == UIColor.white {
                 if getDistance! == 50000{
                 
                 
                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet,btnSeventhOutlet], color: UIColor.red)
            }
                }
            }
                       UIView.animate(withDuration: 0.5, animations: {
                        
                        if getDistance! == 50 {
                            self.strDistance = "0"
                            // self.imgLocation.isHidden = true
                            //  self.imgLocationleading.constant = self.btn1.frame.origin.x - 5
                            self.lbldistance.isHidden = false
                            self.lbldistance.text = "50"
                            self.lbldistanceleading.constant = self.btn1.frame.origin.x - 5
                           }
                        else if getDistance! == 250 {
                               self.strDistance = "10"
                               //self.imgLocation.isHidden = false
                              // self.imgLocation.image = UIImage(named: "2-1")
                             //  self.imgLocationleading.constant = self.btn2.frame.origin.x - 5
                            self.lbldistance.isHidden = false
                            self.lbldistance.text = "250"
                            self.lbldistanceleading.constant = self.btn2.frame.origin.x - 5
                           }
                        else if getDistance! == 750 {
                               self.strDistance = "20"
//                               self.imgLocation.isHidden = false
//                               self.imgLocation.image = UIImage(named:"4-1")
                              // self.imgLocationleading.constant = self.btn3.frame.origin.x - 5
                             self.lbldistance.isHidden = false
                            self.lbldistance.text = "750"
                            self.lbldistanceleading.constant = self.btn3.frame.origin.x - 5
                           }
                        else if getDistance! == 3000 {
                               self.strDistance = "30"
//                               self.imgLocation.isHidden = false
//                               self.imgLocation.image = UIImage(named:"6-1")
                              // self.imgLocationleading.constant = self.btn4.frame.origin.x - 5
                             self.lbldistance.isHidden = false
                            self.lbldistance.text = "3000"
                               self.lbldistanceleading.constant = self.btn4.frame.origin.x - 5
                            
                           }
                        else if getDistance! == 12000 {
                               self.strDistance = "40"
//                               self.imgLocation.isHidden = false
//                               self.imgLocation.image = UIImage(named:"8-1")
                               //self.imgLocationleading.constant = self.btn5.frame.origin.x - 5
                            self.lbldistance.isHidden = false
                            self.lbldistance.text = "12000"
                            self.lbldistanceleading.constant = self.btn5.frame.origin.x - 5
                            
                           }
                           else  if getDistance! == 25000
                        {
                               self.strDistance = "50"
//                               self.imgLocation.isHidden = false
//                               self.imgLocation.image = UIImage(named: "E")
                              // self.imgLocationleading.constant = self.btn6.frame.origin.x - 5
                            self.lbldistance.isHidden = false
                            self.lbldistance.text = "25000"
                            self.lbldistanceleading.constant = self.btn6.frame.origin.x - 5
                            
                           }
                        else if getDistance! == 12000 {
                            self.lbldistance.text = "50000"
                             self.lbldistance.isHidden = false
                            self.lbldistanceleading.constant = self.btn7.frame.origin.x - 5
                        }
                       }, completion: nil)
            
            
        }
        
        func App_Badgest_Skill()
        {
            let parameters = ["User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_BADGEST_SKILL,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.CoinsArray.removeAll()
                            self.CoinsArray = result["data"] as? [[String:Any]] ?? []
                            self.CollectionCoinsView.reloadData()
                        }
            })
        }
        
        func App_Crops()
        {
            let parameters = ["PlayerID" : id] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_PROPS,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        
//                        let Straight = "Straight"
//                        let Down = "Down"
//                        let Up = "Up"
                        
                        if result.getBool(key: "status")
                        {
                            let data:NSArray = result["data"] as! NSArray
                            
                            for i in 0..<data.count
                             {
                                let TrendingActiondesc:String = ((data.object(at: i)) as AnyObject).value(forKey: "TrendingActiondesc") as! String
                                let ArrowType:String = ((data.object(at: i)) as AnyObject).value(forKey: "ArrowType") as! String
                                
                                if TrendingActiondesc == "Frequency of play" {
                                    if ArrowType == "U"
                                    {
                                         self.imgFrequence.image = UIImage(named: "arro-up")
                                    }
                                    else if ArrowType == "S"
                                    {
                                        self.imgFrequence.image = UIImage(named: "arrow-right")
                                    }
                                    else
                                    {
                                        self.imgFrequence.image = UIImage(named: "arrow-down")
                                    }
                                }
                                    
                              else if TrendingActiondesc == "Props given"
                                {
                                    if ArrowType == "U"
                                    {
                                        self.imgPropGiven.image = UIImage(named: "arro-up")
                                    }
                                    else if ArrowType == "S"
                                    {
                                        self.imgPropGiven.image = UIImage(named: "arrow-right")
                                    }
                                    else
                                    {
                                        self.imgPropGiven.image = UIImage(named: "arrow-down")
                                    }
                                }
                                
                                else if TrendingActiondesc == "Props received"
                                {
                                        if ArrowType == "U"
                                        {
                                            self.imgPropReceived.image = UIImage(named: "arro-up")
                                        }
                                        else if ArrowType == "S"
                                        {
                                            self.imgPropReceived.image = UIImage(named: "arrow-right")
                                        }
                                        else
                                        {
                                            self.imgPropReceived.image = UIImage(named: "arrow-down")
                                        }
                                 }

                                
                                else if TrendingActiondesc == "Badge Nominations"
                                {
                                        if ArrowType == "U"
                                        {
                                            self.imgBadge_nominations.image = UIImage(named: "arro-up")
                                        }
                                        else if ArrowType == "S"
                                        {
                                            self.imgBadge_nominations.image = UIImage(named: "arrow-right")
                                        }
                                        else
                                        {
                                            self.imgBadge_nominations.image = UIImage(named: "arrow-down")
                                        }
                                 }

                                
                                else if TrendingActiondesc == "Profile Views"
                                {
                                            if ArrowType == "U"
                                            {
                                                self.imgProfile.image = UIImage(named: "arro-up")
                                            }
                                            else if ArrowType == "S"
                                            {
                                                self.imgProfile.image = UIImage(named: "arrow-right")
                                            }
                                            else
                                            {
                                                self.imgProfile.image = UIImage(named: "arrow-down")
                                            }
                                }

                                
                                else if TrendingActiondesc == "Shares"
                                {
                                            if ArrowType == "U"
                                            {
                                                self.imgShares.image = UIImage(named: "arro-up")
                                            }
                                            else if ArrowType == "S"
                                            {
                                                self.imgShares.image = UIImage(named: "arrow-right")
                                            }
                                            else
                                            {
                                                self.imgShares.image = UIImage(named: "arrow-down")
                                            }
                                 }

                             }
                            
//                            let data = result["data"] as? [String:Any] ?? [:]
//                            let Props_received = data["Props_received"] as? String ?? ""
//                            let Frequency_of_play = data["Frequency_of_play"] as? String ?? ""
//                            let Props_given = data["Props_given"] as? String ?? ""
//                            let Badge_nominations = data["Badge_nominations"] as? String ?? ""
//                            let Shares = data["Shares"] as? String ?? ""
//                            let Profile_views = data["Profile_views"] as? String ?? ""
                            
                           
                            
//                            if Straight == Badge_nominations {
//                                self.imgBadge_nominations.image = UIImage(named: "arrow-right")
//                            }
//
//                            else if Down == Badge_nominations {
//                                self.imgBadge_nominations.image = UIImage(named: "arrow-down")
//                            }
//
//                            else if Up == Badge_nominations {
//                                self.imgBadge_nominations.image = UIImage(named: "arro-up")
//                            }
//
//                            if Straight == Props_received {
//                                self.imgPropReceived.image = UIImage(named: "arrow-right")
//                            }
//
//                            else if Down == Props_received {
//                                self.imgPropReceived.image = UIImage(named: "arrow-down")
//                            }
//
//                            else if Up == Props_received {
//                                self.imgPropReceived.image = UIImage(named: "arro-up")
//
//                            }
//
//                            if Straight == Frequency_of_play {
//                                self.imgFrequence.image = UIImage(named: "arrow-right")
//                            }
//
//                            else if Down == Frequency_of_play {
//                                self.imgFrequence.image = UIImage(named: "arrow-down")
//                            }
//
//                            else if Up == Frequency_of_play {
//                                self.imgFrequence.image = UIImage(named: "arro-up")
//                            }
//                            //, arrow-down, arrow-right
//                            if Straight == Props_given {
//                                self.imgPropGiven.image = UIImage(named: "arrow-right")
//                            }
//
//                            else if Down == Props_given {
//                                self.imgPropGiven.image = UIImage(named: "arrow-down")
//                            }
//
//                            else if Up == Props_given {
//                                self.imgPropGiven.image = UIImage(named: "arro-up")
//                            }
//
//                            if Straight == Shares {
//                                self.imgShares.image = UIImage(named: "arrow-right")
//                            }
//
//                            else if Down == Shares {
//                                self.imgShares.image = UIImage(named: "arrow-down")
//                            }
//
//                            else if Up == Shares {
//                                self.imgShares.image = UIImage(named: "arro-up")
//                            }
//
//                            if Straight == Profile_views {
//                                self.imgProfile.image = UIImage(named: "arrow-right")
//                            }
//
//                            else if Down == Profile_views {
//                                self.imgProfile.image = UIImage(named: "arrow-down")
//                            }
//
//                            else if Up == Profile_views {
//                                self.imgProfile.image = UIImage(named: "arro-up")
//                            }
                        }
            })
        }
        
        func APP_BADGE_LIST_CHARECTER()
        {
            
            let parameters = ["User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any]
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_BADGE_LIST_CHARECTER,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.CoinsArray.removeAll()
                            self.CoinsArray = result["data"] as? [[String:Any]] ?? []
                            self.CollectionCoinsView.reloadData()
                        }
            })
        }
        

    
       func ClickCount()
       {
        let parameters = [
            "PlayerID":UserDefaults.standard.getUserDict()["id"] as? String ?? "","SponsorID":sponserID,"IPAddress":ipAddress,"AdType" : "S"
                        
            ] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.Sponser_click_count,
                method: .post,
                param: parameters,
                extraHeader: header,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        
                    }
        })
 

    }
    
    
        func APP_BADGE_LIST_TEAM()
        {
            let parameters = ["User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any]
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_BADGE_LIST_TEAM,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.CoinsArray.removeAll()
                            self.CoinsArray = result["data"] as? [[String:Any]] ?? []
                            self.CollectionCoinsView.reloadData()
                        }
            })
        }
        
        func APP_HISTORY()
        {
            let parameters = ["PlayerID" : id] as [String : Any]
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_HISTORY,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.HistoryArray.removeAll()
                            self.HistoryArray = result["data"] as? [[String:Any]] ?? []
                            self.tblHistory.reloadData()
                        }
            })
        }
        
        func APP_MY_FRIEND()
        {
            
            let parameters = ["User_ID" :id /* UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any  */]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_MY_FRIEND,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        
                        self.names.removeAll()
                        self.sections.removeAll()
                        self.sortedFirstLetters.removeAll()
                        
                        if result.getBool(key: "status")
                        {
                            self.friendGetList = result["data"] as? [[String:Any]] ?? []
                            
                            for i in 0..<self.friendGetList.count {
                                
                                let name = self.friendGetList[i]["FirstName"] as? String ?? ""
                                let homeTown  = self.friendGetList[i]["Hometown"] as? String ?? ""
                                let icon = self.friendGetList[i]["UserProfile"] as? String ?? ""
                                let friendsId = self.friendGetList[i]["Friends_ID"] as? String ?? ""
                                let Score = self.friendGetList[i]["Score"] as? String ?? ""
                                
                                self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown,Score: Score))
                              //  self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown))
                            }
                            
                            let firstLetters = self.names.map { $0.titleFirstLetter }
                            let uniqueFirstLetters = Array(Set(firstLetters))
                            
                            self.sortedFirstLetters = uniqueFirstLetters.sorted()
                            self.sections = self.sortedFirstLetters.map { firstLetter in
                                return self.names
                                    .filter { $0.titleFirstLetter == firstLetter }
                                    .sorted { $0.nameTitle < $1.nameTitle }
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.tableIndexingView.reloadData()
                        }
            })
        }
    
        func APP_UN_FRIENDS(Friends_ID: String)
        {
            let parameters = ["Friends_ID" : Friends_ID] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_UN_FRIENDS,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.APP_MY_FRIEND()
                            showToast(uiview: self, msg: result.getString(key: "message"))
                        }
            })
        }
    
        func APP_FRIENDS_ACTION(Friends_ID: String, Action: String)
        {
            let parameters = ["Friends_ID" : Friends_ID , "Action" : Action] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_FRIENDS_ACTION,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            self.APP_FRIENDS_REQUEST_GET()
                            showToast(uiview: self, msg: result.getString(key: "message"))
                        }
            })
        }
        
        func APP_FAVOURITE(To_ID: String, Form_ID: String, Photo_ID: String)
        {
            let parameters = ["To_ID" : To_ID , "Form_ID" : Form_ID, "Photo_ID" : Photo_ID] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_FAVOURITE,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            
                            self.appGetPhoto()
                            showToast(uiview: self, msg: result.getString(key: "message"))
                        }
            })
        }
    
    func APP_DELETE_FAVOURITE(To_ID: String, Form_ID: String, Photo_ID: String)
    {
        let parameters = ["To_ID" : To_ID , "Form_ID" : Form_ID, "Photo_ID" : Photo_ID] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_FAVOURITE_DELETE,
                method: .post,
                param: parameters,
                extraHeader: header,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.appGetPhoto()
                        showToast(uiview: self, msg: result.getString(key: "message"))
                    }
        })
    }

    //var myString = myURL.absoluteString
    
    //withLoader: Bool = true,
    
        func addPhotoApi(image: UIImage = UIImage(), video: NSURL = NSURL())
        {
            
            var imgdata = Data()
        
//            let parameters = ["PlayerID" : id ,
//                              "Type" : "image"
//                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
        
        
            if video.absoluteString == "" {
                let parameters = ["PlayerID" : id ,
                                  "Type" : "image"
                    ] as [String : Any]
                
                imgdata = image.jpegData(compressionQuality: 0.5)!
                
                self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PHOTO_ADD,
                             method: .post,
                             param: parameters ,
                             extraHeader: header ,
                             data: [imgdata] ,
                             dataKey: ["Gallery_Image"]) { (result) in
                                
                                print(result)
                                
                                if result.getBool(key: "status")
                                {
                                    self.appGetPhoto()
                                }
                                
                               showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
                
            else {
                
                let parameters = ["PlayerID" : id ,
                                  "Type" : "Video"
                    ] as [String : Any]
                
                self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PHOTO_ADD,
                             method: .post,
                             param: parameters ,
                             extraHeader: header ,
                             data1: [video as URL] ,
                             dataKey: ["Gallery_Image"]) { (result) in
                                
                                print(result)
                                
                                if result.getBool(key: "status")
                                {
                                    self.appGetPhoto()
                                }
                                
                              showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
            
        }
    
        //MARK:- Image Picker
    
        //    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //
        //        if let image = info[.originalImage] as? UIImage {
        //            self.addPhotoApi(image: image)
        //        }
        //
        ////        picker.sourceType = .savedPhotosAlbum
        ////        picker.delegate = self
        ////        picker.mediaTypes = ["public.movie"]
        ////        present(picker, animated: true, completion: nil)
        //
        //        self.dismiss(animated: true, completion: { () -> Void in
        //
        //        })
        //    }
    
        //MARK:- Image Picker
        
//        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//
//            if let image = info[.originalImage] as? UIImage {
//
//                let jpegData = image.jpegData(compressionQuality: 1.0)
//                let jpegSize: Int = jpegData?.count ?? 0
//                print("size of jpeg image in KB: %f ", Double(jpegSize) / 1024.0)
//
//                let kb = Double(jpegSize) / 1024.0
//
//                let fileSize = kb / 1048576
//
//                print("fileSize:\(fileSize)")
//                self.addPhotoApi(image: image)
//
//            }
//
//            else if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
//                do {
//                    let video = try Data(contentsOf: videoURL)
//                    print("File size after compression: \(Double(video.count / 1048576)) mb")
//
//                    let videoSize = Double(video.count / 1048576)
//
//                    if let videoThumbnail = getThumbnailFrom(path: videoURL){
//                        self.addPhotoApi(image: videoThumbnail)
//                    }
//                }
//
//                catch let error {
//                    print("*** Error generating thumbnail: \(error.localizedDescription)")
//                    return
//                }
//            }
//
//            picker.dismiss(animated: true, completion: nil)
//        }

    
        //MARK:- Thumbnail From path
        func getThumbnailFrom(path: URL) -> UIImage? {
            do {
                let asset = AVURLAsset(url: path , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                return thumbnail
                
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
                return nil
            }
        }
    
     //MARK:- Image Action
    func imageTapped(image:UIImage){
        let newImageView = UIImageView(image: image.aspectFittedToHeight(UIScreen.main.bounds.height))
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
      //  newImageView.enableZoom()
       // newImageView.contentMode = .scaleAspectFill
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileView.dismissFullscreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
//        self.navigationController?.isNavigationBarHidden = true
//        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
//        self.navigationController?.isNavigationBarHidden = false
//        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
   
    
        //MARK:- BUTTON ACTIONS
        
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
           
        }
        
        @IBAction func btnSettingAction(_ sender: UIButton) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingView") as! SettingView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }

        @IBAction func btnAddFriendAction(_ sender: UIButton) {
            if sender.currentTitle == "Cancel request" {
                addCancelFriendApi()
            }
                
            else {
                addFriendApi()
            }
        }
        
        @IBAction func btnDeletePressed(_ sender: UIButton) {
            hideViewWithAnimation(vw: vwPopupDetailView, img: imgBlur)
        }
        
        @objc func btnGalleryAction(button: UIButton) {
            // let buttonRow = button.tag
            openGalleryCameraPicker(picker: picker)
        }

        @objc func btnLikeAction(button: UIButton) {
            let buttonRow = button.tag
         //   tappedButtonsTags.append(button.tag)
                 
           // let total = getPhotoDict[button.tag]["total"] as? String ?? ""
            
             let PlayerID = getPhotoDict[button.tag]["PlayerID"] as? String ?? ""
            if PlayerID == UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
                showToast(uiview: self, msg: "You can not like own post")
            }
            else
            {
                let status = getPhotoDict[button.tag]["status"] as? String ?? ""
                    
                    if status == "0" {
                        PhotoID = getPhotoDict[button.tag]["PhotoID"] as? String ?? ""
                        APP_FAVOURITE(To_ID: UserDefaults.standard.getUserDict()["id"] as? String ?? "", Form_ID: id, Photo_ID: PhotoID)
                    }
                        
                    else {
                         PhotoID = getPhotoDict[button.tag]["PhotoID"] as? String ?? ""
                         APP_DELETE_FAVOURITE(To_ID: UserDefaults.standard.getUserDict()["id"] as? String ?? "", Form_ID:  id, Photo_ID: PhotoID)
                    }
                
                    print("Like Clicked: \(buttonRow)")
            }
           
        }
    
        @objc func btnVideoAction(button: UIButton) {
             button.isHidden = false
            let type = getPhotoDict[button.tag]["Type"] as? String ?? ""
            
            if type != "image" {
                var videoURL = getPhotoDict[button.tag]["Photo"] as? String ?? ""
                videoURL = ServiceList.IMAGE_URL + videoURL
                let url : URL = URL(string: videoURL)!
                let player = AVPlayer(url: url)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                    // playerViewController.view.semanticContentAttribute = .forceRightToLeft
                }
            }
           
        }
    
        @objc func btnCommentAction(button: UIButton) {
            let buttonRow = button.tag
            PhotoID = getPhotoDict[buttonRow]["PhotoID"] as? String ?? ""
            
            let myalert = UIAlertController(title: "", message: "Are you sure want to delete photo?", preferredStyle: UIAlertController.Style.alert)

            myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    print("Selected")
                let parameters = ["PhotoID" : self.PhotoID ,
                                         ] as [String : Any]
                                     
                                     let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                   "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                                   ] as [String : Any]
                                     
                self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PHOTO_DELETE,
                                             method: .post,
                                             param: parameters ,
                                             extraHeader: header ,
                                             completionHandler: { (result) in
                                                 print(result)
                                                 if result.getBool(key: "status")
                                                 {
                                                   self.appGetPhoto()
                                                 }
                                               else
                                                 {
                                                   
                                               }
                                                 showToast(uiview: self, msg: result.getString(key: "message"))
                                     })
                })
            myalert.addAction(UIAlertAction(title: "CANCEL", style: .cancel) { (action:UIAlertAction!) in
                    print("Cancel")
                })

                self.present(myalert, animated: true)
            
           
            
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
//            nextViewController.dic_DataDetail = getPhotoDict[buttonRow]
//            nextViewController.currentUserName = lblUserName.text ?? ""
//            nextViewController.currentUserimg = playerDetailResult[0]["UserProfile"] as? String ?? ""
//            self.navigationController?.pushViewController(nextViewController, animated: true)
//            print("Comment Clicked: \(buttonRow)")
        }
    
    /*
     self.btnLFavourite.imageView?.changeImageViewImageColor(color: UIColor.orange)
     self.isFavorite = true
     
     //
     if fav_media?.count ?? 0 > 0
     {
     self.isFavorite = true
     self.btnLFavourite.imageView?.changeImageViewImageColor(color: UIColor.orange)
     }
     else {
     self.isFavorite = false
     self.btnLFavourite.imageView?.changeImageViewImageColor(color: UIColor.gray)
     }
     
     //another code
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
     let cell = tableView.dequeueReusableCell(withIdentifier: "cell_identifier", for:
     indexPath) as! CellClass
     cell.button.tag  = indexPath.row
     cell.button.addTarget(self, action: #selector(playpause), for: .touchUpInside)
     }
     @objc func playpause(btn : UIButton){
     if btn.currentImage == UIImage(named: "Play") {
     btn.setImage(UIImage(named : "Pause"), forState: .Normal)
     }
     else {
     btn.setImage(UIImage(named : "Play"), forState: .Normal)
     }
     // perform your desired action of the button over here
     }
     
     */
    
    
        @IBAction func Btn1Clicked(_ sender: UIButton) {
            
            if sender.currentTitle == "Friends" {
                BoolFriendIs = true
                BtnTeamBadges.backgroundColor = UIColor.clear
                BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                BtnSkillBadge.backgroundColor = UIColor.red
                BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                APP_MY_FRIEND()
            }
                
            else {
                
                BoolIs = true
                BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                BtnTeamBadges.imageView?.changeImageViewImageColor(color: UIColor.darkGray)
                
                vwPopup2ndView.isHidden = true
                tableIndexingView.isHidden = true
                
                if collectionGallery.isHidden == false
                {
                    BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    CollectionCoinsView.isHidden = true
                    collectionGallery.reloadData()
                }
                    
                else
                {
                    BtnSkillBadge.backgroundColor = UIColor.red
                    BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                    BtnTeamBadges.backgroundColor = UIColor.clear
                    BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                    BtnCharBadges.backgroundColor = UIColor.clear
                    BtnCharBadges.setTitleColor(UIColor.red, for: .normal)
                    App_Badgest_Skill()
                    CollectionCoinsView.isHidden = false
                    collectionGallery.isHidden = true
                    CollectionCoinsView.reloadData()
                }
                
            } // End Of Else
            
        }
        
        @IBAction func btnCharAction(_ sender: UIButton) {
            
            if sender.hasImage(named: "plus1.png", for: .normal) || sender.hasImage(named: "plus-button", for: .normal) {
                print("YES")
                 self.showAttachmentActionSheet1(vc: self)
               // openImageVideoSelectionPicker(picker: picker)
                
            } else {
                print("No")
                sender.backgroundColor = UIColor.red
                sender.setTitleColor(UIColor.white, for: .normal)
                BtnSkillBadge.backgroundColor = UIColor.clear
                BtnSkillBadge.setTitleColor(UIColor.red, for: .normal)
                BtnTeamBadges.backgroundColor = UIColor.clear
                BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                APP_BADGE_LIST_CHARECTER()
            }
            
        }
        
        @IBAction func btn2Clicked(_ sender: UIButton) {
            
            if sender.currentTitle == "Friend Requests" {
                BoolFriendIs = false
                BtnSkillBadge.backgroundColor = UIColor.clear
                BtnSkillBadge.setTitleColor(UIColor.red, for: .normal)
                BtnTeamBadges.backgroundColor = UIColor.red
                BtnTeamBadges.setTitleColor(UIColor.white, for: .normal)
                print("Btn2 Clicked")
                APP_FRIENDS_REQUEST_GET()
            }
                
            else {
                
                BoolIs = false
                BtnTeamBadges.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.darkGray)
                
                vwPopup2ndView.isHidden = true
                tableIndexingView.isHidden = true
                
                if collectionGallery.isHidden == false
                {
                    BtnTeamBadges.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    CollectionCoinsView.isHidden = true
                    collectionGallery.reloadData()
                }
                    
                else
                {
                    BtnTeamBadges.backgroundColor = UIColor.red
                    BtnTeamBadges.setTitleColor(UIColor.white, for: .normal)
                    BtnSkillBadge.backgroundColor = UIColor.clear
                    BtnSkillBadge.setTitleColor(UIColor.red, for: .normal)
                    BtnCharBadges.backgroundColor = UIColor.clear
                    BtnCharBadges.setTitleColor(UIColor.red, for: .normal)
                    APP_BADGE_LIST_TEAM()
                    CollectionCoinsView.isHidden = false
                    collectionGallery.isHidden = true
                    CollectionCoinsView.reloadData()
                }
                
            } // End Of Else
            
        }
        
        @IBAction func btnChatAction(_ sender: UIButton) {
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OpenChatView") as! OpenChatView
//            nextViewController.LocationID = playerDetailResult[0]["LocationID"] as? String ?? ""
//       //already commented previously  // nextViewController.commentData = playerDetailResult[0]
//            self.navigationController?.pushViewController(nextViewController, animated: true)
        
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatListView") as! ChatListView
                   self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        @IBAction func btnAddLocationAction(_ sender: UIButton) {
        
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        @IBAction func btnNominationAction(_ sender: UIButton) {
            
//            let Coach = UserDefaults.standard.getUserDict()["Coach"] as? String ?? ""
//
//            if Coach == "1" {
//                addNomination(BadgeID: BadgeID ?? "")
//            }
//
//            else {
//                showAlert(uiview: self, msg: "You have no access to Give nominate, You Must be a Coach First..! ", isTwoButton: false)
//            }
            
             addNomination(BadgeID: BadgeID ?? "")
            
        }
        
        @IBAction func btnCartAction(_ sender: UIButton) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            nextViewController.totalPoint = lblTotalPoint.text ?? "0"
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        @IBAction func btnProgressClicked(_ sender: UIButton) {
            
            if sender.tag == 1 {
                
                self.lbldistance.isHidden = false
                self.lbldistance.text = "50"
                self.lbldistanceleading.constant = self.btn1.frame.origin.x - 5
                
//                ProgressiveOutlet.progress = 1/6*0
//
//                if btnFirstSubOutlet.backgroundColor == UIColor.darkGray {
//                    makeColorChange(view: [btnFirstSubOutlet], color: UIColor.red)
//                }
//
//                else {
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
            }
                
            else if sender.tag == 2 {
                
                self.lbldistance.isHidden = false
                self.lbldistance.text = "250"
                self.lbldistanceleading.constant = self.btn2.frame.origin.x - 5
                
//                ProgressiveOutlet.progress = 1/6*1.2
//
                
//                if btnSecondSubOutlet.backgroundColor == UIColor.darkGray {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet], color: UIColor.red)
                }
                    
//                else {
//                    makeColorChange(view: [btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
        //    }
                
            else if sender.tag == 3 {
//                ProgressiveOutlet.progress = 1/6*2.4
//
//                if btnThirdSubOutlet.backgroundColor == UIColor.darkGray {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet], color: UIColor.red)
//                }
//
//                else {
//
//                    makeColorChange(view: [btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
                self.lbldistance.isHidden = false
                self.lbldistance.text = "750"
                self.lbldistanceleading.constant = self.btn3.frame.origin.x - 5
            }
                
            else if sender.tag == 4 {
//                ProgressiveOutlet.progress = 1/6*3.5
//
//                if btnFourthSubOutlet.backgroundColor == UIColor.darkGray {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet], color: UIColor.red)
//                }
//                else {
//                    makeColorChange(view: [btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
                self.lbldistance.isHidden = false
                self.lbldistance.text = "3000"
                self.lbldistanceleading.constant = self.btn4.frame.origin.x - 5
            }
                
            else if sender.tag == 5 {
//                ProgressiveOutlet.progress = 1/6*4.7
//
//                if btnFifthSubOutlet.backgroundColor == UIColor.darkGray {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet], color: UIColor.red)
//                }
//
//                else {
//                    makeColorChange(view: [btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.darkGray)
//                }
                self.lbldistance.isHidden = false
                self.lbldistance.text = "12000"
                self.lbldistanceleading.constant = self.btn5.frame.origin.x - 5
            }
                
    else  if sender.tag == 6 {
//                ProgressiveOutlet.progress = 1/6*5.8
//
//                if btnSixSubOutlet.backgroundColor == UIColor.darkGray {
//
//                    makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.red)
//                }
//
//                else {
//                    makeColorChange(view: [btnSixSubOutlet], color: UIColor.darkGray)
//                }
     
                self.lbldistance.isHidden = false
                self.lbldistance.text = "25000"
                self.lbldistanceleading.constant = self.btn6.frame.origin.x - 5
    
    
            }
    else  if sender.tag == 7 {
//                   ProgressiveOutlet.progress = 1/6*5.8
//
//                   if btnSixSubOutlet.backgroundColor == UIColor.darkGray {
//
//                       makeColorChange(view: [btnFirstSubOutlet,btnSecondSubOutlet,btnThirdSubOutlet,btnFourthSubOutlet,btnFifthSubOutlet,btnSixSubOutlet], color: UIColor.red)
//                   }
//
//                   else {
//                       makeColorChange(view: [btnSixSubOutlet], color: UIColor.darkGray)
//                   }
                self.lbldistance.isHidden = false
                self.lbldistance.text = "50000"
                self.lbldistanceleading.constant = self.btn7.frame.origin.x - 5
    
               }
            
//            UIView.animate(withDuration: 0.5, animations: {
//                if sender.tag == 1 {
//                    self.strDistance = "0"
//                    self.imgLocation.isHidden = true
//                }
//                else if sender.tag == 2 {
//                    self.strDistance = "10"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "A")
//                }
//                else if sender.tag == 3 {
//                    self.strDistance = "20"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "B")
//                }
//                else if sender.tag == 4 {
//                    self.strDistance = "30"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "C")
//                }
//                else if sender.tag == 5 {
//                    self.strDistance = "40"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "D")
//                }
//                else {
//                    self.strDistance = "50"
//                    self.imgLocation.isHidden = false
//                    self.imgLocation.image = UIImage(named: "E")
//                }
//                self.imgLocation.frame.origin.x = sender.frame.origin.x - 5
//            }, completion: nil)
//
        }
    @IBAction func btnlike_click(_ sender: UIButton) {
        
        AppAddProfileLike()
    }
    
        //MARK:- ViewCircular
        
        func makeViewCircular(view: UIView) {
            view.layer.cornerRadius = view.bounds.size.width / 2.0
            view.clipsToBounds = true
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.white.cgColor
        }
        
        func makeColorChange(view: [UIButton],color: UIColor) {
            
            for btn in view {
                btn.layer.backgroundColor = color.cgColor
            }
        }
        
        //MARK:- COLLECTION METHODS
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if collectionView == myCollectionView {
                return arr_images.count
            }
                
            else if collectionView == CollectionViewFootBoll {
                return 6
            }
                
            else if collectionView == collectionGallery {
                if GetId == id {
                    return getPhotoDict.count
                }
                    
                else {
                    return getPhotoDict.count
                }
            }
                
            else {
                return CoinsArray.count
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            if collectionView == myCollectionView
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionCell", for: indexPath) as! MainCollectionCell
                cell.imgOutlet.image = arr_images[indexPath.row]
                if indexPath.row == selectedIndex
                {
                    cell.imgOutlet.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                }
                else
                {
                  //  cell.imgOutlet.changeImageViewImageColor(color: UIColor.black)
                   //  cell.imgOutlet.mixedTintColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                    if NightNight.theme == .night {
                        cell.imgOutlet.changeImageViewImageColor(color: UIColor.white)
                    } else {
                       cell.imgOutlet.changeImageViewImageColor(color: UIColor.black)
                    }
                }
                return cell
            }
                
            else if collectionView == CollectionViewFootBoll
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FootBollColletionCell", for: indexPath) as! FootBollColletionCell
                return cell
            }
                
            else if collectionView == collectionGallery
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileGalleryCollectionCell", for: indexPath) as! ProfileGalleryCollectionCell
                
                if BoolIs {
                     if GetId == id
                     {
                        cell.btnMessage.tag = indexPath.row
                        cell.btnMessage.addTarget(self, action: #selector(btnCommentAction), for: .touchUpInside)
                    }
                    else
                     {
                        
                    }
                    cell.imgGallery.isHidden = false
                    cell.btnLike.isHidden = false
                    cell.btnMessage.isHidden = false
                    cell.lblCount.isHidden = false
                
                    let imgURL = ServiceList.IMAGE_URL + "\(getPhotoDict[indexPath.row]["Photo"] as? String ?? "")"
                 
                    let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
                   
                    if type == "image" {
                        cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                    completed: nil)
                        cell.btnVideo.isHidden = true
                        
                    }
                    
                    else {
                        let url = URL(string: imgURL)
                        cell.btnVideo.isHidden = false
                        if let videoThumbnail = getThumbnailFrom(path: url!) {
                            cell.imgGallery.image = videoThumbnail
                        }
                    }
                
                   
                    let total = getPhotoDict[indexPath.row]["total"] as? String ?? ""
                    cell.lblCount.text = total
                    
                    
                    let status = getPhotoDict[indexPath.row]["status"] as? String ?? ""
                    
                    if status == "0" {
                    
                        let image = UIImage(named: "UnfilledHurt1")
    
                        cell.btnLike.setImage(image, for: .normal)
                        
                        PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                        cell.btnLike.tag = indexPath.row
                        cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                        cell.btnLike.isEnabled = true
                    }
                        
                    else {
                        
                         let image = UIImage(named: "filledHurt1")
                         cell.btnLike.setImage(image, for: .normal)
                         cell.lblCount.text = total
                         PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                        cell.btnLike.isEnabled = false

                    }
                   
                    cell.btnVideo.tag = indexPath.row
                    cell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                   
                }
                    
                else {
                    
                    if GetId == id {
                        
                        let imgURL = ServiceList.IMAGE_URL + "\(getPhotoDict[indexPath.row]["Photo"] as? String ?? "")"
                        
                        let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
                        
                        if type == "image" {
                            cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                        completed: nil)
                            cell.btnVideo.isHidden = true
                        }
                            
                        else {
                            let url = URL(string: imgURL)
                            cell.btnVideo.isHidden = false
                            if let videoThumbnail = getThumbnailFrom(path: url!) {
                                cell.imgGallery.image = videoThumbnail
                            }
                        }
                        
                        let status = getPhotoDict[indexPath.row]["status"] as? String ?? ""
                        let total = getPhotoDict[indexPath.row]["total"] as? String ?? ""
                        cell.lblCount.text = total
                        
                        if status == "0" {
                            
                            let image = UIImage(named: "UnfilledHurt1")
                            
                            cell.btnLike.setImage(image, for: .normal)
                            
                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                            
                            cell.btnLike.tag = indexPath.row
                            cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                            cell.btnLike.isEnabled = true
                        }
                            
                        else {
                            
                            let image = UIImage(named: "filledHurt1")
                            cell.btnLike.setImage(image, for: .normal)
                            cell.lblCount.text = total
                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                            cell.btnLike.isEnabled = false

                        }
                        
                        PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                        cell.lblCount.text = "(\(getPhotoDict[indexPath.row]["total"] as? String ?? ""))"
                        cell.btnVideo.tag = indexPath.row
                        
                        cell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                        cell.btnMessage.tag = indexPath.row
                        cell.btnMessage.addTarget(self, action: #selector(btnCommentAction), for: .touchUpInside)
                        
                    }
                        
                    else {
                        
                        
                        let imgURL = ServiceList.IMAGE_URL + "\(getPhotoDict[indexPath.row]["Photo"] as? String ?? "")"
//                        cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
//                                                    completed: nil)
                        
                        
                        let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
                        
                        if type == "image" {
                            cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                        completed: nil)
                            cell.btnVideo.isHidden = true
                            
                        }
                            
                        else {
                            let url = URL(string: imgURL)
                            cell.btnVideo.isHidden = false
                            if let videoThumbnail = getThumbnailFrom(path: url!) {
                                cell.imgGallery.image = videoThumbnail
                            }
                        }
                        
                        let total = getPhotoDict[indexPath.row]["total"] as? String ?? ""
                        cell.lblCount.text = total
                        
                        
                        let status = getPhotoDict[indexPath.row]["status"] as? String ?? ""
                        
                        if status == "0" {
                            
                            let image = UIImage(named: "UnfilledHurt1")
                            
                            cell.btnLike.setImage(image, for: .normal)
                            
                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                            
                            cell.btnLike.tag = indexPath.row
                            cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                            cell.btnLike.isEnabled = true
                        }
                            
                        else {
                            
                            let image = UIImage(named: "filledHurt1")
                            cell.btnLike.setImage(image, for: .normal)
                            cell.lblCount.text = total
                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                            
                            cell.btnLike.isEnabled = false
                        }
                        
                    }
                    
                }
                
                return cell
            }
                
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoinsCollectionCell", for: indexPath) as! CoinsCollectionCell
                cell.lblName.text = CoinsArray[indexPath.row]["BadgeName"] as? String
                CountLevel = CoinsArray[indexPath.row]["LevelNo"] as? Int ?? 0
                
                TotalPoints = CoinsArray[indexPath.row]["TotalPoints"] as? Int ?? 0 //TotalPoints
                
                if TotalPoints > 0 {
                    let imgURL = ServiceList.IMAGE_URL + "\(CoinsArray[indexPath.row]["BadgeURL1"] as? String ?? "")"
                    cell.imgCoins.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                              completed: nil)
                }
                    
                else {
                    let imgURL = ServiceList.IMAGE_URL + "\(CoinsArray[indexPath.row]["BadgeURL2"] as? String ?? "")"
                    cell.imgCoins.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                              completed: nil)
                }
                
                cell.lbl1.setRadius(radius: cell.lbl1.frame.height/2)
                cell.lbl2.setRadius(radius: cell.lbl2.frame.height/2)
                cell.lbl3.setRadius(radius: cell.lbl3.frame.height/2)
                cell.lbl4.setRadius(radius: cell.lbl4.frame.height/2)
                cell.lbl5.setRadius(radius: cell.lbl5.frame.height/2)
                
                if CountLevel == 0 {
                    cell.lbl1.backgroundColor = UIColor.clear
                    cell.lbl2.backgroundColor = UIColor.clear
                    cell.lbl3.backgroundColor = UIColor.clear
                    cell.lbl4.backgroundColor = UIColor.clear
                    cell.lbl5.backgroundColor = UIColor.clear
                    cell.lbl1.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl2.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl3.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl4.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl5.setRadiusBorder(color: .lightGray, weight: 1)
                }
                    
                else if CountLevel == 1 {
                    cell.lbl1.backgroundColor = UIColor.red
                    cell.lbl2.backgroundColor = UIColor.clear
                    cell.lbl3.backgroundColor = UIColor.clear
                    cell.lbl4.backgroundColor = UIColor.clear
                    cell.lbl5.backgroundColor = UIColor.clear
                    cell.lbl1.setRadiusBorder(color: .red, weight: 1)
                    cell.lbl2.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl3.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl4.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl5.setRadiusBorder(color: .lightGray, weight: 1)
                }
                    
                else if CountLevel == 2 {
                    cell.lbl1.backgroundColor = UIColor.red
                    cell.lbl2.backgroundColor = UIColor.red
                    cell.lbl3.backgroundColor = UIColor.clear
                    cell.lbl4.backgroundColor = UIColor.clear
                    cell.lbl5.backgroundColor = UIColor.clear
                    cell.lbl2.setRadiusBorder(color: .red, weight: 1)
                    cell.lbl3.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl4.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl5.setRadiusBorder(color: .lightGray, weight: 1)
                }
                    
                else if CountLevel == 3 {
                    cell.lbl1.backgroundColor = UIColor.red
                    cell.lbl2.backgroundColor = UIColor.red
                    cell.lbl3.backgroundColor = UIColor.red
                    cell.lbl4.backgroundColor = UIColor.clear
                    cell.lbl5.backgroundColor = UIColor.clear
                    cell.lbl3.setRadiusBorder(color: .red, weight: 1)
                    cell.lbl4.setRadiusBorder(color: .lightGray, weight: 1)
                    cell.lbl5.setRadiusBorder(color: .lightGray, weight: 1)
                }
                    
                else if CountLevel == 4 {
                    cell.lbl1.backgroundColor = UIColor.red
                    cell.lbl2.backgroundColor = UIColor.red
                    cell.lbl3.backgroundColor = UIColor.red
                    cell.lbl4.backgroundColor = UIColor.red
                    cell.lbl5.backgroundColor = UIColor.clear
                    cell.lbl4.setRadiusBorder(color: .red, weight: 1)
                    cell.lbl5.setRadiusBorder(color: .lightGray, weight: 1)
                }
                    
                else if CountLevel == 5 {
                    cell.lbl1.backgroundColor = UIColor.red
                    cell.lbl2.backgroundColor = UIColor.red
                    cell.lbl3.backgroundColor = UIColor.red
                    cell.lbl4.backgroundColor = UIColor.red
                    cell.lbl5.backgroundColor = UIColor.red
                    cell.lbl5.setRadiusBorder(color: .red, weight: 1)
                }
                
                return cell
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            if collectionView == myCollectionView {
                selectedIndex = indexPath.row
                collectionView.reloadData()
                
                if indexPath.row == 0 {
                    vwPopup2ndView.isHidden = false
                    tableIndexingView.isHidden = true
                    CollectionViewFootBoll.isHidden = true
                    collectionGallery.isHidden = true
                    CollectionCoinsView.isHidden = true
                    tblHistory.isHidden = true
                }
                    
                else if indexPath.row == 1 {
                    
                    if GetId == id {
                        BtnCharBadges.isHidden = false
                    }
                        
                    else {
                        BtnCharBadges.isHidden = true
                    }
                    
                    BtnTeamBadges.backgroundColor = UIColor.clear
                    BtnSkillBadge.backgroundColor = UIColor.clear
                    BtnCharBadges.backgroundColor = UIColor.clear
                    charWidth.constant = 30
                    //btnLeadingOutlet.constant = 25
                    BtnSkillBadge.setTitle("", for: .normal)
                    BtnCharBadges.setTitle("", for: .normal)
                    BtnTeamBadges.setTitle("", for: .normal)
                    
//                    BtnSkillBadge.setImage(UIImage(named: "img_menu_grid"), for: .normal)
//                    BtnCharBadges.setImage(UIImage(named: "plus1"), for: .normal)
//                    BtnTeamBadges.setImage(UIImage(named: "menu"), for: .normal)
                    
                    BtnSkillBadge.setMixedImage(MixedImage(normal:"img_menu_grid", night:"img_menu_grid-W"), forState: .normal)
                    BtnCharBadges.setMixedImage(MixedImage(normal:"plus1", night:"plus-button"), forState: .normal)
                    BtnTeamBadges.setMixedImage(MixedImage(normal:"menu", night:"menu-W"), forState: .normal)
                    
                    
                    //BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    
                    collectionGallery.isHidden = false
                    vwPopup2ndView.isHidden = true
                    tableIndexingView.isHidden = true
                    CollectionViewFootBoll.isHidden = true
                    CollectionCoinsView.isHidden = true
                    tblHistory.isHidden = true
                    collectionGallery.reloadData()
                }
                    
                    
                else if indexPath.row == 2 {
                    App_Badgest_Skill()
                    BtnCharBadges.isHidden = false
                    btn1Width.constant = 90
                    btnLeadingOutlet.constant = 0
                    btn2Width.constant = 90
                    btn2Trailing.constant = 0
                    charWidth.constant = 100
                    BtnSkillBadge.setImage(UIImage(), for: .normal)
                    BtnCharBadges.setImage(UIImage(), for: .normal)
                    BtnTeamBadges.setImage(UIImage(), for: .normal)
                    BtnSkillBadge.setTitle("Skill Badges", for: .normal)
                    BtnCharBadges.setTitle("Character Badges", for: .normal)
                    BtnCharBadges.setTitleColor(UIColor.red, for: .normal)
                    BtnCharBadges.backgroundColor = UIColor.clear
                    BtnSkillBadge.backgroundColor = UIColor.red
                    BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                    BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                    BtnTeamBadges.backgroundColor = UIColor.clear
                    BtnTeamBadges.setTitle("Team Badges", for: .normal)
                    BtnCharBadges.backgroundColor = UIColor.clear
                    BtnCharBadges.setTitleColor(UIColor.red, for: .normal)
                    BtnCharBadges.setRadius(radius: 5)
                    BtnTeamBadges.setRadius(radius: 5)
                    BtnSkillBadge.setRadius(radius: 5)
                    
                    vwPopup2ndView.isHidden = true
                    tableIndexingView.isHidden = true
                    CollectionViewFootBoll.isHidden = true
                    collectionGallery.isHidden = true
                    tblHistory.isHidden = true
                    CollectionCoinsView.isHidden = false
                    CollectionCoinsView.reloadData()
                }
                    
                else if indexPath.row == 3 {
                    BoolFriendIs = true
                    APP_MY_FRIEND()
                    
                    if GetId == id {
                        tblIndexingTop.constant = 65
                    }
                        
                    else {
                       // tblIndexingTop.constant = 0
                        tblIndexingTop.constant = 65
                    }
                    
                    BtnSkillBadge.isHidden = false
                    BtnTeamBadges.isHidden = false
                    BtnCharBadges.isHidden = true
                    btn1Width.constant = 90
                    
                    btnLeadingOutlet.constant = 0
                    btn2Width.constant = 120
                    btn2Trailing.constant = 0
                    BtnSkillBadge.setImage(UIImage(), for: .normal)
                    BtnTeamBadges.setImage(UIImage(), for: .normal)
                    BtnSkillBadge.setTitle("Friends", for: .normal)
                    
                    BtnSkillBadge.backgroundColor = UIColor.red
                    BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                    BtnTeamBadges.backgroundColor = UIColor.clear
                    BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                    BtnTeamBadges.setTitle("Friend Requests", for: .normal)
                    BtnTeamBadges.setRadius(radius: 5)
                    BtnSkillBadge.setRadius(radius: 5)
                    
                    vwPopup2ndView.isHidden = true
                    CollectionViewFootBoll.isHidden = true
                    CollectionCoinsView.isHidden = true
                    collectionGallery.isHidden = true
                    tblHistory.isHidden = true
                    tableIndexingView.isHidden = false
                    tableIndexingView.reloadData()
                }
                    
                else {
                    APP_HISTORY()
                    vwPopup2ndView.isHidden = true
                    CollectionViewFootBoll.isHidden = true
                    CollectionCoinsView.isHidden = true
                    collectionGallery.isHidden = true
                    tableIndexingView.isHidden = true
                    tblHistory.isHidden = false
                    tblHistory.reloadData()
                }
            }
                
            else if collectionView == collectionGallery {
            
//                let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
//
//                if type != "image" {
//                    var videoURL = getPhotoDict[indexPath.row]["Photo"] as? String ?? ""
//                    videoURL = ServiceList.IMAGE_URL + videoURL
//                    let url : URL = URL(string: videoURL)!
//                    let player = AVPlayer(url: url)
//                    let playerViewController = AVPlayerViewController()
//                    playerViewController.player = player
//                    self.present(playerViewController, animated: true) {
//                        playerViewController.player!.play()
//                        // playerViewController.view.semanticContentAttribute = .forceRightToLeft
//                    }
//                }
                
                 let cell = collectionGallery.cellForItem(at: indexPath) as! ProfileGalleryCollectionCell
                 self.imageTapped(image: cell.imgGallery.image!)
                
            }
                
            else if collectionView == CollectionCoinsView {
                
                BadgeID = CoinsArray[indexPath.row]["BadgeID"] as? String
                var PlayerID = CoinsArray[indexPath.row]["BadgeID"] as? String ?? ""
                            if isfromsearch
                           {
                                PlayerID = id
                           }
                           else
                           {
                                PlayerID = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                           }
                showViewWithAnimation(vw: vwPopupDetailView, img: imgBlur)
                appBadgeDescription(BadgeID: BadgeID ?? "", PlayerID: PlayerID)
            }
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        {
            if collectionView == myCollectionView {
                return  CGSize(width: collectionView.frame.width/5 - 10, height: 50)
            }
                
            else if collectionView == CollectionViewFootBoll {
                
                if BoolIs {
                    return  CGSize(width: collectionView.frame.width/3 - 10, height: 104)
                }
                    
                else {
                    
                    return  CGSize(width: collectionView.frame.width/1 - 10, height: collectionView.frame.width/1 - 10)
                }
            }
                
            else if collectionView == collectionGallery {
                if BoolIs {
                    
                    return  CGSize(width: collectionView.frame.width/3 - 10, height: collectionView.frame.width/3 + 30 )
                    
                }
                    
                else {
                    return  CGSize(width: collectionView.frame.width/1 - 10, height: collectionView.frame.width/1 - 10)
                }
                
            }
                
            else {
                
               // return  CGSize(width: collectionView.frame.width/3 - 10, height: 104)
                return  CGSize(width: collectionView.frame.width/3 - 10, height: 150)
            }
        }
    
    
    
    
    }
    
    extension ProfileView: UITableViewDataSource,UITableViewDelegate {
        
        func numberOfSections(in tableView: UITableView) -> Int {
            if tableView == tableIndexingView {
                return sections.count
            }
                
            else {
                return 1
            }
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if tableView == tableIndexingView {
                return sections[section].count
            }
                
            else {
                return HistoryArray.count
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if tableView == tableIndexingView {
                let cell = tableView.dequeueReusableCell(withIdentifier: "indexingTableCell", for: indexPath) as! indexingTableCell
                
                let name = sections[indexPath.section][indexPath.row]
                
                cell.lblName.text = name.nameTitle
              //  cell.lblLast.text = name.homeTown
                 cell.lblLast.text = name.Score
                
                imgURL = name.image
                
                imgURL = ServiceList.IMAGE_URL + imgURL
                cell.imagUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString())!), placeholderImage : nil , options: .progressiveLoad,completed: nil)
                
                cell.imagUser.setRadius(radius: cell.imagUser.frame.height/2)
                //            cell.BtnConfirmFriend.setRadius(radius: cell.BtnConfirmFriend.frame.height/2)
                //            cell.btnDecline.setRadius(radius: cell.btnDecline.frame.height/2)
                
                cell.BtnConfirmFriend.tag = (indexPath.section*100)+indexPath.row
                cell.btnDecline.tag = (indexPath.section*100)+indexPath.row
                
                cell.BtnConfirmFriend.addTarget(self, action: #selector(self.btnCellConfirmFriendAction(_:)), for: .touchUpInside)
                
                cell.btnDecline.addTarget(self, action: #selector(self.btnCellDeclineAction(_:)), for: .touchUpInside)
                
                if GetId != id {
                    cell.BtnConfirmFriend.isHidden = true
                    cell.btnDecline.isHidden = true
                }
                
               
                
                if BoolFriendIs {
                   
                    cell.BtnConfirmFriend.isHidden = false
                    cell.btnTopConfirm.constant = 20
                    cell.BtnConfirmFriend.backgroundColor = UIColor.red
                    cell.BtnConfirmFriend.setTitle("Unfriend", for: .normal)
                    
                    cell.btnDeclineBotom.constant = 0
                    cell.btnDecline.isHidden = true
                    
                }
                    
                else {
                    cell.BtnConfirmFriend.setTitle("Confirm", for: .normal)
                    cell.BtnConfirmFriend.backgroundColor = UIColor.green
                    cell.btnDeclineBotom.constant = 10
                    cell.BtnConfirmFriend.isHidden = false
                    cell.btnDecline.isHidden = false
                }
                
                if isfromsearch
                {
                    cell.BtnConfirmFriend.isHidden = true
                    
                }
                
                
                cell.BtnConfirmFriend.setRadius(radius: cell.BtnConfirmFriend.frame.height/2)
                cell.btnDecline.setRadius(radius: cell.btnDecline.frame.height/2)
                
                return cell
            }
                
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "historyTableCell", for: indexPath) as! historyTableCell
                cell.lblHistory.text = HistoryArray[indexPath.row]["Activity"] as? String ?? ""
                //cell.lblDate.text = HistoryArray[indexPath.row]["CreatedOn"] as? String ?? ""
//                cell.contentView.mixedBackgroundColor =  MixedColor(normal:FontColor.nightfont, night:0x252525)
                cell.contentView.mixedBackgroundColor =  MixedColor(normal:FontColor.nightfont, night:0x000000)
                cell.lblDate.text = "\(HistoryArray[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()
                return cell
            }
            
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              if tableView == tableIndexingView
              {
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                if BoolFriendIs {
                // cell.BtnConfirmFriend.isHidden = true
                // cell.btnDecline.isHidden = true
                  let playerData = friendGetList[indexPath.section]
                    print(playerData)
                    
                    let name = sections[indexPath.section][indexPath.row]
                    let didselectname = name.nameTitle

                    let friendReqlist : NSArray = friendGetList as! NSArray
                      
                    print(friendGetList)
                        var index = 0
                        for i in 0 ..< friendReqlist.count
                        {

                        let dict:NSDictionary = friendReqlist.object(at:i) as! NSDictionary
                        print(dict.value(forKey: "FirstName") as! String)
                            
                            if ( name.nameTitle == dict.value(forKey: "FirstName") as! String)
                                                      {
                                                    
                                        index = i
                                        let selectedarr:NSDictionary = self.friendGetList[index] as! NSDictionary
                                            print(selectedarr)
                                                 
                            }
             
                                                    
                            
                                                
                                              }

                    let usertype = friendGetList[index]["usertype"] as? String
                    
                    if usertype == "Organization"
                    {
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                        nextViewController.playerOrglResult = friendGetList[index]
                           nextViewController.isfromsearch = true;
                       self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                        
                    }
                    else
                    {
                        
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                           nextViewController.playerListDetail = friendGetList[index]
                        nextViewController.isfromsearch = true;
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                    
                            
                                       
                    
                    
                 // nextViewController.playerListDetail = friendGetList[indexPath.section]
                }
                else
                {
                    
                    
                  let playerData = friendRequestList[indexPath.section]
                let name = sections[indexPath.section][indexPath.row]
               let didselectname = name.nameTitle

                    let friendReqlist : NSArray = friendRequestList as! NSArray

                    print(friendRequestList)
                    var index = 0
                               for i in 0 ..< friendReqlist.count {

                let dict:NSDictionary = friendReqlist.object(at:i) as! NSDictionary
                     print(dict.value(forKey: "FirstName") as! String)
                    if ( name.nameTitle == dict.value(forKey: "FirstName") as! String)
                                   {
                                       index = i
                                    let selectedarr:NSDictionary = self.friendRequestList[index] as! NSDictionary
                                    print(selectedarr)
                                                                                    
                                   }

                             
                             
                            
                             
                           }
           
                    let usertype = friendRequestList[index]["usertype"] as? String
                                      
                                      if usertype == "Organization"
                                      {
                                          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                          nextViewController.playerOrglResult = friendRequestList[index]
                                             nextViewController.isfromsearch = true;
                                         self.navigationController?.pushViewController(nextViewController, animated: true)
                                          
                                          
                                      }
                                      else
                                      {
                                          
                                          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                             nextViewController.playerListDetail = friendRequestList[index]
                                          nextViewController.isfromsearch = true;
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                      }
                                      
                                              
                                                         
                                      
                    
                    
                    
//         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
//            nextViewController.playerListDetail = friendRequestList[index]
//            nextViewController.isfromsearch = true;
//           self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                }
               
                
            }
           
        }
        
        func sectionIndexTitles(for tableView: UITableView) -> [String]? {
            
            if tableView == tableIndexingView {
                return sortedFirstLetters
            }
                
            else {
                return []
            }
        }
        /*
         let name = sections[indexPath.section][indexPath.row]
         
         cell.lblName.text = name.nameTitle
         cell.lblLast.text = name.homeTown
         let friendid = name.friendId
         */
        
        //MARK:- Button Cell Action
        
        @objc func btnCellConfirmFriendAction(_ sender : UIButton)
        {
            let section = sender.tag / 100
            let row = sender.tag % 100
            let indexPath = NSIndexPath(row: row, section: section)
            
            let name = sections[indexPath.section][indexPath.row]
            
            if sender.currentTitle == "Confirm" {
                self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "APPROVE")
            }
                
            else {
                self.APP_UN_FRIENDS(Friends_ID: name.friendId)
            }
            
            //        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Do you want to buy this product?")
            //
            //        let action1 = EMAlertAction(title: "Yes", style: .cancel)
            //        {
            //            self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "APPROVE")
            //        }
            //
            //        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            //
            //        }
            //
            //        alert.addAction(action: action1)
            //        alert.addAction(action: action2)
            //
            //        self.present(alert, animated: true, completion: nil)
        }
        
        @objc func btnCellDeclineAction(_ sender : UIButton)
        {
            let section = sender.tag / 100
            let row = sender.tag % 100
            let indexPath = NSIndexPath(row: row, section: section)
            
            let name = sections[indexPath.section][indexPath.row]
            self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "REJECT")
            
            //        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Do you want to buy this product?")
            //
            //        let action1 = EMAlertAction(title: "Yes", style: .cancel)
            //        {
            //            self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "REJECT")
            //        }
            //
            //        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            //        }
            //
            //        alert.addAction(action: action1)
            //        alert.addAction(action: action2)
            //
            //        self.present(alert, animated: true, completion: nil)
        }
    }
    
    extension UIButton {
        func hasImage(named imageName: String, for state: UIControl.State) -> Bool {
            guard let buttonImage = image(for: state), let namedImage = UIImage(named: imageName) else {
                return false
            }
            
            return buttonImage.pngData() == namedImage.pngData()
        }
    }

extension ProfileView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   // fileprivate var currentVC: UIViewController?
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController,
                                     didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print("image picked")
             self.addPhotoApi(image: image)
            //self.imagePickedBlock?(image)
            
        } else{
            print("Something went wrong in  image")
        }
        
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL{
            print("videourl: ", videoUrl)
            //trying compression of video
            let data = NSData(contentsOf: videoUrl as URL)!
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            compressWithSessionStatusFunc(videoUrl)
        }
        else{
            print("Something went wrong in  video")
        }
        
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                   // self.videoPickedBlock?(compressedURL as NSURL)
                    self.addPhotoApi(video:compressedURL as NSURL)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, video, photo and file.
    func showAttachmentActionSheet1(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: AttachmentHandler.Constants.actionFileTypeHeading, message: AttachmentHandler.Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
            
        }))
        
//        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.file, style: .default, handler: { (action) -> Void in
//            self.documentPicker()
//        }))
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone and
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
       
            DispatchQueue.main.async { // Correct
                 if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
              let myPickerController = UIImagePickerController()
               myPickerController.delegate = self
               myPickerController.sourceType = .photoLibrary
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType, vc: UIViewController) {
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentHandler.AttachmentType.camera {
                openCamera()
            }
            if attachmentTypeEnum == AttachmentHandler.AttachmentType.photoLibrary {
                photoLibrary()
            }
            
            if attachmentTypeEnum == AttachmentHandler.AttachmentType.video {
                videoLibrary()
            }
            
        case .denied:
            print("permission denied")
            //self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentHandler.AttachmentType.camera {
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentHandler.AttachmentType.photoLibrary {
                        self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentHandler.AttachmentType.video {
                        self.videoLibrary()
                    }
                } else{
                    print("restriced manually")
                    //self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            //self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
}
