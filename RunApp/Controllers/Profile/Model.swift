//
//  Model.swift
//  tableDemoWithSearchBar
//
//  Created by My Mac on 22/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class RoundedImageView: UIView {
    var image: UIImage? {
        didSet {
            if let image = image {
                self.frame = CGRect(x: 0, y: 0, width: image.size.width/image.scale, height: image.size.width/image.scale)
            }
        }
    }
    var cornerRadius: CGFloat?
    
    private class func frameForImage(image: UIImage) -> CGRect {
        return CGRect(x: 0, y: 0, width: image.size.width/image.scale, height: image.size.width/image.scale)
    }
    
    override func draw(_ rect: CGRect) {
        if let image = self.image {
            image.draw(in: rect)
            
            let cornerRadius = self.cornerRadius ?? rect.size.width/10
            let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
            UIColor.white.setStroke()
            path.lineWidth = cornerRadius
            path.stroke()
        }
    }
}

class Model: NSObject {
    var imageName:String = ""
    var imageYear:String = ""
    var imageBy:String = ""
    init(name:String, year:String,by:String) {
        self.imageName = name
        self.imageYear = year
        self.imageBy = by
    }
    
    class func generateModelArray() -> [Model] {
        var modelAry = [Model]()
        modelAry.append(Model(name: "A", year: "2000", by: "sdj"))
        modelAry.append(Model(name: "B", year: "2001", by: "sdsd"))
        modelAry.append(Model(name: "C", year: "2005", by: "sdsd"))
        modelAry.append(Model(name: "D", year: "2007", by: "jj"))
        modelAry.append(Model(name: "E", year: "2009", by: "fhfg"))
        return modelAry
    }
}
