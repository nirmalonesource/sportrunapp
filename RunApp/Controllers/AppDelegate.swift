//
//  AppDelegate.swift
//  RunApp
//
//  Created by My Mac on 05/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
//import FacebookLogin
import GooglePlaces
import Firebase
import Stripe
import OneSignal
import Crashlytics
import Fabric
import PKHUD
import NightNight
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , OSPermissionObserver , OSSubscriptionObserver {
    
    var window: UIWindow?
//    override init() {
//        // Firebase Init
//        FirebaseApp.configure()
//    }
    
    //id = "ch_1F2I9hHjBlUiD0xaKO9QJDxI";
    
    //shared
    class func shared() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
        
        
        Fabric.with([Crashlytics.self])
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "dc45cffc-9a73-4170-ac07-7a1c8856dcba",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
   
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        StoredData.shared.playerId = userId
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            
//            if #available(iOS 10.0, *) {
//                self.checkNotificationsAuthorizationStatus()
//            } else {
//                // Fallback on earlier versions
//            }
        
            
        })
        
        OneSignal.add(self as OSPermissionObserver)
      
        OneSignal.add(self as OSSubscriptionObserver)
        
//        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
//        print("userId : \(String(describing: userId))")
        
         //Above code Push Notification
        
        //GMSPlacesClient.provideAPIKey("AIzaSyAs3tnkk-_qprnZEuC_vFJqEspgrkKxJps")
        GMSPlacesClient.provideAPIKey("AIzaSyDO1hi6nmBAo9Ai8wcsU16lm26DiOglmYo")
        //Stripe
       
        STPPaymentConfiguration.shared().publishableKey = ServiceList.PUBLISH_KEY
        
        STPPaymentConfiguration.shared().appleMerchantIdentifier = ConstantVariables.Constants.KEY_TEST
        
        IQKeyboardManager.shared.enable = true
       // FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        guard let gai = GAI.sharedInstance() else {

                  assert(false, "Google Analytics not configured correctly")
                  return true
              }

              gai.tracker(withTrackingId: "UA-126391178-1")
              // Optional: automatically report uncaught exceptions.
              gai.trackUncaughtExceptions = true

              // Optional: set Logger to VERBOSE for debug information.
              // Remove before app release.
              gai.logger.logLevel = .verbose;
        gai.dispatchInterval = 20
        
//        if let gai = GAI.sharedInstance(),
//            let gaConfigValues = Bundle.main.infoDictionary?["GoogleAnalytics"] as? [String: String],
//            let trackingId = gaConfigValues["UA-126391178-1"]
//        {
//            gai.logger.logLevel = .verbose
//            gai.trackUncaughtExceptions = false
//            gai.tracker(withTrackingId: trackingId)
//        } else {
//            assertionFailure("Google Analytics not configured correctly")
//        }
        
        FirebaseApp.configure()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = UIColor.black
//        }
        
       UIApplication.shared.statusBarStyle = .lightContent
    
//       if UserDefaults.standard.getIsLogin()
//              {
//                  let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
//                  let controllerHome = mainStoryboard.instantiateViewController(withIdentifier: "HomeView") as? HomeView
//                  let navController = UINavigationController.init(rootViewController: controllerHome!)
//                  window?.rootViewController = navController
//              }
//              else
//              {
//                    NightNight.theme = .night
//                   UserDefaults.standard.set(true, forKey: "on")
//                   
//              }
    
      
        
    
        return true
        
    }
    
    @available(iOS 10.0, *)
   
    private func checkNotificationsAuthorizationStatus() {
        let userNotificationCenter = UNUserNotificationCenter.current()
        userNotificationCenter.getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .authorized:
                print("The app is authorized to schedule or receive notifications.")
            case .denied:
                print("The app isn't authorized to schedule or receive notifications.")
                
            case .notDetermined:
                print("The user hasn't yet made a choice about whether the app is allowed to schedule notifications.")
            case .provisional:
                print("The application is provisionally authorized to post noninterruptive user notifications.")
            }
        }
    }
    
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(String(describing: stateChanges))")
    }
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        
        print("SubscriptionStateChange: \n\(String(describing: stateChanges))")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
            StoredData.shared.playerId = playerId
            UserDefaults.standard.set(playerId, forKey: "playerId")
            //kUserDefault.synchronize()
        }
    }
    
    //ShowHUD
    func ShowHUD(){
        //let loadingNotification = MBProgressHUD.showAdded(to: self.window!, animated: true)
        // loadingNotification.mode = MBProgressHUDMode.indeterminate
        // loadingNotification.label.text = "Loading..."
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    //HideHUD
    func HideHUD(){
        //MBProgressHUD.hide(for: self.window!, animated: true)
        PKHUD.sharedHUD.hide()/*
         PKHUD.sharedHUD.hide(afterDelay: 1.0) { success in
         // Completion Handler
         HUD.flash(.success, delay: 1.0)
         }*/
    }
    
    //ShowAlert
    func ShowAlert(title:String, msg:String) {
        //let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        // alert.show(self.window!, sender: self)
        let alert = UIAlertController(title: title, message:msg, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        if #available(iOS 10.0, *) {
            self.saveContext()
        } else {
            // Fallback on earlier versions
        }
    }
    
    //MARK:- FB/Google
    
//    func application(_ application: UIApplication,
//                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//
//        let handled =  FBSDKApplicationDelegate.sharedInstance().application(application,
//                                                                             open: url,
//                                                                             sourceApplication: sourceApplication,
//                                                                             annotation: annotation)
//        return handled || GIDSignIn.sharedInstance().handle(url,
//                                                            sourceApplication: sourceApplication,
//                                                            annotation: annotation)
//    }
//
//    func application(_ app: UIApplication, open url: URL,
//                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
//        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
//        let handled =  FBSDKApplicationDelegate.sharedInstance().application(app,
//                                                                             open: url,
//                                                                             sourceApplication: sourceApplication,
//                                                                             annotation: annotation)
//        return handled || GIDSignIn.sharedInstance().handle(url,
//                                                            sourceApplication: sourceApplication,
//                                                            annotation: annotation)
//    }
//
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
//    }
//
//    func application(_ application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
//    }
    
    
//    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//
//        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
//        // Add any custom logic here.
//        return handled
//    }
//
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//
//        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//        // Add any custom logic here.
//        return handled
//    }
    
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let handled =  ApplicationDelegate.shared.application(application,
                                                                             open: url,
                                                                             sourceApplication: sourceApplication,
                                                                             annotation: annotation)
//        return handled || GIDSignIn.sharedInstance().handle(url,
//                                                            sourceApplication: sourceApplication,
//                                                            annotation: annotation)
        
        return handled || (GIDSignIn.sharedInstance()?.handle(url))!
    }

    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool
    {
        
//        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
//                                                                sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//                                                                annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        let googleDidHandle = (GIDSignIn.sharedInstance()?.handle(url))!
        
        guard let source = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String else { return false }
        let annotation = options[UIApplication.OpenURLOptionsKey.annotation] as? String
        
        let facebookDidHandle =  ApplicationDelegate.shared.application(application, open: url, sourceApplication: source, annotation: annotation)
        
        
        //            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: source, annotation: annotation)
        
        
        
        return googleDidHandle || facebookDidHandle
    }
    
//    {
//        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
//        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
//        let handled =  FBSDKApplicationDelegate.sharedInstance().application(app,
//                                                                             open: url,
//                                                                             sourceApplication: sourceApplication,
//                                                                             annotation: annotation)
//        return handled || GIDSignIn.sharedInstance().handle(url,
//                                                            sourceApplication: sourceApplication,
//                                                            annotation: annotation)
//    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "RunApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    @available(iOS 10.0, *)
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


struct Analytics {
    static func trackEvent(withScreen screen: Screen, category: String, label: String, action: Actions, value: Int? = nil) {
        guard
            let tracker = GAI.sharedInstance().defaultTracker,
            let builder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action.rawValue, label: label, value: NSNumber(integerLiteral: value ?? 0))
        else { return }
 
        tracker.set(kGAIScreenName, value: screen.rawValue)
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
 
    static func trackPageView(withScreen screen: Screen) {
        guard
            let tracker = GAI.sharedInstance().defaultTracker,
            let builder = GAIDictionaryBuilder.createScreenView()
        else { return }
 
        tracker.set(kGAIScreenName, value: screen.rawValue)
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
 
    enum Actions: String {
        case search = "Search"
        case tap = "Tap"
        case toggle = "Toggle"
    }
 
    enum Screen: String {
        case exampleScreenName = "exampleScreenName"
    }
}
