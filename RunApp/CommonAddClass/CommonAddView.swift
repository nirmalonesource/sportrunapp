//
//  CommonAddView.swift
//  RunApp
//
//  Created by My Mac on 28/06/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight
import Alamofire
import SVProgressHUD
import CoreLocation
import FGRoute
import GoogleTagManager

class CommonAddView: GAITrackedViewController, UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate {

    @IBOutlet var collection_adv: UICollectionView!
     var newsFeedList = [[String : Any]]()
    var lagitude = Double()
    var logitude = Double()
    var AdvID = String()
    var locationManager = CLLocationManager()
     var flag = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //   self.view.mixedBackgroundColor = MixedColor(normal:FontColor.dayfont , night: FontColor.nightfont)
       // self.screenName = "Ticker";
       // GAI
//       let tracker = GAI.sharedInstance().defaultTracker
//       tracker?.set(kGAIScreenName, value: "Ticker")
//       let eventTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
//       tracker?.send(eventTracker as! [NSObject : AnyObject])
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Ticker")

        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         getNewsFeed()
        
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.requestLocation()
            flag = true
            // mapVW.mapType = .hybrid
        } else {
            print("Please turn on location services or GPS")
        }
    }
    var timr=Timer()
    var w:CGFloat=0.0
    
    override func viewDidAppear(_ animated: Bool) {
        configAutoscrollTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        deconfigAutoscrollTimer()
    }
    
    func configAutoscrollTimer()
    {
        
        timr=Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(CommonAddView.autoScrollView), userInfo: nil, repeats: true)
        RunLoop.current.add(self.timr, forMode: .common)
    }
    
    func deconfigAutoscrollTimer()
    {
        timr.invalidate()
        
    }
    
    func onTimer()
    {
        autoScrollView()
    }
    
    @objc func autoScrollView()
    {
        
        let initailPoint = CGPoint(x: w,y :0)
        
        if __CGPointEqualToPoint(initailPoint, collection_adv.contentOffset)
        {
            if w<collection_adv.contentSize.width
            {
                w += 2.0
            }
            else
            {
                w = -self.view.frame.size.width
            }
            
            let offsetPoint = CGPoint(x: w,y :0)
            
            collection_adv.contentOffset=offsetPoint
            
        }
        else
        {
            w=collection_adv.contentOffset.x
        }
    }
    
    func getlocationinfo()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            let parameters = ["Lat" : lagitude,
                              "Long" : logitude,
                              "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_AUTO_CHECK_OUT,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print("AUTOCHECKOUTRESPONSE:",result)
                        if result.getBool(key: "status")
                        {
                            //  let dic = result["data"] as! NSDictionary
                            //    self.newsFeedList = dic.value(forKey:  "news") as! [[String : Any]]
                            //                                       self.newsFeedList = result["data"] as? [[String:Any]] ?? []
                            //                                       self.collection_adv.reloadData()
                        }
            })
        }
        
    
    }
    
    func getNewsFeed()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_APP_TICKER,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print("TICKERLIST:",result)
                    if result.getBool(key: "status")
                    {
                        //  let dic = result["data"] as! NSDictionary
                        //    self.newsFeedList = dic.value(forKey:  "news") as! [[String : Any]]
                        self.newsFeedList = result["data"] as? [[String:Any]] ?? []
                        self.collection_adv.reloadData()
                    }
        })
    }
    
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
     {
        if flag {
            flag = false
            if let location = locations.last
            {
                //print("Found user's location: \(location)")
                print("latitude: \(location.coordinate.latitude)")
                print("longitude: \(location.coordinate.longitude)")
                lagitude = location.coordinate.latitude
                logitude = location.coordinate.longitude
                locationManager.stopUpdatingLocation()
                getlocationinfo()
            }
        }
            
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
           print("Unable to access your current location")
       }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return newsFeedList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionAdvertiseCell", for: indexPath) as! CollectionAdvertiseCell
            
            //            cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
            
            cell.imgImage.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
            
            let dic:NSDictionary = newsFeedList[indexPath.row] as NSDictionary
            let type = dic.value(forKey: "type") as? String
            if type == "Adv"
            {
                cell.lblName.text = dic.value(forKey: "TickerTitle") as? String
                cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
                let imgURL = ServiceList.ADMIN_IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                          completed: nil)
            }
                
            else if type == "Scores"
            {
                let TickerName1 = dic.value(forKey: "TickerName1") as? String
                let TickerName2 = dic.value(forKey: "TickerName2") as? String
                let TikcerScore1 = dic.value(forKey: "TikcerScore1") as? String
                let TickerScore2 = dic.value(forKey: "TickerScore2") as? String
                cell.lblName.text = String(format: "%@-%@",TickerName1 ?? "",TickerName2 ?? "" )
                cell.lblDescription.text = String(format: "%@ | %@",TikcerScore1 ?? "",TickerScore2 ?? "" )
                cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
                let imgURL = ServiceList.ADMIN_IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                          completed: nil)
            }
                
            else if type == "News"
            {
                cell.lblName.text = dic.value(forKey: "TickerTitle") as? String
                cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
                let imgURL = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                          completed: nil)
            }
                
            else if type == "Tweet"
            {
                let TickerName1 = dic.value(forKey: "TickerName1") as? String
                //let TickerName2 = dic.value(forKey: "TickerName2") as? String
                cell.lblName.text = TickerName1
                cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
                let imgURL = ServiceList.ADMIN_IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad, completed: nil)
            }
        else if type == "tv"
                   {
                    cell.lblName.text = dic.value(forKey: "TickerTitle") as? String
                    cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                    cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
                    let imgURL = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                    cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,completed: nil)
                   }
        else if type == "Events"
        {
            let TickerName1 = dic.value(forKey: "EvenetName") as? String
            //let TickerName2 = dic.value(forKey: "TickerName2") as? String
            cell.lblName.text = TickerName1
            cell.lblDescription.text = dic.value(forKey: "EventFromDate") as? String
            cell.imgImage.setRadius(radius: cell.imgImage.frame.height/2)
//            let imgURL = ServiceList.ADMIN_IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
//            cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
//                                      completed: nil)
            cell.imgImage.image = UIImage(named: "redLocation")
        }
        
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            return  CGSize(width: collectionView.frame.width/1.5 - 10, height: 51)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dic:NSDictionary = newsFeedList[indexPath.row] as NSDictionary
        let type = dic.value(forKey: "type") as? String
        let Ticker1ID = dic.value(forKey: "Ticker1ID") as? String
        
        if type == "Tweet"
        {
            if Ticker1ID != nil {
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                nextViewController.id = Ticker1ID!
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }

        }
        
        else if type == "Adv" {
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
//
//            nextViewController.type = type
//            self.navigationController?.pushViewController(nextViewController, animated: true)
             let NewsURL = newsFeedList[indexPath.row]["TickerURL"] as? String
            AdvID = newsFeedList[indexPath.row]["AdvID"] as? String ?? ""
            ClickCount()
//            weak var tracker = GAI.sharedInstance()?.defaultTracker
//                           tracker?.set(kGAIScreenName, value: dic.value(forKey: "TickerTitle") as? String)
//                           //tracker?.send(GAIDictionaryBuilder.createAppView().build() as? [AnyHashable : Any])
//                           tracker?.send(GAIDictionaryBuilder.createScreenView()?.build() as? [AnyHashable : Any])
//            tracker?.set(kGAIScreenName, value: nil)
            
            let tracker = GAI.sharedInstance().defaultTracker

            tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: "Game 1", action: "Start Pressed", label: dic.value(forKey: "TickerTitle") as? String, value: nil).build() as [NSObject : AnyObject])
            
             tracker?.set(kGAIScreenName, value:nil)
            
             if (NewsURL != nil)
             {
                
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! webView
                    nextViewController.urlString = NewsURL!
                    self.navigationController?.pushViewController(nextViewController, animated: true)
            }
           
        }
        
        else if type == "Scores" {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
            nextViewController.type = type
           
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            
        else if type == "News" {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
             nextViewController.type = type
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else if type == "tv" {
                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
                    nextViewController.type = "tv"
                   self.navigationController?.pushViewController(nextViewController, animated: true)
               }
        else if type == "Events" {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
             nextViewController.type = type
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    
        
        }
    
    
    func ClickCount()
          {
            
           let parameters = [ "PlayerID" :UserDefaults.standard.getUserDict()["id"] as? String ?? "","SponsorID":AdvID,
                               "IPAddress":FGRoute.getIPAddress()!,
                               "AdType":"TI"] as [String : Any]
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           callApi(ServiceList.SERVICE_URL+ServiceList.Sponser_click_count,
                   method: .post,
                   param: parameters,
                   extraHeader: header,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           
                       }
           })
    

       }
}
